<Class name="Repository" id="c8ea3030-1542-4352-a120-fc6813cbe923">
    <superclass name="GitObject"/>
    <transient>true</transient>
    <final>true</final>
    <subclassPersistentAllowed>true</subclassPersistentAllowed>
    <subclassSharedTransientAllowed>true</subclassSharedTransientAllowed>
    <subclassTransientAllowed>true</subclassTransientAllowed>
    <transientAllowed>true</transientAllowed>
    <Constant name="State_None" id="44478b46-5877-46fc-8a29-b2d96fd00f43">
        <source>0</source>
        <type name="Integer"/>
    </Constant>
    <Constant name="State_Merge" id="e8fe5ac2-5c54-4c57-9cdc-0c9a8b1aca66">
        <source>1</source>
        <type name="Integer"/>
    </Constant>
    <ExplicitInverseRef name="commits" id="d5b5bb8e-7a22-4fc5-9844-9f459cb934a1">
        <kind>parent</kind>
        <updateMode>automatic</updateMode>
        <type name="CommitDict"/>
        <access>protected</access>
        <Inverse name="GitObject::parent"/>
    </ExplicitInverseRef>
    <ExplicitInverseRef name="trees" id="7c519281-c2da-4288-b0ca-fc2d54c4db1a">
        <kind>parent</kind>
        <updateMode>automatic</updateMode>
        <type name="TreeDict"/>
        <access>protected</access>
        <Inverse name="GitObject::parent"/>
    </ExplicitInverseRef>
    <ExplicitInverseRef name="blobs" id="014bf11a-68c0-41bf-8089-1a275b73ebd0">
        <kind>parent</kind>
        <updateMode>automatic</updateMode>
        <type name="BlobDict"/>
        <access>protected</access>
        <Inverse name="GitObject::parent"/>
    </ExplicitInverseRef>
    <ImplicitInverseRef name="data" id="f73f9883-ee4e-481d-b5a8-e0ca6fd53b19">
        <embedded>true</embedded>
        <type name="RepositoryData"/>
        <access>readonly</access>
    </ImplicitInverseRef>
    <ImplicitInverseRef name="worktree" id="88a317ed-51f1-43d2-af0a-b1c95ee17f10">
        <embedded>true</embedded>
        <type name="WorktreeData"/>
        <access>readonly</access>
    </ImplicitInverseRef>
    <ExplicitInverseRef name="index_" id="47142671-36e5-4585-bff0-c6ccb0b1f6bb">
        <kind>parent</kind>
        <updateMode>automatic</updateMode>
        <embedded>true</embedded>
        <type name="Index"/>
        <access>protected</access>
        <Inverse name="GitObject::parent"/>
    </ExplicitInverseRef>
    <JadeMethod name="open" id="2dcea02f-3e50-4e36-8e5c-048b5c8b6fbc">
        <options>4</options>
        <source>open(path: String): Repository typeMethod;

// TODO: Switch to using external method that can distinguish when to open repo on disk rather than within database

begin
	return Root@get().repositories[path].open();
end;
</source>
        <Parameter name="path">
            <type name="String"/>
        </Parameter>
        <ReturnType>
            <type name="Repository"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="getRepository" id="a5520ab1-a4b4-48d3-bda8-04a389835ec6">
        <source>getRepository(): Repository;

begin
	return self;
end;
</source>
        <ReturnType>
            <type name="Repository"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="isActive" id="25a0d4af-bafd-4815-b485-7cad7852d8f5">
        <source>isActive(): Boolean;

begin
	return data.isActive();
end;
</source>
        <ReturnType>
            <type name="Boolean"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="isStaged" id="82aeaf39-0042-4603-966a-1208bca66218">
        <source>isStaged(): Boolean;

begin
	return worktree &lt;&gt; null and worktree.changes.hasStaged();
end;
</source>
        <ReturnType>
            <type name="Boolean"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="isHead" id="5344b224-a09e-4cb7-afa7-cab2261d11cd">
        <source>isHead(ref: Reference_): Boolean;

begin
	return worktree &lt;&gt; null and worktree.head().target = ref.name;
end;
</source>
        <Parameter name="ref">
            <type name="Reference_"/>
        </Parameter>
        <ReturnType>
            <type name="Boolean"/>
        </ReturnType>
    </JadeMethod>
    <ExternalMethod name="head" id="bab9a2a8-40ed-45c9-9718-cea9d5fee7df">
        <entrypoint>jadegit_repo_head</entrypoint>
        <library name="jadegitscm"/>
        <source>head(): Reference_ is jadegit_repo_head in jadegitscm;
</source>
        <ReturnType>
            <type name="Reference_"/>
        </ReturnType>
    </ExternalMethod>
    <ExternalMethod name="abort" id="bee2e2f0-4416-44a4-bb88-c102df7fe860">
        <entrypoint>jadegit_repo_abort</entrypoint>
        <library name="jadegitscm"/>
        <source>abort() is jadegit_repo_abort in jadegitscm;
</source>
    </ExternalMethod>
    <ExternalMethod name="continue_" id="1097adb2-408c-4319-98da-4ed53f85a8ac">
        <entrypoint>jadegit_repo_continue</entrypoint>
        <library name="jadegitscm"/>
        <source>continue_(message: String): Task is jadegit_repo_continue in jadegitscm;
</source>
        <Parameter name="message">
            <type name="String"/>
        </Parameter>
        <ReturnType>
            <type name="Task"/>
        </ReturnType>
    </ExternalMethod>
    <ExternalMethod name="state" id="d188156c-be8f-48f3-9445-930176639e73">
        <entrypoint>jadegit_repo_state</entrypoint>
        <library name="jadegitscm"/>
        <source>state(): Integer is jadegit_repo_state in jadegitscm;
</source>
        <ReturnType>
            <type name="Integer"/>
        </ReturnType>
    </ExternalMethod>
    <ExternalMethod name="index" id="f69bba66-c42a-4589-b42f-2a7dca0c126d">
        <entrypoint>jadegit_repo_index</entrypoint>
        <library name="jadegitscm"/>
        <source>index(): Index is jadegit_repo_index in jadegitscm;
</source>
        <ReturnType>
            <type name="Index"/>
        </ReturnType>
    </ExternalMethod>
    <ExternalMethod name="message" id="a6e9ba2c-084c-4b2c-b222-38a2d9074766">
        <entrypoint>jadegit_repo_message</entrypoint>
        <library name="jadegitscm"/>
        <source>message(): String is jadegit_repo_message in jadegitscm;
</source>
        <ReturnType>
            <type name="String"/>
        </ReturnType>
    </ExternalMethod>
    <ExternalMethod name="commit" id="27642fab-9c78-46ed-8aa2-bc796dd78f04">
        <entrypoint>jadegit_repo_commit</entrypoint>
        <library name="jadegitscm"/>
        <source>commit(message: String) is jadegit_repo_commit in jadegitscm;
</source>
        <Parameter name="message">
            <type name="String"/>
        </Parameter>
    </ExternalMethod>
    <JadeMethod name="reset" id="9a8b9a8e-a98a-4191-8e74-f4535f968526">
        <source>reset(hard: Boolean): Task;

begin
	// TODO: Implement reset menu items in context of commit history
	return Commit@lookup(self, head().target).reset(hard);
end;
</source>
        <Parameter name="hard">
            <type name="Boolean"/>
        </Parameter>
        <ReturnType>
            <type name="Task"/>
        </ReturnType>
    </JadeMethod>
    <ExternalMethod name="config" id="c7549f05-e179-4cd4-96f2-5065e41fb56d">
        <entrypoint>jadegit_repo_config</entrypoint>
        <library name="jadegitscm"/>
        <source>config(): Config is jadegit_repo_config in jadegitscm;
</source>
        <ReturnType>
            <type name="Config"/>
        </ReturnType>
    </ExternalMethod>
    <ExternalMethod name="amend" id="82247925-4ea8-4664-a1a2-2b2158939255">
        <entrypoint>jadegit_repo_amend</entrypoint>
        <library name="jadegitscm"/>
        <source>amend(message: String) is jadegit_repo_amend in jadegitscm;
</source>
        <Parameter name="message">
            <type name="String"/>
        </Parameter>
    </ExternalMethod>
</Class>

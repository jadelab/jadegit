<Class name="Property">
    <JadeMethod name="filter" id="2a1384ef-e2ad-4bc4-b1f7-3b33b4508c87">
        <source>filter(): Boolean;

constants
	SubAccess_Unpublished	: Character = 4.Character;
	SubAccess_SystemOnly	: Character = 6.Character;

vars
	explicitRef	: ExplicitInverseRef;
	
begin
	if virtual then
		return false;
	endif;
	
	if isDesignTimeDynamicProperty() then
		return false;
	endif;
	
	if (subAccess = SubAccess_SystemOnly or subAccess = SubAccess_Unpublished) and
	   (access = SchemaEntity.Access_Protected and name[1] = "_") then
		return false;
	endif;
	
	if isKindOf(ExplicitInverseRef) then
		explicitRef := self.ExplicitInverseRef;
		if explicitRef.updateMode = ExplicitInverseRef.UpdateMode_Automatic then
			return false;
		endif;
		if explicitRef.updateMode = ExplicitInverseRef.UpdateMode_ManAuto then
			if explicitRef.kind = ExplicitInverseRef.Kind_Parent then
				return false;
			endif;
			if explicitRef.type.isKindOf(CollClass) then
				return false;
			endif;
		endif;
	endif;
	
	// Don't support collections at all yet ..
	if type.isKindOf( CollClass ) then
	
		if type.inheritsFrom(InternalPseudoArray) then
			return false;
		endif;
		
		if type.inheritsFrom(Dictionary) then
			return false;
		endif;
	
	//	return false;
		
	elseif type.isKindOf( Class ) then
		// Exclude properties referring to hidden classes
		if not SchemaGenerator@filterClass(type.Class, false) then
			return false;
		endif;
		
	// Currently only support attributes &amp; class references .. further work needed to add in support for interfaces 
	elseif not type.isKindOf( PrimType ) then
		return false;
	endif;
	
	return true;
end;</source>
        <ReturnType>
            <type name="Boolean"/>
        </ReturnType>
    </JadeMethod>
    <JadeMethod name="generate" id="8ba6d6cf-fdd4-4623-b9bf-86fd8abbb85e">
        <source>generate(version: String; first: Boolean io; xmlClass: JadeXMLElement input; xmlFeature: JadeXMLElement io; classHeader: IStringStream; classInitialization: IStringStream): Boolean;

vars
	xml  : JadeXMLElement;
	minVersion : String;
	
begin
	xml := xmlClass.getElementByTagName(name);
	if xml = null then
		xml := xmlClass.addElement(name);
		if xmlClass.getAttributeValue("minVersion", "") &lt;&gt; version then		// Assume minimum version is current, except when that matches class
			minVersion := version;
			xml.setAttributeValue("minVersion", minVersion);
		endif;
	else
		minVersion := xml.getAttributeValue("minVersion", null);
	endif;
	if xmlFeature &lt;&gt; null then
		xml.moveAfter(xmlFeature);
	endif;
	xmlFeature := xml;

	if not xml.setAttributeValue("included", xml.getAttributeValue("included", filter())).Boolean then
		return false;
	endif;
	
	if first then
		classHeader.writeLine(Tab);
		first := false;
	endif;
	
	classHeader.writeString(Tab &amp; Tab);
	if class.inheritsFrom(PrimAttribute) then
		classHeader.writeString(PrimAttribute.name);
	else
		classHeader.writeString(class.name);
	endif;
	classHeader.writeLine("* const " &amp; xml.getAttributeValue("name", generateName()) &amp; ";");
	
	// Source
	if classInitialization = null then
		return true;
	endif;
	
	classInitialization.writeLine(",");
	classInitialization.writeString(Tab &amp; Tab);
	classInitialization.writeString(xml.getAttributeValue("name", generateName()) &amp; "(New");
	
	if isKindOf(PrimAttribute) then
		classInitialization.writeString(type.name &amp; "(");
	elseif isKindOf(Reference) then
		classInitialization.writeString("Reference&lt;" &amp; class.name &amp; "&gt;(");
	else
		classInitialization.writeString("Property&lt;" &amp; class.name &amp; "&gt;(");
	endif;
	
	if minVersion &lt;&gt; null then
		classInitialization.writeString("Version(" &amp; generateVersionParams(minVersion) &amp;"), ");
	endif;
	
	classInitialization.writeString('"' &amp; name &amp; '"');
	
	if isKindOf(PrimAttribute) then
		if getLength() &lt;&gt; null and not (self.PrimAttribute.hasUnboundedLength or getLength() = type.maxLength()) then
			classInitialization.writeString(", " &amp; getLength().String);
		endif;
	else
		classInitialization.writeString(", NewType&lt;" &amp; type.class.name &amp; '&gt;("' &amp; type.name &amp; '")');
	endif;
	classInitialization.writeString("))");
	
	return true;
end;
</source>
        <Parameter name="version">
            <type name="String"/>
        </Parameter>
        <Parameter name="first">
            <usage>io</usage>
            <type name="Boolean"/>
        </Parameter>
        <Parameter name="xmlClass">
            <usage>input</usage>
            <type name="JadeXMLElement"/>
        </Parameter>
        <Parameter name="xmlFeature">
            <usage>io</usage>
            <type name="JadeXMLElement"/>
        </Parameter>
        <Parameter name="classHeader">
            <type name="IStringStream"/>
        </Parameter>
        <Parameter name="classInitialization">
            <type name="IStringStream"/>
        </Parameter>
        <ReturnType>
            <type name="Boolean"/>
        </ReturnType>
    </JadeMethod>
</Class>

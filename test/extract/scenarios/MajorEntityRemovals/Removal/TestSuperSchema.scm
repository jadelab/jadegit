jadeVersionNumber "20.0.02";
schemaDefinition
TestSuperSchema subschemaOf RootSchema completeDefinition, patchVersioningEnabled = false;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:19:19:05:30.663;
importedPackageDefinitions
constantDefinitions
localeDefinitions
	2057 "English (United Kingdom)";
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:19:19:05:30.647;
	5129 "English (New Zealand)" schemaDefaultLocale;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:19:19:05:30.647;
	1033 "English (United States)" _cloneOf 5129;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:19:19:05:30.647;
libraryDefinitions
typeHeaders
	TestSuperSchema subclassOf RootSchemaApp transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2192;
	GTestSuperSchema subclassOf RootSchemaGlobal transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2193;
	STestSuperSchema subclassOf RootSchemaSession transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2194;
 
membershipDefinitions
 
typeDefinitions
	Object completeDefinition
	(
	)
	Application completeDefinition
	(
	)
	RootSchemaApp completeDefinition
	(
	)
	TestSuperSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:19:19:05:30.663;
	)
	Global completeDefinition
	(
	)
	RootSchemaGlobal completeDefinition
	(
	)
	GTestSuperSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:19:19:05:30.663;
	)
	JadeWebService completeDefinition
	(
		setModifiedTimeStamp "<unknown>" "9.9.00" 260208 2008:03:04:13:10:57.584;
	webServicesClassProperties
	(
		wsdl = ``;
	)
	)
	JadeWebServiceProvider completeDefinition
	(
		setModifiedTimeStamp "<unknown>" "9.9.00" 130307 2007:03:15:14:50:43.084;
	webServicesClassProperties
	(
		additionalInfo = ``;
		wsdl = ``;
		secureService = default;
	)
	)
	WebSession completeDefinition
	(
	)
	RootSchemaSession completeDefinition
	(
		setModifiedTimeStamp "<unknown>" "6.1.00" 20031119 2003:12:01:13:54:02.270;
	)
	STestSuperSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:19:19:05:30.663;
	)
 
inverseDefinitions
databaseDefinitions
TestSuperSchemaDb
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:19:19:05:30.664;
	databaseFileDefinitions
		"testsuperschema" number = 58;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:10:19:19:05:30.664;
	defaultFileDefinition "testsuperschema";
	classMapDefinitions
		STestSuperSchema in "_environ";
		TestSuperSchema in "_usergui";
		GTestSuperSchema in "testsuperschema";
	)
schemaViewDefinitions
exportedPackageDefinitions
typeSources

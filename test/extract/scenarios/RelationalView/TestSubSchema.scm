jadeVersionNumber "20.0.02";
schemaDefinition
TestSubSchema subschemaOf TestSchema completeDefinition, patchVersioningEnabled = false;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:14:44:44.272;
importedPackageDefinitions
constantDefinitions
localeDefinitions
	5129 "English (New Zealand)" schemaDefaultLocale;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:14:44:44.260;
	1033 "English (United States)" _cloneOf 5129;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:14:44:44.260;
	2057 "English (United Kingdom)" _cloneOf 5129;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:14:44:44.260;
libraryDefinitions
typeHeaders
	TestSubSchema subclassOf TestSchema transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2051;
	GTestSubSchema subclassOf GTestSchema transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2052;
	TestChild subclassOf Object highestOrdinal = 2, number = 2056;
	TestParent subclassOf TestObject highestSubId = 2, highestOrdinal = 3, number = 2055;
	STestSubSchema subclassOf STestSchema transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2053;
	TestChildSet subclassOf ObjectSet loadFactor = 66, transient, sharedTransientAllowed, transientAllowed, number = 2057;
	TestParentArray subclassOf ObjectArray transient, sharedTransientAllowed, transientAllowed, number = 2058;
 
membershipDefinitions
	TestChildSet of TestChild ;
	TestParentArray of TestParent ;
 
typeDefinitions
	Object completeDefinition
	(
	)
	Application completeDefinition
	(
	)
	RootSchemaApp completeDefinition
	(
	)
	TestSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:14:44:39.009;
	)
	TestSubSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:14:44:44.272;
	)
	Global completeDefinition
	(
	)
	RootSchemaGlobal completeDefinition
	(
	)
	GTestSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:14:44:39.009;
	)
	GTestSubSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:14:44:44.272;
	)
	TestChild completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:15:06:39.080;
	referenceDefinitions
		parent:                        TestParent   explicitEmbeddedInverse, number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:14:47:34.836;
	)
	TestObject completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:14:44:58.406;
	)
	TestParent completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:14:46:27.376;
	attributeDefinitions
		number:                        Integer number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:14:45:26.355;
	referenceDefinitions
		children:                      TestChildSet   explicitInverse, readonly, subId = 1, number = 2, ordinal = 2;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:14:47:34.838;
		peers:                         TestParentArray  implicitMemberInverse, subId = 2, number = 3, ordinal = 3;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:14:59:36.317;
	)
	WebSession completeDefinition
	(
	)
	RootSchemaSession completeDefinition
	(
		setModifiedTimeStamp "<unknown>" "6.1.00" 20031119 2003:12:01:13:54:02.270;
	)
	STestSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:14:44:39.009;
	)
	STestSubSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:14:44:44.272;
	)
	Collection completeDefinition
	(
	)
	Btree completeDefinition
	(
	)
	Set completeDefinition
	(
	)
	ObjectSet completeDefinition
	(
	)
	TestChildSet completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:14:46:13.823;
	)
	List completeDefinition
	(
	)
	Array completeDefinition
	(
	)
	ObjectArray completeDefinition
	(
	)
	TestParentArray completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:14:59:21.007;
	)
 
inverseDefinitions
	children of TestParent automatic parentOf parent of TestChild manual;
databaseDefinitions
TestSubSchemaDb
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:14:44:44.272;
	databaseFileDefinitions
		"testsubschema" number = 52;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:14:44:44.272;
	defaultFileDefinition "testsubschema";
	classMapDefinitions
		STestSubSchema in "_environ";
		TestSubSchema in "_usergui";
		TestParent in "testschema";
		TestChild in "testschema";
		GTestSubSchema in "testsubschema";
	)
schemaViewDefinitions
exportedPackageDefinitions
typeSources

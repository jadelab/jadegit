jadeVersionNumber "20.0.02";
schemaDefinition
TestSchema subschemaOf RootSchema completeDefinition, patchVersioningEnabled = false;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:09:23:34.705;
importedPackageDefinitions
constantDefinitions
localeDefinitions
	5129 "English (New Zealand)" schemaDefaultLocale;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:09:23:34.690;
	1033 "English (United States)" _cloneOf 5129;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:09:23:34.690;
	2057 "English (United Kingdom)" _cloneOf 5129;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:09:23:34.690;
libraryDefinitions
typeHeaders
	AnyArray subclassOf Array number = 2389;
	TestSchema subclassOf RootSchemaApp transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2155;
	GTestSchema subclassOf RootSchemaGlobal transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2156;
	STestSchema subclassOf RootSchemaSession transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2157;
membershipDefinitions
	AnyArray of Any;
typeDefinitions
	AnyArray completeDefinition
	(
		setModifiedTimeStamp "Kevin" "22.0.03" 2024:06:09:20:19:19.719;
	)
	Object completeDefinition
	(
	)
	Application completeDefinition
	(
	)
	RootSchemaApp completeDefinition
	(
	)
	TestSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:09:23:34.704;
	)
	Global completeDefinition
	(
	)
	RootSchemaGlobal completeDefinition
	(
	)
	GTestSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:09:23:34.705;
	)
	WebSession completeDefinition
	(
	)
	RootSchemaSession completeDefinition
	(
		setModifiedTimeStamp "<unknown>" "6.1.00" 20031119 2003:12:01:13:54:02.270;
	)
	STestSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:09:23:34.705;
	)
 
inverseDefinitions
databaseDefinitions
TestSchemaDb
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:09:23:34.705;
	databaseFileDefinitions
		"testschema" number = 57;
		setModifiedTimeStamp "Kevin" "20.0.02" 2023:12:07:09:23:34.705;
	defaultFileDefinition "testschema";
	classMapDefinitions
		AnyArray in "testschema";
		STestSchema in "_environ";
		TestSchema in "_usergui";
		GTestSchema in "testschema";
	)
schemaViewDefinitions
exportedPackageDefinitions
typeSources

jadeVersionNumber "20.0.02";
schemaDefinition
TestSchema subschemaOf RootSchema completeDefinition, patchVersioningEnabled = true;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:17:14:03.270;
importedPackageDefinitions
	TestPackage is TestExportSchema::TestPackage
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:27:19:56:07.080;
		importedClassDefinitions
			ExternalItem
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:27:19:56:07.080;
	)

constantDefinitions
localeDefinitions
	5129 "English (New Zealand)" schemaDefaultLocale;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:17:13:59.597;
	1033 "English (United States)" _cloneOf 5129;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:17:13:59.597;
	2057 "English (United Kingdom)" _cloneOf 5129;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:17:13:59.597;
libraryDefinitions
typeHeaders
	TestSchema subclassOf RootSchemaApp transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2192;
	GTestSchema subclassOf RootSchemaGlobal transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2193;
	Item subclassOf Object abstract, highestSubId = 3, highestOrdinal = 8, number = 2195;
	Bug subclassOf Item highestOrdinal = 2, number = 2199;
	Epic subclassOf Item number = 2198;
	Story subclassOf Item highestOrdinal = 2, number = 2197;
	Link subclassOf Object highestOrdinal = 2, number = 2200;
	User subclassOf Object highestSubId = 1, highestOrdinal = 2, number = 2202;
	STestSchema subclassOf RootSchemaSession transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2194;
	ItemDict subclassOf MemberKeyDictionary loadFactor = 66, transient, sharedTransientAllowed, transientAllowed, number = 2196;
	LinkSet subclassOf ObjectSet loadFactor = 66, transient, sharedTransientAllowed, transientAllowed, number = 2201;
	UserSet subclassOf ObjectSet loadFactor = 66, transient, sharedTransientAllowed, transientAllowed, number = 2203;
 
membershipDefinitions
	ItemDict of Item ;
	LinkSet of Link ;
	UserSet of User ;
 
typeDefinitions
	TestPackage::ExternalItem completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:27:19:56:07.080;
 
	jadeMethodDefinitions
		getBug(): Bug number = 1001;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:12:06:11:58:59.196;
		getEpic(): Epic number = 1002;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:12:06:11:58:50.116;
		getStory(): Story number = 1003;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:12:06:11:58:42.764;
		getUser(): User number = 1004;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:12:06:11:58:27.741;
	)
	Object completeDefinition
	(
	)
	Application completeDefinition
	(
	)
	RootSchemaApp completeDefinition
	(
	)
	TestSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:17:14:03.267;
	)
	Global completeDefinition
	(
	)
	RootSchemaGlobal completeDefinition
	(
	)
	GTestSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:17:14:03.269;
	)
	Item completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:21:30:28.621;
	attributeDefinitions
		description:                   String subId = 1, number = 5, ordinal = 6;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:21:31:28.574;
		number:                        Integer number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:17:15:37.657;
		title:                         String[51] number = 2, ordinal = 2;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:21:27:31.264;
	referenceDefinitions
		children:                      ItemDict   explicitInverse, readonly, subId = 1, number = 3, ordinal = 4;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:21:29:34.226;
		links:                         LinkSet   explicitInverse, readonly, subId = 2, number = 6, ordinal = 7;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:21:35:53.859;
		parent:                        Item   explicitEmbeddedInverse, number = 4, ordinal = 5;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:21:29:34.226;
		users:                         UserSet   explicitInverse, subId = 3, number = 7, ordinal = 8;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:22:14:42.141;
 
	jadeMethodDefinitions
		isActive(): Boolean condition, number = 1001;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:21:30:10.934;
	)
	Bug completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:21:30:56.457;
	attributeDefinitions
		reproductionSteps:             String subId = 1, number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:21:33:09.353;
		systemInformation:             String subId = 2, number = 2, ordinal = 2;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:21:33:22.415;
	)
	Epic completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:21:30:35.653;
	)
	Story completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:21:30:23.664;
	attributeDefinitions
		criteria:                      String subId = 1, number = 2, ordinal = 2;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:21:32:04.313;
		points:                        Integer number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:21:30:45.200;
	)
	Link completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:21:34:26.048;
	referenceDefinitions
		left:                          Item   explicitEmbeddedInverse, number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:21:35:53.883;
		right:                         Item   explicitEmbeddedInverse, number = 2, ordinal = 2;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:21:35:53.893;
	)
	User completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:22:13:29.642;
	attributeDefinitions
		name:                          String[51] number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:22:13:38.120;
	referenceDefinitions
		items:                         ItemDict   explicitInverse, readonly, subId = 1, number = 2, ordinal = 2;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:22:14:42.126;
	)
	WebSession completeDefinition
	(
	)
	RootSchemaSession completeDefinition
	(
		setModifiedTimeStamp "<unknown>" "6.1.00" 20031119 2003:12:01:13:54:02.270;
	)
	STestSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:17:14:03.270;
	)
	Collection completeDefinition
	(
	)
	Btree completeDefinition
	(
	)
	Dictionary completeDefinition
	(
	)
	MemberKeyDictionary completeDefinition
	(
	)
	ItemDict completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:21:28:45.518;
	)
	Set completeDefinition
	(
	)
	ObjectSet completeDefinition
	(
	)
	LinkSet completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:21:35:19.184;
	)
	UserSet completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:22:14:01.463;
	)
 
memberKeyDefinitions
	ItemDict completeDefinition
	(
		number;
	)
 
inverseDefinitions
	children of Item automatic parentOf parent of Item manual;
	links of Item automatic parentOf left of Link manual;
	links of Item automatic parentOf right of Link manual;
	items of User automatic peerOf users of Item manual;
databaseDefinitions
TestSchemaDb
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:17:14:03.271;
	databaseFileDefinitions
		"testschema" number = 57;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:17:14:03.271;
	defaultFileDefinition "testschema";
	classMapDefinitions
		STestSchema in "_environ";
		TestSchema in "_usergui";
		GTestSchema in "testschema";
		Item in "testschema";
		Story in "testschema";
		Epic in "testschema";
		Bug in "testschema";
		Link in "testschema";
		User in "testschema";
	)
schemaViewDefinitions
exportedPackageDefinitions
typeSources
	TestPackage::ExternalItem (
	jadeMethodSources
getBug
{
getBug(): Bug;

begin
	return null;
end;

}

getEpic
{
getEpic(): Epic;

begin
	return null;
end;

}

getStory
{
getStory(): Story;

begin
	return null;
end;

}

getUser
{
getUser(): User;

begin
	return null;
end;

}

	)
	Item (
	jadeMethodSources
isActive
{
isActive(): Boolean condition;

begin
	return true;
end;

}

	)

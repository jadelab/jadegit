jadeVersionNumber "20.0.02";
schemaDefinition
TestExportSchema subschemaOf RootSchema completeDefinition, patchVersioningEnabled = false;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:27:19:51:20.904;
importedPackageDefinitions
constantDefinitions
localeDefinitions
	5129 "English (New Zealand)" schemaDefaultLocale;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:17:13:59.597;
	1033 "English (United States)" _cloneOf 5129;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:17:13:59.597;
	2057 "English (United Kingdom)" _cloneOf 5129;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:20:17:13:59.597;
libraryDefinitions
typeHeaders
	TestExportSchema subclassOf RootSchemaApp transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2187;
	ExternalItem subclassOf Object highestOrdinal = 1, number = 2190;
	GTestExportSchema subclassOf RootSchemaGlobal transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2188;
	STestExportSchema subclassOf RootSchemaSession transient, sharedTransientAllowed, transientAllowed, subclassSharedTransientAllowed, subclassTransientAllowed, number = 2189;
 
membershipDefinitions
 
typeDefinitions
	Object completeDefinition
	(
	)
	Application completeDefinition
	(
	)
	RootSchemaApp completeDefinition
	(
	)
	TestExportSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:27:19:51:20.894;
	)
	ExternalItem completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:27:19:55:17.310;
	attributeDefinitions
		name:                          String[51] number = 1, ordinal = 1;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:27:19:51:53.554;
	jadeMethodDefinitions
		getLastUpdatedTimeStamp(): TimeStamp;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:27:19:51:53.554;
	)
	Global completeDefinition
	(
	)
	RootSchemaGlobal completeDefinition
	(
	)
	GTestExportSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:27:19:51:20.904;
	)
	WebSession completeDefinition
	(
	)
	RootSchemaSession completeDefinition
	(
		setModifiedTimeStamp "<unknown>" "6.1.00" 20031119 2003:12:01:13:54:02.270;
	)
	STestExportSchema completeDefinition
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:27:19:51:20.904;
	)
 
inverseDefinitions
databaseDefinitions
TestExportSchemaDb
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:27:19:51:20.904;
	databaseFileDefinitions
		"testexportschema" number = 58;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:27:19:51:20.904;
	defaultFileDefinition "testexportschema";
	classMapDefinitions
		STestExportSchema in "_environ";
		TestExportSchema in "_usergui";
		GTestExportSchema in "testexportschema";
		ExternalItem in "testexportschema";
	)
schemaViewDefinitions
exportedPackageDefinitions
	TestPackage
	(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:27:19:55:55.708;
	exportedClassDefinitions
	ExternalItem
		(
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:27:19:55:55.718;
		exportedPropertyDefinitions
			name ;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:27:19:55:55.718;
		exportedMethodDefinitions
			getLastUpdatedTimeStamp;
		setModifiedTimeStamp "Kevin" "20.0.02" 2024:11:27:19:55:55.718;
		)
	exportedInterfaceDefinitions
	)
typeSources
	ExternalItem
	(
	jadeMethodSources
getLastUpdatedTimeStamp
{
getLastUpdatedTimeStamp(): TimeStamp;

begin
	return null;
end;

}
	)

#include <catch2/catch_test_macros.hpp>
#include <registry/Commit.h>
#include <registry/Repository.h>
#include <registry/Schema.h>

using namespace JadeGit::Registry;

TEST_CASE("Repository.Match", "[registry]")
{
	RepositoryT empty;

	RepositoryT foo;
	foo.name = "foo";
	foo.origin = "http://gitmock.com/foo.git";

	RepositoryT bar;
	bar.name = "bar";
	bar.origin = "http://gitmock.com/bar.git";
	bar.latest.push_back(make_commit("aabb"));
	bar.previous.push_back(make_commit("ccdd"));

	SECTION("Empty")
	{
		CHECK(!repo_match(empty, empty));
	}
	SECTION("None")
	{
		CHECK(!repo_match(foo, bar));
	}
	SECTION("Origin")
	{
		RepositoryT other;
		other.origin = "http://gitmock.com/foo.git";

		CHECK(repo_match(foo, other));
		CHECK(!repo_match(bar, other));
	}
	SECTION("Commit")
	{
		RepositoryT other;
		
		SECTION("Update")
		{
			other.latest.push_back(make_commit("eeff"));
			other.previous.push_back(make_commit("aabb"));

			CHECK(repo_match(bar, other));
		}
		SECTION("Static")
		{
			other.latest.push_back(make_commit("aabb"));
			other.previous.push_back(make_commit("ccdd"));

			CHECK(repo_match(bar, other));
		}
	}
	SECTION("Name")
	{
		RepositoryT other;
		other.name = "foo";

		SECTION("WithoutSchemas")
		{
			CHECK(repo_match(foo, other));
			CHECK(!repo_match(bar, other));
		}
		SECTION("WithSchemas")
		{
			foo.schemas.push_back(make_schema("FooSchemaA"));
			foo.schemas.push_back(make_schema("FooSchemaB"));
			foo.schemas.push_back(make_schema("FooSchemaC"));

			SECTION("None")
			{
				CHECK(!repo_match(foo, other));
				CHECK(!repo_match(bar, other));
			}
			SECTION("Same")
			{
				other.schemas.push_back(make_schema("FooSchemaA"));
				other.schemas.push_back(make_schema("FooSchemaB"));
				other.schemas.push_back(make_schema("FooSchemaC"));

				CHECK(repo_match(foo, other));
			}
			SECTION("Diff")
			{
				other.schemas.push_back(make_schema("BarSchemaD"));
				other.schemas.push_back(make_schema("BarSchemaE"));
				other.schemas.push_back(make_schema("BarSchemaF"));

				CHECK(!repo_match(foo, other));
			}
			SECTION("Intersect")
			{
				other.schemas.push_back(make_schema("FooSchemaA"));
				other.schemas.push_back(make_schema("FooSchemaD"));

				CHECK(repo_match(foo, other));
			}
			SECTION("Unordered")
			{
				other.schemas.push_back(make_schema("FooSchemaC"));
				other.schemas.push_back(make_schema("FooSchemaB"));
				other.schemas.push_back(make_schema("FooSchemaA"));

				CHECK(repo_match(foo, other));
			}
		}
	}
}

TEST_CASE("Repository.Update", "[registry]")
{
	RepositoryT updated;
	updated.name = "foo";
	updated.origin = "http://gitmock.com/foo.git";
	updated.latest.push_back(make_commit("cccc"));
	updated.previous.push_back(make_commit("bbbb"));

	SECTION("Empty")
	{
		RepositoryT empty;

		SECTION("Normal")
		{
			CHECK_THROWS(repo_update(empty, updated, false));
			CHECK(empty != updated);
		}
		SECTION("Forced")
		{
			CHECK_NOTHROW(repo_update(empty, updated, true));
			CHECK(empty == updated);
		}
	}
	SECTION("Current")
	{
		RepositoryT current;
		current.name = "foo";
		current.origin = "http://gitmock.com/foo.git";
		current.latest.push_back(make_commit("bbbb"));
		current.previous.push_back(make_commit("aaaa"));

		SECTION("Normal")
		{
			CHECK_NOTHROW(repo_update(current, updated, false));
			CHECK(current == updated);
		}
		SECTION("Forced")
		{
			CHECK_NOTHROW(repo_update(current, updated, true));
			CHECK(current == updated);
		}
	}
	SECTION("Repeated")
	{
		RepositoryT current = updated;

		SECTION("Normal")
		{
			CHECK_NOTHROW(repo_update(current, updated, false));
			CHECK(current == updated);
		}
		SECTION("Forced")
		{
			CHECK_NOTHROW(repo_update(current, updated, true));
			CHECK(current == updated);
		}
	}
	SECTION("Diverged")
	{
		RepositoryT current;
		current.name = "foo";
		current.origin = "http://gitmock.com/foo.git";
		current.latest.push_back(make_commit("ffff"));
		current.previous.push_back(make_commit("eeee"));

		SECTION("Normal")
		{
			CHECK_THROWS(repo_update(current, updated, false));
			CHECK(current != updated);
		}
		SECTION("Forced")
		{
			CHECK_NOTHROW(repo_update(current, updated, true));
			CHECK(current == updated);
		}
	}
}
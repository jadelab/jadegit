#include <catch2/catch_test_macros.hpp>
#include <jadegit/data/Assembly.h>
#include <jadegit/data/JadeWebServiceManager.h>
#include <jadegit/vfs/MemoryFileSystem.h>
#include <SourceFileSystem.h>

using namespace std;
using namespace JadeGit;
using namespace JadeGit::Data;

TEST_CASE("Application.Resolution", "[data]")
{
	MemoryFileSystem fs;
	Assembly assembly(fs);

	Schema schema(assembly, nullptr, "BaseSchema");
	Application app(&schema, nullptr, "BaseSchemaApp");

	// Check local application can be resolved irrespective of inherit option
	CHECK(Entity::resolve<Application>(schema, "BaseSchemaApp", false, false) == &app);
	CHECK(Entity::resolve<Application>(schema, "BaseSchemaApp", false, true) == &app);

	Schema subschema(assembly, nullptr, "SubSchema");
	subschema.superschema = &schema;

	// Check inherited application can be resolved when inherit option is used
	CHECK(Entity::resolve<Application>(subschema, "BaseSchemaApp", false, false) == nullptr);
	CHECK(Entity::resolve<Application>(subschema, "BaseSchemaApp", false, true) == &app);
}

TEST_CASE("Application.WebServiceClassesConversion", "[data]")
{
	MemoryFileSystem fs(SourceFileSystem("resources/Application/WebServiceClasses"));
	Assembly assembly(fs);

	// Load schema
	auto& schema = Entity::resolve<Schema&>(assembly, "TestSchema", true);

	// Load application
	auto& app = Entity::resolve<Application&>(schema, "TestApplication", false);

	// Load exposure list
	auto& exposure = Entity::resolve<JadeExposedList&>(schema, "TestExposure");

	// Verify original web service classes property has been converted to exposure list
	const JadeWebServiceManager* manager = app.webServiceManager;
	REQUIRE(manager);
	REQUIRE(manager->exposures.size() == 1);
	REQUIRE(manager->exposures.front() == &exposure);

	// Verify original web service classes property is rebuilt from exposure list (invalid entry should've been removed)
	CHECK(app.webServiceClasses == " TestExposure");

	// Verify saving converted application will omit original property
	app.Modified();
	assembly.save();
	ApprovalTests::Approvals::verify(fs);
}
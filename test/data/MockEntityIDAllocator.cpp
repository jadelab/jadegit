#include "MockEntityIDAllocator.h"

namespace JadeGit::Data
{
	MockEntityIDAllocator::MockEntityIDAllocator()
	{
		// Override global allocator to mock allocator
		EntityIDAllocator::set(this);
	}

	MockEntityIDAllocator::~MockEntityIDAllocator()
	{
		// Reset global allocator
		EntityIDAllocator::set(nullptr);
	}

	uuids::uuid MockEntityIDAllocator::allocate(const Entity& entity)
	{
		// Increment last byte & previous when it wraps around to zero
		for (int i = 15; i >= 0; i--)
		{
			if (++bytes[i] != 0)
				break;
		}

		return uuids::uuid(bytes);
	}
}
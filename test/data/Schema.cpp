#include <Approvals.h>
#include <catch2/catch_test_macros.hpp>
#include <jadegit/data/Assembly.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/vfs/MemoryFileSystem.h>
#include <SourceFileSystem.h>

using namespace std;
using namespace JadeGit;
using namespace JadeGit::Data;

TEST_CASE("Schema.Creation", "[data]") 
{
	MemoryFileSystem fs;
	Assembly assembly(fs);

	{
		Schema schema(assembly, nullptr, "TestSchema");

		// Check name has been set
		CHECK("TestSchema" == schema.name);
		CHECK("TestSchema" == schema.GetValue("name").Get<string>());

		// Check schema has been added to collection
		CHECK(&schema == assembly.schemas.Get("TestSchema"));
	}

	// Check schema has been removed from collection
	CHECK(nullptr == assembly.schemas.Get("TestSchema"));
}

TEST_CASE("Schema.Rename", "[data]") 
{
	MemoryFileSystem fs(SourceFileSystem("resources/Schema/Basic"));
	Assembly assembly(fs);

	// Load existing schema
	Schema* schema = Entity::resolve<Schema>(assembly, "TestSchema", false);
	REQUIRE(schema);

	// Verify schema is in collections with current name
	CHECK(schema == assembly.schemas.Get("TestSchema"));

	// Rename schema
	schema->Rename("TestSchemaRenamed");

	// Check name change has been applied
	CHECK("TestSchemaRenamed" == schema->name);
	CHECK(nullptr == assembly.schemas.Get("TestSchema"));
	CHECK(schema == assembly.schemas.Get("TestSchemaRenamed"));

	// Save changes
	assembly.save();

	// Check output matches expected
	ApprovalTests::Approvals::verify(fs);
}

TEST_CASE("Schema.SuperschemaInverseMaintenance", "[data]") 
{
	MemoryFileSystem fs;
	Assembly assembly(fs);

	{
		Schema schema(assembly, nullptr, "TestSchema");

		// Set superschema
		schema.superschema = assembly.GetRootSchema();

		// Check schema has been added to superschema subschemas collection
		CHECK(assembly.GetRootSchema()->subschemas.Includes(&schema));
	}

	// Check subschemas has been cleared
	CHECK(assembly.GetRootSchema()->subschemas.Empty());
}

TEST_CASE("Schema.Write", "[data]") 
{
	MemoryFileSystem fs;
	Assembly assembly(fs);

	Schema schema(assembly, nullptr, "TestSchema");
	schema.Created("47183823-2574-4bfd-b411-99ed177d3e43");

	// Set superschema
	schema.superschema = assembly.GetRootSchema();

	assembly.save();

	ApprovalTests::Approvals::verify(fs);
}

TEST_CASE("Schema.LoadCardSchema", "[data]") 
{
	MemoryFileSystem fs;
	Assembly assembly(fs);

	// Check Cardschema will be loaded on demand
	auto cardSchema = Entity::resolve<Schema>(assembly, "CardSchema");
	REQUIRE(cardSchema);

	// Ensure name is as expected
	CHECK("CardSchema" == cardSchema->name);

	// Ensure CardSchema superschema set to RootSchema
	CHECK(assembly.GetRootSchema() == cardSchema->superschema);

	// Ensure subsequent retrieval returns initial
	CHECK(assembly.schemas.Get("CardSchema") == cardSchema);
}
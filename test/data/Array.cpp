#include <catch2/catch_test_macros.hpp>
#include <jadegit/data/Array.h>

using namespace std;
using namespace JadeGit::Data;

TEST_CASE("Array.Simple", "[data]") 
{
	Array<string> strings;

	strings.Add("Test");
	CHECK(strings.Includes("Test"));

	strings.Remove("Test");
	CHECK(!strings.Includes("Test"));
}
#pragma once
#include <data/EntityIDAllocator.h>

namespace JadeGit::Data
{
	class MockEntityIDAllocator : public EntityIDAllocator
	{
	public:
		MockEntityIDAllocator();
		~MockEntityIDAllocator();

		uuids::uuid allocate(const Entity& entity) final;

	private:
		uuids::uuid::value_type bytes[16] = {};
	};
}
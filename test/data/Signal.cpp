#include <catch2/catch_test_macros.hpp>
#include <jadegit/data/Signal.h>

using namespace JadeGit::Data;

TEST_CASE("Signal.Simple", "[data]") 
{
	Signal<> signal;

	int count = 0;

	signal.Connect(nullptr, [&]() { count++; });

	signal.Emit();

	CHECK(1 == count);

	signal.Disconnect(nullptr);

	signal.Emit();

	CHECK(1 == count);
}

TEST_CASE("Signal.Member", "[data]") 
{
	class Receiver
	{
	public:
		Receiver(Signal<int> &signal) : signal(signal) { signal.Connect(this, &Receiver::SetNumber); }
		~Receiver() { signal.Disconnect(this); }

		void SetNumber(int i) { number = i; }

		int number = 0;

	private:
		Signal<int> &signal;
	};

	Signal<int> signal;
	Receiver receiver(signal);

	signal.Emit(123);
	CHECK(123 == receiver.number);

	signal.Emit(456);
	CHECK(456 == receiver.number);
}
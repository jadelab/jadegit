#include "FakeRegistry.h"
#include <registry/Commit.h>

using namespace JadeGit::Registry;

FakeRegistry FakeRegistry::make_basic()
{
	return FakeRegistry()
		.add_repository("fake-origin")
		.add_previous_commit("aabbcc")
		.add_latest_commit("ddeeff");
}

FakeRegistry& FakeRegistry::add_repository(const char* origin)
{
	RepositoryT repo;
	repo.origin = origin;

	add(repo);

	return *this;
}

FakeRegistry& FakeRegistry::add_previous_commit(const char* commit)
{
	repos.back().previous.push_back(make_commit(commit));
	return *this;
}

FakeRegistry& FakeRegistry::add_latest_commit(const char* commit)
{
	repos.back().latest.push_back(make_commit(commit));
	return *this;
}
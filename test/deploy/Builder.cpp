#include "Builder.h"
#include "FakeRegistry.h"
#include <Approvals.h>

using namespace JadeGit;
using namespace JadeGit::Build;
using namespace JadeGit::Deploy;

void empty_test_scenario(JadeGit::MemoryFileSystem& fs, JadeGit::Deploy::Builder& builder)
{
	// Empty registry
	Registry::Root registry;

	// Empty deployment
	builder.start();
	builder.start(registry);
	builder.finish(registry);
	builder.finish();

	// Check output matches expected
	ApprovalTests::Approvals::verify(fs);
}

void common_test_scenarios(JadeGit::MemoryFileSystem& fs, JadeGit::Deploy::Builder& builder)
{
	SECTION("Empty")
	{
		empty_test_scenario(fs, builder);
	}
	SECTION("SchemaFiles")
	{
		// Fake registry
		FakeRegistry registry = FakeRegistry::make_basic();

		// Start deployment & stage
		builder.start();
		builder.start(registry);

		// Add schema files
		auto schemaFile = builder.AddSchemaFile("TestSchema", false);
		*schemaFile << "schema definition" << std::flush;

		auto schemaDataFile = builder.AddSchemaDataFile("TestSchema", false);
		*schemaDataFile << "schema data" << std::flush;

		// Finish stage & deployment
		builder.finish(registry);
		builder.finish();

		// Check output matches expected
		ApprovalTests::Approvals::verify(fs);
	}
	SECTION("SchemaFilesWithInferredReorg")
	{
		// Fake registry
		FakeRegistry registry = FakeRegistry::make_basic();

		// Start deployment & stage
		builder.start();
		builder.start(registry);

		// Add schema files
		auto schemaFile = builder.AddSchemaFile("TestSchema", true);
		*schemaFile << "schema definition" << std::flush;

		auto schemaDataFile = builder.AddSchemaDataFile("TestSchema", true);
		*schemaDataFile << "schema data" << std::flush;

		// Reorg should be inferred at end of stage

		// Finish stage & deployment
		builder.finish(registry);
		builder.finish();

		// Check output matches expected
		ApprovalTests::Approvals::verify(fs);
	}
	SECTION("CommandFile")
	{
		// Fake registry
		FakeRegistry registry = FakeRegistry::make_basic();

		// Start deployment & stage
		builder.start();
		builder.start(registry);

		// Add command file
		auto commandFile = builder.AddCommandFile(false);
		*commandFile << "commands" << std::flush;

		// Finish stage & deployment
		builder.finish(registry);
		builder.finish();

		// Check output matches expected
		ApprovalTests::Approvals::verify(fs);
	}
	SECTION("CommandFileWithInferredReorg")
	{
		// Fake registry
		FakeRegistry registry = FakeRegistry::make_basic();

		// Start deployment & stage
		builder.start();
		builder.start(registry);

		// Add command file
		auto commandFile = builder.AddCommandFile(true);
		*commandFile << "commands" << std::flush;

		// Reorg should be inferred at end of stage

		// Finish stage & deployment
		builder.finish(registry);
		builder.finish();

		// Check output matches expected
		ApprovalTests::Approvals::verify(fs);
	}
	SECTION("MultipleFilesWithExplicitReorgs")
	{
		// Fake registry
		FakeRegistry registry = FakeRegistry::make_basic();

		// Start deployment & stage
		builder.start();
		builder.start(registry);

		// Add initial commands
		auto commandFile = builder.AddCommandFile(true);
		*commandFile << "commands" << std::flush;

		// Add schema files
		auto schemaFile = builder.AddSchemaFile("TestSchema", true);
		*schemaFile << "schema definition" << std::flush;

		auto schemaDataFile = builder.AddSchemaDataFile("TestSchema", true);
		*schemaDataFile << "schema data" << std::flush;

		// Initial reorg
		builder.reorg();

		// More commands
		commandFile = builder.AddCommandFile(true);
		*commandFile << "more commands" << std::flush;

		schemaFile = builder.AddSchemaFile("AnotherTestSchema", true);
		*schemaFile << "another schema definition" << std::flush;

		schemaDataFile = builder.AddSchemaDataFile("AnotherTestSchema", true);
		*schemaDataFile << "more schema data" << std::flush;

		// Final reorg
		builder.reorg();

		// Finish stage & deployment
		builder.finish(registry);
		builder.finish();

		// Check output matches expected
		ApprovalTests::Approvals::verify(fs);
	}
	SECTION("MultipleFilesWithInferredReorgs")
	{
		// Fake registry
		FakeRegistry registry = FakeRegistry::make_basic();

		// Start deployment & stage
		builder.start();
		builder.start(registry);

		// Add initial commands
		auto commandFile = builder.AddCommandFile(true);
		*commandFile << "commands" << std::flush;

		// Add schema files
		auto schemaFile = builder.AddSchemaFile("TestSchema", true);
		*schemaFile << "schema definition" << std::flush;

		auto schemaDataFile = builder.AddSchemaDataFile("TestSchema", true);
		*schemaDataFile << "schema data" << std::flush;

		// Initial re-org should be inferred by current version load below

		// More commands
		commandFile = builder.AddCommandFile(false);
		*commandFile << "more commands" << std::flush;

		schemaFile = builder.AddSchemaFile("AnotherTestSchema", true);
		*schemaFile << "another schema definition" << std::flush;

		schemaDataFile = builder.AddSchemaDataFile("AnotherTestSchema", true);
		*schemaDataFile << "more schema data" << std::flush;

		// Final reorg should be inferred by prior latest schema version loads

		// Finish stage & deployment
		builder.finish(registry);
		builder.finish();

		// Check output matches expected
		ApprovalTests::Approvals::verify(fs);
	}
	SECTION("Scripts")
	{
		// Fake registry
		FakeRegistry registry = FakeRegistry::make_basic();

		// Start deployment & stage
		builder.start();
		builder.start(registry);

		// Finish stage
		builder.finish(registry);

		// Add scripts
		{
			Script script;
			script.schema = "TestSchema";
			script.app = "TestApp";
			script.executeClass = "JadeScript";
			script.executeMethod = "doSomething";
			builder.addScript(script);
		}
		{
			Script script;
			script.schema = "TestSchema";
			script.app = "TestApp";
			script.executeClass = "TestClass";
			script.executeMethod = "doSomethingWithParam";
			script.executeParam = "param-value";
			builder.addScript(script);
		}
		{
			Script script;
			script.schema = "TestSubSchema";
			script.app = "TestSubApp";
			script.executeClass = "TestClass";
			script.executeMethod = "doSomethingTransiently";
			script.executeTransient = true;
			builder.addScript(script);
		}
		{
			Script script;
			script.schema = "TestSchema";
			script.app = "TestApp";
			script.executeClass = "TestClass";
			script.executeMethod = "doSomethingTransientlyWithParam";
			script.executeParam = "param-value";
			script.executeTransient = true;
			builder.addScript(script);
		}
		{
			Script script;
			script.schema = "TestSchema";
			script.app = "TestApp";
			script.executeClass = "TestClass";
			script.executeMethod = "doSomethingStatically";
			script.executeTypeMethod = true;
			builder.addScript(script);
		}
		{
			Script script;
			script.schema = "TestSchema";
			script.app = "TestApp";
			script.executeClass = "TestClass";
			script.executeMethod = "doSomethingStaticallyWithParam";
			script.executeParam = "param-value";
			script.executeTypeMethod = true;
			builder.addScript(script);
		}
		{
			Script script;
			script.schema = "TestSchema";
			script.app = "TestApp";
			script.executeSchema = "AnotherTestSchema";
			script.executeClass = "AnotherTestClass";
			script.executeMethod = "doAnotherThing";
			builder.addScript(script);
		}
		{
			Script script;
			script.schema = "TestSchema";
			script.app = "TestApp";
			script.executeSchema = "AnotherTestSchema";
			script.executeClass = "AnotherTestClass";
			script.executeMethod = "doAnotherThingTransientlyWithParam";
			script.executeParam = "another-param-value";
			script.executeTransient = true;
			builder.addScript(script);
		}
		{
			Script script;
			script.schema = "TestSchema";
			script.app = "TestApp";
			script.executeSchema = "AnotherTestSchema";
			script.executeClass = "AnotherTestClass";
			script.executeMethod = "doAnotherThingStatically";
			script.executeTypeMethod = true;
			builder.addScript(script);
		}

		// Finish deployment
		builder.finish();

		// Check output matches expected
		ApprovalTests::Approvals::verify(fs);
	}
	SECTION("ScriptAfterCommandFileWithInferredReorg")
	{
		// Fake registry
		FakeRegistry registry = FakeRegistry::make_basic();

		// Start deployment & stage
		builder.start();
		builder.start(registry);

		// Add command file
		auto commandFile = builder.AddCommandFile(true);
		*commandFile << "commands" << std::flush;

		// Re-org should be inferred here

		// Finish stage
		builder.finish(registry);

		// Add script
		Script script;
		script.schema = "TestSchema";
		script.app = "TestApp";
		script.executeClass = "JadeScript";
		script.executeMethod = "doSomething";
		builder.addScript(script);

		// Finish deployment
		builder.finish();

		// Check output matches expected
		ApprovalTests::Approvals::verify(fs);
	}
	SECTION("ScriptAfterSchemaFileWithInferredReorg")
	{
		// Fake registry
		FakeRegistry registry = FakeRegistry::make_basic();

		// Start deployment & stage
		builder.start();
		builder.start(registry);

		// Add schema file
		auto schemaFile = builder.AddSchemaFile("TestSchema", true);
		*schemaFile << "schema definition" << std::flush;

		// Re-org should be inferred here

		// Finish stage
		builder.finish(registry);

		// Add script
		Script script;
		script.schema = "TestSchema";
		script.app = "TestApp";
		script.executeClass = "JadeScript";
		script.executeMethod = "doSomething";
		builder.addScript(script);

		// Finish deployment
		builder.finish();

		// Check output matches expected
		ApprovalTests::Approvals::verify(fs);
	}
	SECTION("ScriptAfterSchemaDataFileWithInferredReorg")
	{
		// Fake registry
		FakeRegistry registry = FakeRegistry::make_basic();

		// Start deployment & stage
		builder.start();
		builder.start(registry);

		// Add schema data file
		auto schemaDataFile = builder.AddSchemaDataFile("TestSchema", true);
		*schemaDataFile << "schema data" << std::flush;

		// Re-org should be inferred here

		// Finish stage
		builder.finish(registry);

		// Add script
		Script script;
		script.schema = "TestSchema";
		script.app = "TestApp";
		script.executeClass = "JadeScript";
		script.executeMethod = "doSomething";
		builder.addScript(script);

		// Finish deployment
		builder.finish();

		// Check output matches expected
		ApprovalTests::Approvals::verify(fs);
	}
}
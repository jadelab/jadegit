#include "Builder.h"
#include <deploy/build/JARIDeploymentBuilder.h>

using namespace JadeGit;
using namespace JadeGit::Deploy;

void backup_test_scenarios(MemoryFileSystem& fs, JARIDeploymentBuilder& builder)
{
	SECTION("WithBothBackups")
	{
		empty_test_scenario(fs, builder);
	}
	SECTION("WithBothBackupDefeated")
	{
		builder.setDefeatBackups();
		empty_test_scenario(fs, builder);
	}
	SECTION("WithPreBackupDefeated")
	{
		builder.setDefeatPreBackup();
		empty_test_scenario(fs, builder);
	}
	SECTION("WithPostBackupDefeated")
	{
		builder.setDefeatPostBackup();
		empty_test_scenario(fs, builder);
	}
}

TEST_CASE("JARIDeploymentBuilder", "[deploy]")
{
	// Setup builder
	MemoryFileSystem fs;
	JARIDeploymentBuilder builder(fs, "TestEnv", "Test Description");
	builder.setWorkingDirectory("C:\\workdir\\");

	// Common tests
	common_test_scenarios(fs, builder);

	// Deployment options
	SECTION("WithUsername")
	{
		builder.setDeveloperId("TestUsername");
		backup_test_scenarios(fs, builder);
	}
	SECTION("WithoutUsername")
	{
		backup_test_scenarios(fs, builder);
	}
}
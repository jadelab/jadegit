#pragma once
#include <catch2/catch_test_macros.hpp>
#include <jadegit/vfs/MemoryFileSystem.h>
#include <deploy/build/Builder.h>

void empty_test_scenario(JadeGit::MemoryFileSystem& fs, JadeGit::Deploy::Builder& builder);
void common_test_scenarios(JadeGit::MemoryFileSystem& fs, JadeGit::Deploy::Builder& builder);
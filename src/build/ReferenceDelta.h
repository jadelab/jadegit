#pragma once
#include "PropertyDelta.h"

namespace JadeGit::Build
{
	class ReferenceDelta : public PropertyDelta<Data::Reference>
	{
	public:
		using PropertyDelta::PropertyDelta;

	protected:
		bool AnalyzeEnter() final;
		void AnalyzeExit() final;
		
		bool compare(const Data::Reference& previous, const Data::Reference& latest, bool deep) const final;

		Task* handleDeletion(Task* parent) const final;
	};
}
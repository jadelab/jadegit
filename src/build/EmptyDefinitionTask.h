#pragma once
#include "ExtractDefinitionTask.h"

namespace JadeGit::Build
{
	template <PartiallyDefinable TEntity>
	class EmptyDefinitionTask : public ExtractDefinitionTask<TEntity>
	{
	public:
		EmptyDefinitionTask(Graph& graph, Task* parent, const TEntity& entity, bool had_text) : ExtractDefinitionTask<TEntity>(graph, parent, entity, true, false, had_text)
		{
		}

	private:
		bool required(const Task* cascade, bool repeat) const final
		{
			// Suppress repeat "deletions"
			if (repeat)
				return false;

			// Suppress deletions when they're implied by a complete parent task
			// NOTE: This needs to iterate parents to check as there may be an intermediary incomplete/empty placeholder task
			if (cascade)
			{
				assert(this->isChildOf(cascade));

				const Task* parent = this->parent;
				while (parent)
				{
					if (parent->complete)
						return false;

					if (parent == cascade)
						break;

					parent = parent->parent;
				}
			}

			// Explicit "deletion" still required
			return true;
		}
	};
}
#pragma once
#include "SchemaComponentDelta.h"
#include <jadegit/data/ConstantCategory.h>

namespace JadeGit::Build
{
	class ConstantCategoryDelta : public SchemaComponentDelta<ConstantCategory>
	{
	public:
		ConstantCategoryDelta(Graph& graph) : SchemaComponentDelta(graph, "GlobalConstantCategory") {}

	};
}
#pragma once
#include "ReferenceDelta.h"
#include <jadegit/data/Reference.h>

using namespace JadeGit::Data;

namespace JadeGit::Build
{
	bool ReferenceDelta::AnalyzeEnter()
	{
		// Basic analysis
		if (!PropertyDelta::AnalyzeEnter())
		{
			auto deletion = this->GetDeletion();

			// Handle deletion before deleting previous constraint
			this->addDeletionDependency(previous->constraint, deletion);

			// Handle deletion before moving previous type
			if (previous->type)
			{
				if (auto delta = graph.Analyze<IClassDelta>(previous->type->getRootType()))
					delta->getMove()->addPredecessor(deletion);
			}

			return false;
		}

		auto definition = this->GetDefinition();

		if (latest->constraint)
		{
			// Definition depends on latest constraint creation, with reorg if inherited from superschema prior to fix for PAR #69906
			this->addCreationDependency(latest->constraint, definition, latest->constraint->schemaType->schema != latest->schemaType->schema && latest->GetRootSchema().version < jade2022SP3);

			// Definition is dependent on latest constraint
			this->addDefinitionDependency(latest->constraint, definition);
		}

		// Handle update before deleting previous constraint
		if (previous)
			this->addDeletionDependency(previous->constraint, definition);

		// Handle definition before/after type moves
		auto previousType = previous && previous->type ? graph.Analyze<IClassDelta>(previous->type->getRootType()) : nullptr;
		auto latestType = latest && latest->type ? graph.Analyze<IClassDelta>(latest->type->getRootType()) : nullptr;

		if (previousType && previousType != latestType)
		{
			if (auto previousTypeLatest = previousType->getLatest(); previousTypeLatest && previousTypeLatest->inheritsFrom(latest->type))
			{
				// Handle definition after previous type is moved under latest type
				definition->addPredecessor(previousType->getMove());
			}
			else
			{
				// Handle definition before previous type is moved
				previousType->getMove()->addPredecessor(definition);
			}
		}

		if (latestType && latestType != previousType)
		{
			// Handle definition after latest type is moved
			definition->addPredecessor(latestType->getMove());
		}

		return true;
	}

	void ReferenceDelta::AnalyzeExit()
	{
		PropertyDelta::AnalyzeExit();

		// Definition is peer of inverse definitions
		// Handled during post-analysis to prevent circular analysis errors when multiple member keys are inverses of collection references
		if (auto definition = this->GetDefinition())
		{
			if (latest->isExplicitInverseRef())
			{
				auto inverses = static_cast<ExplicitInverseRef*>(latest)->getInverseReferences();

				for (auto inverse : inverses)
				{
					if (auto delta = graph.tryAnalyze(inverse))
						definition->addPeer(delta->GetDefinition());
				}
			}
		}
	}

	bool ReferenceDelta::compare(const Reference& previous, const Reference& latest, bool deep) const
	{
		// Handle explicit inverse reference comparison
		if (previous.isExplicitInverseRef())
		{
			// Check reference hasn't been converted
			if (!latest.isExplicitInverseRef())
				return false;	

			// Quick check if number of inverses have changed
			if (static_cast<const ExplicitInverseRef&>(previous).inverses.size() != static_cast<const ExplicitInverseRef&>(latest).inverses.size())
				return false;

			// Compare reference details
			if (!PropertyDelta::compare(previous, latest, deep))
				return false;

			// Retrieve previous/latest inverse references
			auto previous_inverses = static_cast<const ExplicitInverseRef&>(previous).getInverseReferences();
			auto latest_inverses = static_cast<const ExplicitInverseRef&>(latest).getInverseReferences();

			// Match each previous inverse, looking for any match in latest to ignore inconsistent order
			for (auto previous_inverse : previous_inverses)
			{
				bool matched = false;

				for (auto iter = latest_inverses.begin(); iter != latest_inverses.end(); iter++)
				{
					if (match(previous_inverse, *iter))
					{
						latest_inverses.erase(iter);
						matched = true;
						break;
					}
				}

				if (!matched)
					return false;
			}

			// Sanity check (all should've been matched/cleared above)
			if (!latest_inverses.empty())
				return false;

			// Everything appears the same
			return true;
		}
		
		// Check reference hasn't been converted
		if (latest.isExplicitInverseRef())
			return false;

		// Compare basic details for implicit references
		return PropertyDelta::compare(previous, latest, deep);
	}

	Task* ReferenceDelta::handleDeletion(Task* parent) const
	{
		// Control properties cannot be deleted explicitly, but can be implicitily deleted by complete parent definition
		if (previous->isImplicitInverseRef() && static_cast<ImplicitInverseRef*>(previous)->isFormControlProperty())
		{
			assert(parent && parent->complete);
			return parent;
		}

		return PropertyDelta::handleDeletion(parent);
	}
}
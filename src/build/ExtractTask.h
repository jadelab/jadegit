#pragma once
#include "FileTask.h"
#include "ExtractStrategy.h"

namespace JadeGit::Build
{
	class ExtractTask : public FileTask
	{
	public:
		using FileTask::FileTask;
		ExtractTask(Graph& graph, Task* parent, bool complete, short priority = 0) : ExtractTask(graph, parent, LoadStyle::Latest, complete, priority) {}

		virtual bool execute(ExtractStrategy& strategy) const = 0;

	protected:
		bool accept(TaskVisitor& v) const final;
		bool required(const Task* cascade, bool repeat) const;
	};
}
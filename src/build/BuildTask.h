#pragma once
#include "Task.h"
#include <build/Builder.h>

namespace JadeGit::Build
{
	class BuildTask : public Task
	{
	public:
		BuildTask(Graph& graph) : Task(graph) {}

		virtual bool execute(Builder& builder) const = 0;

	protected:
		bool accept(TaskVisitor& v) const final;
	};
}
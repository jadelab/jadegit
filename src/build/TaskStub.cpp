#include "TaskStub.h"
#include "Graph.h"
#include <jadegit/data/Entity.h>

using namespace std;
using namespace JadeGit::Data;

namespace JadeGit::Build
{
	TaskStub* TaskStub::make(Graph& graph, Task* parent, const Data::Entity* entity)
	{
		if (!entity)
			return nullptr;

		if (TaskStub* task = graph.stubs[entity])
		{
			assert(task->parent == parent);
			return task;
		}

		return graph.stubs[entity] = new TaskStub(graph, parent, *entity);
	}

	TaskStub::TaskStub(Graph& graph, Task* parent, const Data::Entity& entity) : Task(graph, parent), entity(entity)
	{
	}

	TaskStub::operator string() const
	{
		return format("{} {}", entity.dataClass->name.c_str(), entity.getQualifiedName());
	}
}
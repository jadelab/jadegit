#pragma once
#include "Task.h"

namespace JadeGit::Build
{
	class EmptyTask : public Task
	{
	public:
		EmptyTask(Graph& graph, Task* parent, const char* description = "empty") : Task(graph, parent), 
			description(description)
		{
		}

		operator std::string() const final
		{
			return description;
		}

	private:
		const char* description;

		bool accept(TaskVisitor& v) const final { return true; };
	};
}
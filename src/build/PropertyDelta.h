#pragma once
#include "FeatureDelta.h"
#include "ClassDelta.h"

namespace JadeGit::Build
{
	class IPropertyDelta : public IFeatureDelta
	{
	public:
		using IFeatureDelta::IFeatureDelta;

		const Data::Property* getPrevious() const override = 0;
		const Data::Property* getLatest() const override = 0;
	};

	template<class TComponent, class TInterface = IPropertyDelta>
	class PropertyDelta : public FeatureDelta<TComponent, TInterface>
	{
	public:
		PropertyDelta(Graph& graph) : FeatureDelta<TComponent, TInterface>(graph, "Property") {}

		using FeatureDelta<TComponent, TInterface>::previous;
		using FeatureDelta<TComponent, TInterface>::latest;

	protected:
		using FeatureDelta<TComponent, TInterface>::graph;
		using FeatureDelta<TComponent, TInterface>::GetDefinition;
		using FeatureDelta<TComponent, TInterface>::GetDeletion;

		bool AnalyzeEnter() override
		{
			// Basic analysis
			if (!FeatureDelta<TComponent, TInterface>::AnalyzeEnter())
			{
				// Handle deletion before moving previous schema type
				if (auto delta = graph.Analyze<IClassDelta>(previous->schemaType))
					delta->getMove()->addPredecessor(GetDeletion());
			
				return false;
			}

			// Definition dependent on latest schema type being moved
			if (previous)
			{
				if (auto delta = graph.Analyze<IClassDelta>(latest->schemaType))
					GetDefinition()->addPredecessor(delta->getMove());
			}

			return true;
		}
	};
}
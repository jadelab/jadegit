#pragma once
#include "SchemaComponentDelta.h"
#include <jadegit/data/JadeExposedList.h>

namespace JadeGit::Build
{
	class JadeExposedListDelta : public SchemaComponentDelta<Data::JadeExposedList>
	{
	public:
		JadeExposedListDelta(Graph& graph);

	protected:
		bool AnalyzeEnter() final;

		Task* handleDefinition(Task* parent) final;
	};
}
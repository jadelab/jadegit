#pragma once
#include "ClassDelta.h"
#include <jadegit/data/CollClass.h>

namespace JadeGit::Build
{
	class CollClassDelta : public ClassDelta<CollClass>
	{
	public:
		using ClassDelta::ClassDelta;
	
	protected:
		bool AnalyzeEnter() final
		{
			// Basic analysis
			if (!ClassDelta::AnalyzeEnter())
			{
				// Handle deletion before member type deletion
				if (previous->memberType)
					addDeletionDependency(previous->memberType->getRootType(), GetDeletion());

				// Handle deletion before key properties/types
				for (auto& key : previous->keys)
				{
					if (auto memberKey = dynamic_cast<MemberKey*>(key))
					{
						addDeletionDependency(memberKey->property, GetDeletion());
					}
					else if (auto externalKey = dynamic_cast<ExternalKey*>(key))
					{
						addDeletionDependency(externalKey->type, GetDeletion());
					}
				}

				return false;
			}

			// Declaration depends on latest member type
			if (latest->memberType)
				addCreationDependency(latest->memberType->getRootType(), GetDeclaration());

			// Deleting prior member type depends on update to new member type
			if (previous && previous->memberType)
				addDeletionDependency(previous->memberType->getRootType(), GetDeclaration());

			// Deleting prior key properties/types depends on definition update
			if (previous)
			{
				for (auto& key : previous->keys)
				{
					if (auto memberKey = dynamic_cast<MemberKey*>(key))
					{
						addDeletionDependency(memberKey->property, GetDefinition());
					}
					else if (auto externalKey = dynamic_cast<ExternalKey*>(key))
					{
						addDeletionDependency(externalKey->type, GetDefinition());
					}
				}
			}

			// Defining keys depends on superclass key definition
			if (latest->superclass)
				addDefinitionDependency(latest->superclass->getRootType(), GetDefinition());

			return true;
		}

		void AnalyzeExit() final
		{
			ClassDelta::AnalyzeExit();

			// Definition is dependent on key properties/types
			// Handled during post-analysis to prevent circular analysis errors when member keys are inverses of collection references
			if (auto definition = GetDefinition())
			{
				for (auto& key : latest->keys)
				{
					if (auto memberKey = dynamic_cast<MemberKey*>(key))
					{
						addDefinitionDependency(memberKey->property, definition);
					}
					else if (auto externalKey = dynamic_cast<ExternalKey*>(key))
					{
						addCreationDependency(externalKey->type, definition);
					}
				}
			}
		}
		
		// Determine if complete definition is required
		bool requiresCompleteDefinition(const Task* parent) const final
		{
			return ClassDelta::requiresCompleteDefinition(parent) || isChangingKeys();
		}

		// Determine if forward declaration is required
		bool requiresDeclaration(const Task* parent) const final
		{
			// Check base implementation
			if (ClassDelta::requiresDeclaration(parent))
				return true;

			// Not applicable to subschema copies
			if (latest->isSubschemaCopy())
				return false;

			// Required if member type or other collection header details are changing
			return previous->blockSize != latest->blockSize ||
				previous->duplicatesAllowed != latest->duplicatesAllowed ||
				previous->expectedPopulation != latest->expectedPopulation ||
				previous->loadFactor != latest->loadFactor ||
				previous->memberTypePrecision != latest->memberTypePrecision ||
				previous->memberTypeScaleFactor != latest->memberTypeScaleFactor ||
				previous->memberTypeSize != latest->memberTypeSize ||
				previous->memberTypeWSDLName != latest->memberTypeWSDLName ||
				!match<Type>(previous->memberType, latest->memberType);
		}

	private:
		bool isChangingKey(const MemberKey* previous, const MemberKey* latest) const
		{
			if (!previous || !latest)
				return true;

			if (!match<Property>(previous->property, latest->property))
				return true;

			if (previous->keyPath.size() != latest->keyPath.size())
				return true;

			auto previous_iter = previous->keyPath.begin();
			auto latest_iter = latest->keyPath.begin();
			while (previous_iter != previous->keyPath.end() && latest_iter != latest->keyPath.end())
			{
				if (!match<Property>(*previous_iter, *latest_iter))
					return true;

				previous_iter++;
				latest_iter++;
			}

			return false;
		}

		bool isChangingKey(const ExternalKey* previous, const ExternalKey* latest) const
		{
			if (!previous || !latest)
				return true;

			if (previous->length != latest->length ||
				previous->precision != latest->precision ||
				previous->scaleFactor != latest->scaleFactor)
				return true;

			return !match<Type>(previous->type, latest->type);
		}

		bool isChangingKey(const Key* previous, const Key* latest) const
		{
			if (!previous || !latest)
				return true;

			if (previous->caseInsensitive != latest->caseInsensitive ||
				previous->descending != latest->descending ||
				previous->sortOrder != latest->sortOrder)
				return true;

			if (auto member_key = dynamic_cast<const MemberKey*>(previous))
				return isChangingKey(member_key, dynamic_cast<const MemberKey*>(latest));
			else if (auto external_key = dynamic_cast<const ExternalKey*>(previous))
				return isChangingKey(external_key, dynamic_cast<const ExternalKey*>(latest));

			throw std::logic_error("Unhandled key type");
		}

		bool isChangingKeys() const
		{
			if (!previous || !latest)
				return false;

			if (previous->keys.size() != latest->keys.size())
				return true;

			auto previous_iter = previous->keys.begin();
			auto latest_iter = latest->keys.begin();
			while (previous_iter != previous->keys.end() && latest_iter != latest->keys.end())
			{
				if (isChangingKey(*previous_iter, *latest_iter))
					return true;

				previous_iter++;
				latest_iter++;
			}

			return false;
		}
	};
}
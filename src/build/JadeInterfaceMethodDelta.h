#pragma once
#include "MethodDelta.h"
#include <jadegit/data/JadeInterfaceMethod.h>

namespace JadeGit::Build
{
	class JadeInterfaceMethodDelta : public MethodDelta<JadeInterfaceMethod>
	{
	public:
		using MethodDelta<JadeInterfaceMethod>::MethodDelta;

	protected:
		Task* handleDefinition(Task* parent) final
		{
			// Resolve original interface method
			auto& original = latest->getOriginal();

			// Define original methods
			if (latest == &original)
				return MethodDelta<JadeInterfaceMethod>::handleDefinition(parent);

			// Use empty task for interface method copies, which are not explicitly defined, but are dependent on originals
			auto task = new EmptyTask(this->graph, parent);
			
			// Dependent on original method being created/renamed
			this->addCreationDependency(original, task);

			return task;
		}
	};
}
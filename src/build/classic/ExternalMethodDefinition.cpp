#include "MethodDefinition.h"
#include "TypeDefinition.h"

namespace JadeGit::Build::Classic
{
	class ExternalMethodDefinition : public MethodDefinition
	{
	public:
		ExternalMethodDefinition(TypeDefinition& type, const Data::ExternalMethod& method) : MethodDefinition(type, method)
		{
			type.externalMethodDefinitions.push_back(this);
		}
	};

	static RoutineDefinitionRegistration<ExternalMethodDefinition, Data::ExternalMethod> registrar;
}
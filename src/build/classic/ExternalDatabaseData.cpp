#include "DataNode.h"
#include "SchemaDefinition.h"
#include <jadegit/data/ExternalDatabase.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/ExternalDatabaseMeta.h>
#include <Platform.h>

using namespace JadeGit::Data;

namespace JadeGit::Build::Classic
{
	template <class TNode, class TContainer>
	void PrintChildren(XMLPrinter& printer, const char* element, const TContainer& children)
	{
		printer.OpenElement(element);
		for (auto& child : children)
			TNode(*child.second).Print(printer);
		printer.CloseElement();
	}

	class ExternalData : public DataNode
	{
	public:
		using DataNode::DataNode;

	protected:
		void PrintAttributes(XMLPrinter& printer) const override {}
	};

	class ExternalColumnData : public ExternalData
	{
	public:
		ExternalColumnData(const ExternalColumn& source) : ExternalData(source) {}
	};

	class ExternalIndexData : public ExternalData
	{
	public:
		ExternalIndexData(const ExternalIndex& source) : ExternalData(source) {}

	protected:
		void PrintData(XMLPrinter& printer) const final
		{
			ExternalData::PrintData(printer);

			auto& source = static_cast<const ExternalIndex&>(this->source);

			printer.OpenElement("keys");
			for (auto& key : source.keys)
				ExternalData(*key).Print(printer);
			printer.CloseElement();
		}
	};

	class ExternalTableData : public ExternalData
	{
	public:
		ExternalTableData(const ExternalTable& source) : ExternalData(source) {}

	protected:
		void PrintData(XMLPrinter& printer) const final
		{
			ExternalData::PrintData(printer);

			auto& source = static_cast<const ExternalTable&>(this->source);

			PrintChildren<ExternalData>(printer, "columns", source.columns);

			printer.OpenElement("primaryKey");	// TODO: Need test case
			printer.CloseElement();

			PrintChildren<ExternalData>(printer, "foreignKeys", source.foreignKeys);
			PrintChildren<ExternalIndexData>(printer, "indexes", source.indexes);

			printer.OpenElement("specialColumns");
			for (auto& column : source.specialColumns)
			{
				printer.OpenElement("ExternalColumn");
				printer.PushAttribute("name", column->name.c_str());
				printer.CloseElement();
			}
			printer.CloseElement();
		}

		bool PrintPropertyFilter(const RootSchema& rootSchema, const Property* property) const final
		{
			// Special columns handled explicitly above (after columns have been defined)
			if (property == rootSchema.externalTable->specialColumns)
				return false;

			return ExternalData::PrintPropertyFilter(rootSchema, property);
		}
	};

	class ExternalAttributeMapData : public ExternalData
	{
	public:
		ExternalAttributeMapData(const ExternalAttributeMap& source) : ExternalData(source) {}

	protected:
		void PrintData(XMLPrinter& printer) const final
		{
			ExternalData::PrintData(printer);

			auto& source = static_cast<const ExternalAttributeMap&>(this->source);

			printer.OpenElement("column");
			if (source.column)
			{
				auto& column = *source.column;
				printer.PushAttribute("name", column.name.c_str());
				printer.PushAttribute("table", column.table->name.c_str());
			}
			printer.CloseElement();
		}

		bool PrintPropertyFilter(const RootSchema& rootSchema, const Property* property) const final
		{
			// Exclude parent reference
			if (property == rootSchema.externalAttributeMap->classMap)
				return false;

			// Exclude column handled explicitly above
			if (property == rootSchema.externalAttributeMap->column)
				return false;

			return ExternalData::PrintPropertyFilter(rootSchema, property);
		}
	};

	class ExternalReferenceMapData : public ExternalData
	{
	public:
		ExternalReferenceMapData(const ExternalReferenceMap& source) : ExternalData(source) {}

	protected:
		void PrintAttributes(XMLPrinter& printer) const final
		{
			auto& source = static_cast<const ExternalReferenceMap&>(this->source);

			printer.PushAttribute("id", source.id);
		}

		void PrintData(XMLPrinter& printer) const final
		{
			ExternalData::PrintData(printer);

			auto& source = static_cast<const ExternalReferenceMap&>(this->source);

			PrintColumns(printer, "leftColumns", source.leftColumns);

			printer.OpenElement("foreignKey");	// TODO: Need test case
			printer.CloseElement();

			printer.OpenElement("otherRefMap");
			if (source.otherRefMap)
				printer.PushAttribute("id", source.otherRefMap->id);
			printer.CloseElement();

			printer.OpenElement("primaryKey");	// TODO: Need test case
			printer.CloseElement();

			PrintColumns(printer, "rightColumns", source.rightColumns);
		}

		bool PrintPropertyFilter(const RootSchema& rootSchema, const Property* property) const final
		{
			// Exclude parent reference
			if (property == rootSchema.externalReferenceMap->classMap)
				return false;

			// Exclude properties handled explicitly above
			if (property == rootSchema.externalReferenceMap->leftColumns ||
				property == rootSchema.externalReferenceMap->otherRefMap ||
				property == rootSchema.externalReferenceMap->rightColumns)
				return false;

			return ExternalData::PrintPropertyFilter(rootSchema, property);
		}

	private:
		void PrintColumns(XMLPrinter& printer, const char* element, const Array<ExternalColumn*>& columns) const
		{
			printer.OpenElement(element);
			for (const ExternalColumn* column : columns)
			{
				printer.OpenElement("ExternalColumn");
				printer.PushAttribute("name", column->name.c_str());
				printer.PushAttribute("table", column->table->name.c_str());
				printer.CloseElement();
			}
			printer.CloseElement();
		}
	};

	class ExternalClassMapData : public ExternalData
	{
	public:
		ExternalClassMapData(const ExternalClassMap& source) : ExternalData(source) {}

	protected:
		void PrintData(XMLPrinter& printer) const final
		{
			ExternalData::PrintData(printer);

			auto& source = static_cast<const ExternalClassMap&>(this->source);

			if (source.schemaEntity)
			{
				printer.OpenElement("schemaEntity");
				DataNode::PrintData(printer, source.schemaEntity);
				printer.CloseElement();
			}

			PrintChildren<ExternalAttributeMapData>(printer, "attributeMaps", source.attributeMaps);

			// Suppress reference maps prior to JADE 2022
			// Needed to avoid issues where ExternalReferenceMap property elements were previously extracted with a leading slash, which couldn't be reloaded (invalid XML)
			if (source.GetRootSchema().version >= jade2022)
				PrintChildren<ExternalReferenceMapData>(printer, "referenceMaps", source.referenceMaps);
		
			printer.OpenElement("tables");
			for (auto& table : source.tables)
			{
				printer.OpenElement("ExternalTable");
				printer.PushAttribute("name", table->name.c_str());
				printer.CloseElement();
			}
			printer.CloseElement();
		}

		bool PrintPropertyFilter(const RootSchema& rootSchema, const Property* property) const final
		{
			// Suppress schema entity reference (handled explicitly above)
			if (property == rootSchema.externalSchemaMap->schemaEntity)
				return false;

			// Suppress modified by details
			if (!property)
				return false;
			
			// Include parent reference
			if (property == rootSchema.externalClassMap->database)
				return true;

			// Include schema entity name
			if (property == rootSchema.schemaEntity->name)
				return true;

			// Include schema entity properties defined by external class
			if (property->schemaType != *rootSchema.externalClass)
				return false;

			return DataNode::PrintPropertyFilter(rootSchema, property);
		}
	};

	class ExternalCollClassMapData : public ExternalData
	{
	public:
		ExternalCollClassMapData(const ExternalCollClassMap& source) : ExternalData(source) {}

	protected:
		void PrintData(XMLPrinter& printer) const final
		{
			ExternalData::PrintData(printer);

			auto& source = static_cast<const ExternalSchemaMap&>(this->source);

			if (source.schemaEntity)
			{
				printer.OpenElement("schemaEntity");
				DataNode::PrintData(printer, source.schemaEntity);
				printer.CloseElement();
			}
		}

		bool PrintPropertyFilter(const RootSchema& rootSchema, const Property* property) const final
		{
			// Suppress schema entity reference (handled explicitly above)
			if (property == rootSchema.externalSchemaMap->schemaEntity)
				return false;

			// Suppress modified by details
			if (!property)
				return false;

			// Include parent reference
			if (property == rootSchema.externalCollClassMap->database)
				return true;

			// Include schema entity name
			if (property == rootSchema.schemaEntity->name)
				return true;

			// Include schema entity properties defined by external collection class
			if (property->schemaType != *rootSchema.externalCollClass)
				return false;

			return DataNode::PrintPropertyFilter(rootSchema, property);
		}
	};

	class ExternalDatabaseData : public DataNode
	{
	public:
		using parent_node = SchemaDefinition;

		ExternalDatabaseData(SchemaDefinition& schema, const ExternalDatabase& database) : DataNode(schema.data, database) {}

	protected:
		void PrintAttributes(XMLPrinter& printer) const final
		{
			auto& source = static_cast<const ExternalDatabase&>(this->source);

			printer.PushAttribute("name", source.name.c_str());
		}

		void PrintData(XMLPrinter& printer) const final
		{
			DataNode::PrintData(printer);

			auto& source = static_cast<const ExternalDatabase&>(this->source);
			
			printer.OpenElement("_dbConnection");
			printer.CloseElement();

			printer.OpenElement("_modification");
			printer.CloseElement();

			printer.OpenElement("_modified");
			printer.PushText(false);
			printer.CloseElement();

			printer.OpenElement("_lockProxy");
			printer.PushAttribute("name", source.name.c_str());
			printer.CloseElement();

			printer.OpenElement("_profile");
			if (source.profile)
				ExternalData(source.profile).Print(printer);
			printer.CloseElement();

			printer.OpenElement("_driverInfo");
			if (source.driverInfo)
				ExternalData(source.driverInfo).Print(printer);
			printer.CloseElement();

			PrintChildren<ExternalTableData>(printer, "_tables", source.tables);

			printer.OpenElement("_storedProcs");									// TODO: Need test case
			printer.CloseElement();

			PrintChildren<ExternalClassMapData>(printer, "_classMaps", source.classMaps);
			PrintChildren<ExternalCollClassMapData>(printer, "_collClassMaps", source.collClassMaps);
		}
	};
	static NodeRegistration<ExternalDatabaseData, ExternalDatabase> registrar;
}
#include "TypeHeader.h"
#include "SchemaDefinition.h"
#include "MembershipDefinition.h"
#include <jadegit/data/ExternalCollClass.h>

namespace JadeGit::Build::Classic
{
	class CollClassHeader : public TypeHeader
	{
	public:
		CollClassHeader(SchemaDefinition& schema, const Data::CollClass& klass) : TypeHeader(schema, klass)
		{
			if (klass.memberType)
				NodeFactory<MembershipDefinition>::get().create(std::type_index(typeid(klass)), schema, klass);
		}

	protected:
		void WriteAttributes(std::ostream& output) override
		{
			auto& source = static_cast<const Data::CollClass&>(this->source);

			WriteAttribute(output, "duplicatesAllowed", source.duplicatesAllowed);
			WriteAttribute(output, "blockSize", source.blockSize);
			WriteAttribute(output, "expectedPopulation", source.expectedPopulation);
			WriteAttribute(output, "loadFactor", source.loadFactor);
			WriteAttribute(output, "memberTypeWSDLName", source.memberTypeWSDLName);

			TypeHeader::WriteAttributes(output);
		}
	};
	static NodeRegistration<CollClassHeader, Data::CollClass> collClass;
	static NodeRegistration<CollClassHeader, Data::ExternalCollClass> externalCollClass;
}
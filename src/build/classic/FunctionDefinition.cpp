#include "RoutineDefinition.h"
#include "SchemaDefinition.h"
#include <jadegit/data/Function.h>

using namespace JadeGit::Data;

namespace JadeGit::Build::Classic
{
	class FunctionDefinition : public RoutineDefinition
	{
	public:
		using parent_node = SchemaDefinition;

		FunctionDefinition(SchemaDefinition& schema, const Function& function) : RoutineDefinition(function)
		{
			schema.externalFunctionDefinitions.push_back(this);
		}
	};

	static RoutineDefinitionRegistration<FunctionDefinition, Function> registrar;
}
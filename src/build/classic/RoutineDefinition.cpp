#include "RoutineDefinition.h"
#include "RoutineSource.h"
#include <jadegit/data/Library.h>
#include <jadegit/data/ScriptElement.h>

using namespace JadeGit::Data;

namespace JadeGit::Build::Classic
{
	void RoutineDefinition::WriteEnter(std::ostream& output, const std::string& indent)
	{
		FeatureDefinition::WriteEnter(output, indent);

		output << "(";

		size_t size = source.parameters.size();
		size_t index = 0;
		for (Parameter* param : source.parameters)
		{
			if (size > 1)
			{
				if (++index > 1)
					output << ";";
				output << "\n" << indent << "\t";
			}

			output << param->name << ": " << param->GetType()->GetLocalName();
			if (param->length)
				output << "[" << param->length << "]";

			if (param->usage)
				if (param->usage == Parameter::Input)
					output << " input";
				else if (param->usage == Parameter::IO)
					output << " io";
				else if (param->usage == Parameter::Output)
					output << " output";
		}

		output << ")";

		if (source.returnType && source.returnType->type)
		{
			output << ": " << source.returnType->type->GetLocalName();
			if (source.returnType->length)
				output << "[" << source.returnType->length << "]";
		}

		if (!source.entrypoint.empty())
			output << " is \"" << source.entrypoint << "\"";

		if (source.library)
			output << " in \"" << source.library->name << "\"";
	}

	void RoutineDefinition::WriteAttributes(std::ostream& output)
	{
		FeatureDefinition::WriteAttributes(output);

		// Script
		WriteAttribute(output, "notImplemented", source.notImplemented);

		WriteAttribute(output, "serverExecution", source.executionLocation == Routine::ExecutionLocation::Server);
		WriteAttribute(output, "clientExecution", source.executionLocation == Routine::ExecutionLocation::Client);
		WriteAttribute(output, "applicationServerExecution", source.executionLocation == Routine::ExecutionLocation::ApplicationServer);
		WriteAttribute(output, "presentationClientExecution", source.executionLocation == Routine::ExecutionLocation::PresentationClient);
	}
}
#include "Node.h"

namespace JadeGit::Build::Classic
{
	Node::~Node()
	{
		for (auto& pair : children)
			delete pair.second;
	}
}
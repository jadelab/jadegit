#pragma once
#include "Node.h"

namespace JadeGit::Build::Classic
{
	class DefinitionNode : public Node
	{
	public:
		using base_node = DefinitionNode;

		void Write(std::ostream& output, const std::string& indent = "");

	protected:
		virtual void WriteEnter(std::ostream& output, const std::string& indent) {}
		virtual void WriteBody(std::ostream& output, const std::string& indent) = 0;
		virtual void WriteExit(std::ostream& output, const std::string& indent) {}

		void WriteAttribute(std::ostream& output, const char* attribute, bool condition);
		void WriteAttribute(std::ostream& output, const char* attribute, int value);
		void WriteAttribute(std::ostream& output, const char* attribute, int value, bool condition);
		void WriteAttribute(std::ostream& output, const char* attribute, const char* value);
		void WriteAttribute(std::ostream& output, const char* attribute, const std::string& value);

	private:
		bool delim = false;
	};

	class DefinitionNodes : public std::vector<DefinitionNode*>
	{
	public:
		DefinitionNodes(const char* heading, bool bracketed = false) : heading(heading), bracketed(bracketed) {}

		void Write(std::ostream& output, const std::string& indent = "") const;

	private:
		const char* heading;
		bool bracketed;
	};
}
#pragma once
#include "Storage.h"
#include <jadegit/vfs/File.h>
#include <jadegit/vfs/FileSystem.h>

namespace JadeGit::Registry
{
	class FileStorage : public Storage
	{
	public:
		FileStorage(std::filesystem::path path, bool mustexist = true);
		FileStorage(std::unique_ptr<FileSystem> fs, std::filesystem::path path, bool mustexist = true);
		FileStorage(File file, bool mustexist = true);

	protected:
		File file;
		bool mustexist = true;

		void load(Root& registry) const final;
		void save(const uint8_t* buffer, size_t length) const final;

	private:
		std::unique_ptr<FileSystem> fs;
	};
}
#pragma once
#include <registry/Data_generated.h>

namespace JadeGit::Registry
{
	inline SchemaT make_schema(const std::string& name)
	{
		SchemaT schema;
		schema.name = name;
		return schema;
	}

	inline bool operator==(const SchemaT& lhs, const SchemaT& rhs)
	{
		return lhs.name == rhs.name;
	}
}
#pragma once
#include "Visitor.h"

namespace JadeGit::Registry
{
	class Validator : public Visitor
	{
	protected:
		bool visitEnter(const RepositoryT& repo) final;
		bool visit(const SchemaT& schema, bool first) final;

	private:
		std::set<std::string> repos;
		std::set<std::string> origins;
		std::set<std::string> schemas;
	};
}
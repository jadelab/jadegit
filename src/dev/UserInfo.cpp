#include "Development.h"
#include "MsgBox.h"
#include "Session.h"
#include <schema/install/Install.h>
#include <jade/AppContext.h>
#include <Exception.h>
#include <Log.h>
#include <Platform.h>

using namespace std;
using namespace Jade;
using namespace JadeGit;

namespace JadeGit::Development
{
	int jadeUserInfo(const Character* pUserName, const Character* pPassword)
	{
		try
		{
			// Ignore any monitor schema apps
			if (AppContext::currentSchema() == JadeMonitorSchemaOid)
				return JADEGIT_ALLOW;

			auto userName = narrow(pUserName);

			log(format("UserInfo: userName={}", userName));

			// Check library is not being called in server-only execution
			if (AppContext::GetNodeType() == Node_Type::DATABASE_SERVER)
				throw unsupported_feature("Running the IDE in multi-user mode", jedi402);

			// Check patch control hooks are enabled
			if (!AppContext::GetIniSettingBoolean(TEXT("JadeSecurity"), TEXT("JadePatchControlSecurity"), false))
				throw runtime_error("Using JadeGit depends on the JADE IDE patch control security being enabled to track changes.\n\n\
The JadeSecurity.JadePatchControlSecurity INI setting needs to be set to true.");

			// Check schema is up to date
			Schema::install();

			// Setup session and sign-on
			return Session::signOn(userName) ? JADEGIT_ALLOW : JADEGIT_CANCEL;
		}
		catch (exception& e)
		{
			MsgBox::display(e);
		}

		return JADEGIT_ERROR;
	};
}
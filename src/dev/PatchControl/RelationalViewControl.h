#pragma once
#include "EntityControl.h"

namespace JadeGit::Development
{
	class RelationalViewControl : public EntityControl
	{
	public:
		RelationalViewControl();

	protected:
		bool execute(Session& session, const std::string& entityType, const QualifiedName& entityName, const std::string& operation) final;
	};
}
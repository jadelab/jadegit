#include "SchemaControl.h"
#include <dev/Development.h>
#include <dev/Command.h>
#include <dev/MsgBox.h>
#include <extract/Schema.h>
#include <jade/Transaction.h>
#include <schema/data/Change.h>
#include <schema/deploy/DeploymentLock.h>
#include <Exception.h>
#include <Platform.h>
#include <Log.h>

using namespace std;
using namespace Jade;
using namespace JadeGit::Schema;

namespace JadeGit::Development
{
	const char* get_status_description(SchemaStatus status)
	{
		switch (status)
		{
		case SchemaStatus::SCHEMA_STATUS_NONE:
			return "None";
#if JADE_BUILD_VERSION_BUILD >= 22
		case SchemaStatus::SCHEMA_BEING_CREATED:
			return "Being Created";
#endif
		case SchemaStatus::SCHEMA_BEING_DELETED:
			return "Being Deleted";
		case SchemaStatus::SCHEMA_PARTLY_DELETED:
			return "Partly Deleted";
		case SchemaStatus::SCHEMA_DELETING_SELF:
			return "Deleting Self";
		case SchemaStatus::SCHEMA_UNVERSIONABLE:
			return "Unversionable";
		default:
			return "Unknown";
		}
	}

	bool SchemaControl::prelude(const ClassNumber& entityType, const QualifiedName& entityName, bool removal)
	{
		// Passthrough on reset
		if (!entityType)
			return EntityControl::prelude(entityType, entityName, removal);

		// Get schema name
		auto& name = entityName.first().name;

		// Query schema status, setting up if required
		switch (status(name, !removal))
		{
		case GitSchema::Status::Excluded:
		{
			// Reset rest of chain and ignore
			return EntityControl::prelude(0, string(), removal);
		}
		case GitSchema::Status::Included:
		{
			// Pass onto next in chain if schema isn't being modified
			if (entityType != DSKSCHEMA)
				return PatchControl::prelude(entityType, entityName, removal);
			else
				return EntityControl::prelude(entityType, entityName, removal);
		}
		default:
			return false;
		}
	}

	bool SchemaControl::execute(Session& session, const string& entityType, const QualifiedName& entityName, const string& operation)
	{
		// Passthrough control rename operations (entityName is not prefixed with schema, so can't be checked below)
		if (operation == "CR")
			return PatchControl::execute(session, entityType, entityName, operation);
		
		// Get schema name
		auto& name = entityName.first().name;

		// Determine if entity is being added or deleted
		bool adding = operation.front() == 'A';
		bool deleting = operation.front() == 'D';

		// Handle child entity operations
		if (entityType != "Schema")
		{
			// Query schema source control status, setting up if not deleting, prompting to add if required
			switch (status(name, !deleting))
			{
			case GitSchema::Status::Excluded:

				// Suppress source control when schema has been excluded
				return true;

			case GitSchema::Status::Included:

				// Pass onto next in chain
				return PatchControl::execute(session, entityType, entityName, operation);

			case GitSchema::Status::Unknown:

				// Ignore child entity deletions for unregistered schemas, which may be part of deleting the whole schema
				// Instead, we rely on add/update operations to prompt the initial setup of an unknown schema
				return deleting;

			default:
				return false;
			}
		}

		// Resolve actual schema & status
		auto actual = Extract::Schema::get(entityName);
		auto status = actual.getStatus();
		LOG_DEBUG("Schema Status: " << get_status_description(status) << " (" << status << ")");

		// Query schema source control status, setting up if not deleting
		switch (this->status(name, !deleting, adding))
		{
		case GitSchema::Status::Excluded:
		{
			// Forget excluded schemas on deletion
			if (deleting && status == SchemaStatus::SCHEMA_DELETING_SELF)
				GitSchema::forget(name);

			// Suppress source control when schema has been excluded
			return true;
		}
		case GitSchema::Status::Included:
		{
			// Handle deletion
			if (deleting)
			{
				// Schema deletion unsupported when it cannot be tracked atomically in same transaction (PAR #68417)
				if (!Transaction::InTransactionState() && jadeVersion < jade2022SP3)
					throw unsupported_feature("Deleting schemas", jade2022SP3);

				// Prevent changes while deploying
				DeploymentLock lock;

				// Resolve schema
				if (auto schema = GitSchema::resolve(actual, name))
				{
					// TODO: Entity lock to single thread worktree/entity operations

					// Ensure schema can be deleted during first phase
					if (status <= SchemaStatus::SCHEMA_BEING_DELETED)
					{
						if (!schema->getChange().isAdd())
							GitChange::dele(*schema, actual);
					}

					// Cleanup latest schema changes during second phase
					if (status <= SchemaStatus::SCHEMA_PARTLY_DELETED)
					{
						schema->unversion();
					}

					// Apply deletion during last phase hook when schema instance finally deleted
					if (status == SchemaStatus::SCHEMA_DELETING_SELF)
					{
						if (GitChange::dele(*schema, actual).isVoid())
						{
							// TODO: Cleanup redundant schema record (ready to be recreated/linked to another repo)
						}
					}
				}

				return true;
			}

			// Pass onto base implementation to handle create/update operations
			return EntityControl::execute(session, entityType, entityName, operation);
		}
		case GitSchema::Status::Unknown:
		{
			// Ignore unregistered schema deletion
			return deleting;
		}
		default:
			return false;
		}
	}

	// Handles querying schema status, setting up if required
	GitSchema::Status SchemaControl::status(const string& name, bool setup, bool adding)
	{
		// Query current status
		auto status = GitSchema::status(name);

		// Setup if required
		if (setup && status == GitSchema::Status::Unknown)
		{
			// If not adding explicitly, prompt user to confirm whether schema should be added to source control
			MsgBox::Result prompt = MsgBox::Result::Cancel;
			if (!adding)
			{
				prompt = MsgBox::display(name + " has not been associated with a repository.\n\n" +
					"Would you like to add this schema to source control?", MsgBox::Style::Warning, MsgBox::Options::Yes_No_Cancel, MsgBox::DefaultOption::Third);

				if (prompt == MsgBox::Result::No)		// Schema needs to be excluded
				{
					GitSchema::exclude(name);
					return GitSchema::Status::Excluded;
				}
				else if (prompt != MsgBox::Result::Yes)	// User cancelled
					return GitSchema::Status::Blocked;
			}

			// Setup schema
			SetupSchemaCommand command(name);
			if (!command.execute())
				return GitSchema::Status::Blocked;

			// Repeat status query to verify setup
			status = GitSchema::status(name);

			// Add schema implicitly (ensuring we're in transaction state), if not adding explicitly
			if (!adding && status == GitSchema::Status::Included)
			{
				Transaction transaction;
				if (!PatchControl::execute("Schema", name, "A"))
					return GitSchema::Status::Blocked;
				transaction.commit();
			}
		}

		return status;
	}
}
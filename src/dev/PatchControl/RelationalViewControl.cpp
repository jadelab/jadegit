#include "RelationalViewControl.h"
#include "EntityRenameStrategy.h"

using namespace std;

namespace JadeGit::Development
{
	RelationalViewControl::RelationalViewControl() : EntityControl(make_unique<EntityRenameBackgroundStrategy>()) {}

	bool RelationalViewControl::execute(Session& session, const string& entityType, const QualifiedName& entityName, const string& operation)
	{
		// Treat as RelationalView update when operation invoked for ExternalRPSClassMap
		if (entityType == "ExternalRPSClassMap" && entityName.parts() == 4)
			return execute(session, "RelationalView", *(entityName.parent->parent), "U");

		// Pass onto next in chain when entity isn't a relational view
		if (entityType != "RelationalView")
			return PatchControl::execute(session, entityType, entityName, operation);

		// Pass onto base implementation
		return EntityControl::execute(session, entityType, entityName, operation);
	}
}
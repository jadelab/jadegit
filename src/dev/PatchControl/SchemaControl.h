#pragma once
#include "EntityControl.h"
#include <schema/data/Schema.h>

namespace JadeGit::Development
{
	// Schema control handles checking status of the schema being updated, setting up if required when new one is being added
	class SchemaControl : public EntityControl
	{
	protected:
		bool prelude(const ClassNumber& entityType, const QualifiedName& entityName, bool removal) final;
		bool execute(Session& session, const std::string& entityType, const QualifiedName& entityName, const std::string& operation) final;

	private:
		Schema::GitSchema::Status status(const std::string& name, bool setup = true, bool adding = false);
	};
}
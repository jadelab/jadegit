#pragma once
#include <jadegit/arch.h>
#include <jomarch.h>

namespace JadeGit::Development
{
	/*
		Define constants for development hook results

		While these aren't currently supported by JADE, these are defined to show the intended behavior.

		JADE changes have been requested to suppress IDE messages in scenarios where we'd have already
		shown our error message or when the user has cancelled a source control operation, etc.
	*/
	#define JADEGIT_ALLOW 0
	#define JADEGIT_DENY 1
	#define JADEGIT_CANCEL -1
	#define JADEGIT_ERROR -2

	int JADEGIT_EXPORT jadeUserInfo(const Character* pUserName, const Character* pPassword);
	int JADEGIT_EXPORT jadeFunctionSelected(const Character* pUserName, const Character* pTaskName, const Character* pEntityName);
	int JADEGIT_EXPORT jadePatchControl(const Character* pUserName, const Character* pPatchDetails, const Character* pEntityName, const Character* pEntityType, const Character* pOperation);
}
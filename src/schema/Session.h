#pragma once

namespace JadeGit::Schema
{
	class RepositoryData;
	class User;
	class WorktreeData;

	class Session
	{
	public:
		static void set(const Session* session);
		static const Session& get();

		virtual const User& user() const = 0;

		WorktreeData getActiveWorktree(const RepositoryData& repo) const;
	};
}
#include "ConfigEntry.h"
#include "Config.h"
#include <schema/ObjectRegistration.h>

namespace JadeGit::Schema
{
	static GitObjectRegistration<ConfigEntry> registration(TEXT("ConfigEntry"));

	ConfigEntry::ConfigEntry(const Object& parent, git_config_entry* ptr, bool owner) : GitObject(registration, parent, ptr, owner)
	{
	}

	int JOMAPI jadegit_config_entry_name(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return jadegit_proxy_mapping_string<ConfigEntry>(pBuffer, pParams, &git_config_entry::name);
	}

	int JOMAPI jadegit_config_entry_value(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return jadegit_proxy_mapping_string<ConfigEntry>(pBuffer, pParams, &git_config_entry::value);
	}
}

#include "Progress.h"

using namespace Jade;

namespace JadeGit::Schema
{
	void Progress::message(const std::string& message)
	{
		DskParamString pMessage(message);
		invokeInterfaceMethod(TEXT("IProgress"), TEXT("message"), &pMessage, nullptr);
	}

	void Progress::update(double progress, const char* caption)
	{
		DskParam pCompleted;
		jade_throw(paramSetInteger(pCompleted, static_cast<int>(progress * 1000)));

		DskParam pTotal;
		jade_throw(paramSetInteger(pTotal, 1000));

		DskParam pParams;
		jade_throw(paramSetParamList(pParams, &pCompleted, &pTotal));

		invokeInterfaceMethod(TEXT("IProgress"), TEXT("update"), &pParams, nullptr);

		if (caption)
		{
			DskParamCString pCaption(caption);
			invokeInterfaceMethod(TEXT("IProgress"), TEXT("caption"), (DskParam*)&pCaption, nullptr);
		}
	}

	bool Progress::wasCancelled()
	{
		DskParam pResult;
		jade_throw(paramSetBoolean(pResult, false));

		invokeInterfaceMethod(TEXT("IProgress"), TEXT("wasCancelled"), nullptr, &pResult);

		bool result = false;
		jade_throw(paramGetBoolean(pResult, result));
		return result;
	}
}
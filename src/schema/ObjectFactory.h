#pragma once
#include "Object.h"
#include <map>
#include <memory>

namespace JadeGit::Schema
{
	class GitObjectFactory
	{
	public:
		static GitObjectFactory& get();

		class Registration
		{
		public:
			virtual DskObject* create(const DskObjectId& oid) const = 0;
			virtual void dispose(const DskObjectId& oid) const = 0;
			virtual void initialize(ClassNumber number) const = 0;
		};

		void registration(const Character* key, const Registration* registrar);
		void initialize() const;

		template<class TDerived = DskObject>
		std::unique_ptr<TDerived> create(const DskObjectId& oid) const
		{
			DskObject* result = lookup(oid.classNo).create(oid);
			assert(!result || dynamic_cast<TDerived*>(result));
			return std::unique_ptr<TDerived>(static_cast<TDerived*>(result));
		}

		void dispose(const DskObjectId& oid) const;

	protected:
		GitObjectFactory() {}

		std::map<Jade::String, const Registration*> registryByName;
		mutable std::map<ClassNumber, const Registration*> registryByNumber;

		const Registration& lookup(const ClassNumber& key) const;
	};
}
#pragma once
#include "DeploymentBuilder.h"
#include "WorktreeDeployment.h"

namespace JadeGit
{
	class GitFileSystem;
}

namespace JadeGit::Schema
{
	class WorktreeData;

	class WorktreeDeploymentBuilder : public DeploymentBuilder
	{
	public:
		WorktreeDeploymentBuilder(const WorktreeData& worktree, git_repository* repo);

		const std::shared_ptr<WorktreeDeployment> deployment;

		bool load(IProgress* progress);
		bool merge(IProgress* progress, std::unique_ptr<git_commit> commit);
		bool reset(IProgress* progress, const git_commit* commit);
		bool switch_(IProgress* progress, const git_reference* branch);
		bool switch_(IProgress* progress, const git_commit* commit);
		bool unload(IProgress* progress);

	protected:
		WorktreeDeploymentBuilder(std::shared_ptr<WorktreeDeployment> deployment, const WorktreeData& worktree, git_repository* repo);

		const WorktreeData& worktree;
		git_repository* repo = nullptr;

		bool build(IProgress* progress, GitFileSystem& target, bool loading = true);
	};
}
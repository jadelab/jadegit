#include "DeploymentFile.h"
#include <jade/JadeBytes.h>
#include <sstream>

using namespace Jade;

namespace JadeGit::Schema
{
	std::unique_ptr<std::ostream> GitDeploymentFile::CreateOutputStream()
	{
		JadeBytes contents;
		getProperty(TEXT("contents"), contents);
		return contents.createOutputStream();
	}

	std::unique_ptr<std::istream> GitDeploymentFile::CreateInputStream() const
	{
		JadeBytes contents;
		getProperty(TEXT("contents"), contents);
		return contents.createInputStream();
	}

	void GitDeploymentFile::ExtractToFile(const std::string& path) const
	{
		JadeBytes contents;
		getProperty(TEXT("contents"), contents);
		contents.extractToFile(path, true);
	}
}
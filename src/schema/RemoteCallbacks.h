#pragma once
#include <jadegit/Progress.h>
#include <jadegit/schema/IRemoteCallbacks.h>

namespace JadeGit::Schema
{
	class RemoteCallbacks : public IRemoteCallbacks
	{
	public:
		RemoteCallbacks(IProgress* progress) : progress(progress) {}

	private:
		IProgress* progress = nullptr;
		bool transferring = false;
		bool authenticating = false;

		int credentials(git_credential** cred, const char* url, const char* username_from_url, unsigned int allowed_types) override;
		int push_transfer_progress(unsigned int current, unsigned int total, size_t bytes) override;
		int sideband_progress(const char* str, int len) override;
		int transfer_progress(const git_indexer_progress* stats) override;
	};
}
#include "EntitySet.h"
#include <schema/ObjectRegistration.h>

namespace JadeGit::Schema
{
	static GitObjectRegistration<GitEntitySet> registration(TEXT("GitEntitySet"));

	GitEntitySet::GitEntitySet() : ObjectSet(registration)
	{
	}

	GitEntitySet::GitEntitySet(const GitEntity& owner) : ObjectSet(owner, TEXT("associates"))
	{
	}
}
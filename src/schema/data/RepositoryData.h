#pragma once

namespace JadeGit::Schema
{
	class RepositoryData;

	class IGitRepositoryData
	{
	public:
		virtual void GetRepository(RepositoryData& repo) const = 0;
	};
}
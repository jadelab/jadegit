#include "Root.h"
#include "Repository.h"
#include "Schema.h"
#include <jade/AppContext.h>
#include <jade/Lock.h>
#include <jade/Transaction.h>
#include <registry/Root.h>
#include <registry/storage/Database.h>
#include <schema/Exception.h>
#include <schema/ObjectRegistration.h>
#include <schema/Workers.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{
	static GitObjectRegistration<Root> registration(TEXT("Root"));

	Root::Root() : Object(registration)
	{
		jade_throw(classFirstOid(&oid));
	}

	Root::Root(const DskBuffer* pBuffer) : Object(pBuffer)
	{
	}

	const Root& Root::get()
	{
		static Root root;

		if (root.isNull())
		{
			// Using system as a semaphore to single thread root creation
			Lock lock(SystemOid, LockType::RESERVED_LOCK);

			// Create root if it hasn't just been created by another process
			root = Root();
			if (root.isNull())
			{
				root.setCls(registration);

				Transaction transaction;
				root.createObject();
				transaction.commit();
			}
		}

		return root;
	}

	bool Root::deploying() const
	{
		DskObjectId deployment;
		getProperty(TEXT("deployment"), deployment);
		return !deployment.isNull();
	}

	unique_ptr<GitSchema> Root::getSchema(const string& name) const
	{
		// Redirect to backend thread if required
		if (AppContext::IsSystem())
			return Backend::workers.push(bind(&Root::getSchema, this, ref(name))).get();

		DskMemberKeyDictionary schemas;
		getProperty(TEXT("schemas"), schemas);

		auto schema = make_unique<GitSchema>();
		jade_throw(schemas.getAtKey(widen(name).c_str(), *schema));

		if (schema->isNull())
			return nullptr;

		return schema;
	}

	int JOMAPI jadegit_root_create(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				Root root(pBuffer);

				// Load registry from database
				Registry::DatabaseStorage storage;
				Registry::Root registry(storage);

				// Restore repositories with a known origin / commit
				for (auto& repo : registry.repos)
					RepositoryData::restore(root, repo);

				// Save any registry changes
				registry.save(storage);

				return J_OK;
			});
	}

	int JOMAPI jadegit_root_get(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				return paramSetOid(*pReturn, Root::get().oid);
			});
	}
}
target_sources(jadegit PRIVATE
	${CMAKE_CURRENT_LIST_DIR}/Backend.cpp
	${CMAKE_CURRENT_LIST_DIR}/Data.cpp
	${CMAKE_CURRENT_LIST_DIR}/Entry.cpp
	${CMAKE_CURRENT_LIST_DIR}/Iterator.cpp
)
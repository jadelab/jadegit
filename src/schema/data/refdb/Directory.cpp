#include "Data.h"
#include "Directory.h"
#include "NodeDict.h"
#include <schema/ObjectRegistration.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{
	static GitObjectRegistration<ReferenceDirectory> registration(TEXT("ReferenceDirectory"));

	ReferenceDirectory::ReferenceDirectory() : ReferenceNode(registration) {}

	ReferenceNodeDict ReferenceDirectory::children() const
	{
		return ReferenceNodeDict(*this, TEXT("children"));
	}

	bool ReferenceNode::isDirectory() const
	{
		return getProperty<bool>(TEXT("directory"));
	}

	string ReferenceNode::path() const
	{
		string path;
		auto parent = getProperty<Object>(TEXT("parent"));
		if (!parent.isNull() && parent.isA(registration))
			path = ReferenceNode(parent.oid).path();

		path += getProperty<string>(TEXT("name"));

		if (isDirectory())
			path += "/";

		return path;
	}
}
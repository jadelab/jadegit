#pragma once
#include "NodeDict.h"
#include <git2/sys/refdb_backend.h>
#include <schema/data/Repository.h>
#include <schema/data/Worktree.h>

namespace JadeGit::Schema::Backend
{
	class jadegit_refdb_backend : public git_refdb_backend
	{
	public:
		jadegit_refdb_backend(const RepositoryData& repo, const WorktreeData& worktree);

		const RepositoryData repo;

		ReferenceData lookup(std::string name) const;
		void remove(std::string name) const;
		bool write(const git_reference* ref, bool force) const;

	private:
		const WorktreeData worktree;

		ReferenceNodeDict refs(std::string& name) const;
	};
}
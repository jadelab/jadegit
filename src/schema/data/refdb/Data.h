#pragma once
#include "Node.h"
#include <jadegit/git2.h>

namespace JadeGit::Schema
{
	class ReferenceDirectory;

	class ReferenceData : public ReferenceNode
	{
	public:
		using ReferenceNode::ReferenceNode;
		ReferenceData();

		void read(git_reference** out) const;
		void swap(const ReferenceData& other) const;
		git_reference_t type() const;
		void update(const git_oid& commit_id) const;
		void update(const std::unique_ptr<git_commit>& commit) const;
		void update(const std::unique_ptr<git_reference>& ref) const;
		bool valid() const;
		void write(const git_reference* reference) const;

		template <class T>
		std::unique_ptr<T> target(git_repository* repo) const = delete;

		template <>
		std::unique_ptr<git_commit> target<git_commit>(git_repository* repo) const;

		template <>
		std::unique_ptr<git_reference> target<git_reference>(git_repository* repo) const;
	};
}
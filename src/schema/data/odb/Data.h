#pragma once
#include <schema/Object.h>
#include <git2/errors.h>
#include <git2/oid.h>

namespace JadeGit::Schema
{
	class RepositoryData;

	class ObjectData : public Object
	{
	public:
		static ObjectData lookup(const RepositoryData& repo, const git_oid* oid);
		static void write(const RepositoryData& repo, const git_oid* oid, const void* data, size_t len, git_object_t type);
		
		git_error_code read(void** data_p, size_t* len_p, git_object_t* type_p, git_odb_backend* backend) const;

	private:
		using Object::Object;
		ObjectData(const RepositoryData& repo, const git_oid* oid, const void* data, size_t len, git_object_t type);

		git_object_t getType() const;
		git_error_code getData(void** data_p, size_t* len_p, git_odb_backend* backend) const;
	};
}
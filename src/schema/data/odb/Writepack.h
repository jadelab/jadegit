#pragma once
#include <jadegit/git2.h>
#include <git2/sys/odb_backend.h>
#include <jomtypes.h>

namespace JadeGit::Schema::Backend
{
	class jadegit_odb_writepack : public git_odb_writepack
	{
	public:
		static int make(git_odb_writepack** pack, git_odb_backend* backend, git_odb* odb, git_indexer_progress_cb progress_cb, void* progress_payload);

		git_odb* const odb;
		std::unique_ptr<git_indexer> indexer;
		std::unique_ptr<git_odb> pack_odb;

	private:
		jadegit_odb_writepack(git_odb_backend* backend, git_odb* odb, std::unique_ptr<git_indexer> indexer);
	};
}
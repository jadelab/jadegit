#pragma once
#include <jade/Object.h>

struct git_oid;

namespace JadeGit::Schema
{
	class Object : public Jade::Object
	{
	public:
		using Jade::Object::Object;

		LockType getLockStatus() const;

		template <typename T>
		void getProperty(const Character* name, T& value) const
		{
			if constexpr (std::is_base_of_v<DskObject, T>)
				jade_throw(Jade::Object::getProperty(name, value));
			else
				jade_throw(Jade::Object::getProperty(name, &value));
		}

		template <typename T>
		void getProperty(const Character* name, T* value) const
		{
			jade_throw(Jade::Object::getProperty(name, value));
		}

		template <typename T>
		T getProperty(const Character* name) const
		{
			return Jade::Object::getProperty<T>(name);
		}
		
		template <>
		git_oid getProperty(const Character* name) const;

		using Jade::Object::setProperty;
		void setProperty(const Character* name, const git_oid* oid) const;

	protected:
		static Object* getThis(const DskBuffer* buffer);
		static Object* getThis(const DskObjectId& oid);
		void setThis() const;
	};
}
#include "Repository.h"
#include "Exception.h"
#include "ObjectRegistration.h"
#include <schema/index/IndexEntry.h>

using namespace std;
using namespace Jade;

namespace JadeGit::Schema
{
	class MergeFileResult : public GitObject<git_merge_file_result>
	{
	public:
		static MergeFileResult merge_file_from_index(const Repository& repo, const IndexEntry& base, const IndexEntry& ours, const IndexEntry& theirs)
		{
			unique_ptr<git_merge_file_result> result = make_unique<git_merge_file_result>();
			git_throw(git_merge_file_from_index(result.get(), repo, base, ours, theirs, nullptr));
			return MergeFileResult(repo, move(result));
		}

		using GitObject::GitObject;

	protected:
		MergeFileResult(const Repository& repo, unique_ptr<git_merge_file_result> ptr);
	};
	static GitObjectRegistration<MergeFileResult> registration(TEXT("MergeFileResult"));

	MergeFileResult::MergeFileResult(const Repository& repo, unique_ptr<git_merge_file_result> ptr) : GitObject(registration, move(ptr))
	{
		jade_throw(setProperty(TEXT("parent"), repo));
		jade_throw(setProperty(TEXT("automergeable"), static_cast<bool>(static_cast<git_merge_file_result*>(*this)->automergeable)));
	}

	int JOMAPI jadegit_merge_file_from_index(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return GitException::wrapper([&]()
			{
				DskParam* pRepo;
				DskParam* pBase;
				DskParam* pOurs;
				DskParam* pTheirs;

				JADE_RETURN(paramGetParameter(*pParams, 1, pRepo));
				JADE_RETURN(paramGetParameter(*pParams, 2, pBase));
				JADE_RETURN(paramGetParameter(*pParams, 3, pOurs));
				JADE_RETURN(paramGetParameter(*pParams, 4, pTheirs));

				Repository repo;
				JADE_RETURN(paramGetOid(*pRepo, repo.oid));

				IndexEntry base;
				JADE_RETURN(paramGetOid(*pBase, base.oid));

				IndexEntry ours;
				JADE_RETURN(paramGetOid(*pOurs, ours.oid));

				IndexEntry theirs;
				JADE_RETURN(paramGetOid(*pTheirs, theirs.oid));

				auto result = MergeFileResult::merge_file_from_index(repo, base, ours, theirs);
				return paramSetOid(*pReturn, result.oid);
			});
	}

	int JOMAPI jadegit_merge_file_result_content(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return jadegit_proxy_mapping_method<MergeFileResult>(pBuffer, pParams, [&](git_merge_file_result* result, DskParam& value)
			{
			//	TODO: Does content need to be filtered??
			// 	unique_ptr<git_buf> buf = make_unique<git_buf>();
			//	git_throw(git_blob_filter(buf.get(), blob, "", nullptr));
				return paramSetString(value, widen(string(result->ptr, result->len)));
			});
	}

	int JOMAPI jadegit_merge_file_result_path(DskBuffer* pBuffer, DskParam* pParams, DskParam* pReturn)
	{
		return jadegit_proxy_mapping_string<MergeFileResult>(pBuffer, pParams, &git_merge_file_result::path);
	}
}
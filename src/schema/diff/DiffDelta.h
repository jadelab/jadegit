#pragma once
#include <schema/Proxy.h>

namespace JadeGit::Schema
{
	class Diff;

	class DiffDelta : public GitObject<const git_diff_delta*>
	{
	public:
		using GitObject::GitObject;
		DiffDelta(const git_diff_delta* ptr);
		DiffDelta(const Diff& diff, const git_diff_delta* ptr);
	};
}
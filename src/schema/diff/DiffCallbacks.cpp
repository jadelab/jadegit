#include "DiffCallbacks.h"
#include "DiffBinary.h"
#include "DiffDelta.h"
#include "DiffHunk.h"
#include "DiffLine.h"
#include <jomparam.hpp>

namespace JadeGit::Schema
{
	const Character* InterfaceName = TEXT("IDiffCallbacks");

	int diff_binary_cb(const git_diff_delta* delta, const git_diff_binary* binary, void* payload)
	{
		return static_cast<DiffCallbacks*>(payload)->binary_cb(delta, binary);
	}

	int diff_file_cb(const git_diff_delta* delta, float progress, void* payload)
	{
		return static_cast<DiffCallbacks*>(payload)->file_cb(delta, progress);
	}

	int diff_hunk_cb(const git_diff_delta* delta, const git_diff_hunk* hunk, void* payload)
	{
		return static_cast<DiffCallbacks*>(payload)->hunk_cb(delta, hunk);
	}

	int diff_line_cb(const git_diff_delta* delta, const git_diff_hunk* hunk, const git_diff_line* line, void* payload)
	{
		return static_cast<DiffCallbacks*>(payload)->line_cb(delta, hunk, line);
	}

	DiffCallbacks::operator git_diff_binary_cb() const
	{
		return isInterfaceMethodImplemented(InterfaceName, TEXT("binary_cb")) ? &diff_binary_cb : nullptr;
	}

	DiffCallbacks::operator git_diff_file_cb() const
	{
		return isInterfaceMethodImplemented(InterfaceName, TEXT("file_cb")) ? &diff_file_cb : nullptr;
	}

	DiffCallbacks::operator git_diff_hunk_cb() const
	{
		return isInterfaceMethodImplemented(InterfaceName, TEXT("hunk_cb")) ? &diff_hunk_cb : nullptr;
	}

	DiffCallbacks::operator git_diff_line_cb() const
	{
		return isInterfaceMethodImplemented(InterfaceName, TEXT("line_cb")) ? &diff_line_cb : nullptr;
	}

	int DiffCallbacks::binary_cb(const git_diff_delta* delta_ptr, const git_diff_binary* binary_ptr) const
	{
		DiffDelta delta(delta_ptr);
		DiffBinary binary(binary_ptr);

		DskParam pDelta;
		paramSetOid(pDelta, delta.oid);

		DskParam pBinary;
		paramSetOid(pBinary, binary.oid);

		DskParam params;
		paramSetParamList(params, &pDelta, &pBinary);

		invokeInterfaceMethod(InterfaceName, TEXT("binary_cb"), &params);
		return GIT_OK;
	}

	int DiffCallbacks::file_cb(const git_diff_delta* delta_ptr, float progress) const
	{
		DiffDelta delta(delta_ptr);

		DskParam pDelta;
		paramSetOid(pDelta, delta.oid);

		DskParam pProgress;
		paramSetFloat(pProgress, progress);

		DskParam params;
		paramSetParamList(params, &pDelta, &pProgress);

		invokeInterfaceMethod(InterfaceName, TEXT("file_cb"), &params);
		return GIT_OK;
	}

	int DiffCallbacks::hunk_cb(const git_diff_delta* delta_ptr, const git_diff_hunk* hunk_ptr) const
	{
		DiffDelta delta(delta_ptr);
		DiffHunk hunk(hunk_ptr);

		DskParam pDelta;
		paramSetOid(pDelta, delta.oid);

		DskParam pHunk;
		paramSetOid(pHunk, hunk.oid);

		DskParam params;
		paramSetParamList(params, &pDelta, &pHunk);

		invokeInterfaceMethod(InterfaceName, TEXT("hunk_cb"), &params);
		return GIT_OK;
	}

	int DiffCallbacks::line_cb(const git_diff_delta* delta_ptr, const git_diff_hunk* hunk_ptr, const git_diff_line* line_ptr) const
	{
		DiffDelta delta(delta_ptr);
		DiffHunk hunk(hunk_ptr);
		DiffLine line(line_ptr);

		DskParam pDelta;
		paramSetOid(pDelta, delta.oid);

		DskParam pHunk;
		paramSetOid(pHunk, hunk.oid);

		DskParam pLine;
		paramSetOid(pLine, line.oid);

		DskParam params;
		paramSetParamList(params, &pDelta, &pHunk, &pLine);

		invokeInterfaceMethod(InterfaceName, TEXT("line_cb"), &params);
		return GIT_OK;
	}
}
#pragma once
#include "DiffDelta.h"

namespace JadeGit::Schema
{
	class DiffFile : public GitObject<const git_diff_file*>
	{
	public:
		using GitObject::GitObject;
		DiffFile(const DiffDelta& parent, const Character* prop, const git_diff_file* file);

	};
}
#include "Install.h"
#include "Deployment.h"
#include "Repository.h"
#include <jadegit/build.h>
#include <deploy/build/Director.h>
#include <registry/Commit.h>
#include <registry/Manager.h>
#include <registry/Schema.h>
#include <registry/storage/Database.h>
#include <mutex>

using namespace std;
using namespace JadeGit::Deploy;
using namespace JadeGit::Registry;
using namespace JadeGit::Schema::Install;

namespace JadeGit::Schema
{
	bool check_registry(Root& registry)
	{
		// Find jadegit registry
		auto repo = registry.find_repo_by_name("jadegit");
		if (!repo)
			repo = registry.find_repo_by_origin(build_repo);

		// No need to continue if repo already up-to-date
		if (repo)
			return repo->latest.size() == 1 && repo->latest.front() == build_commit;

		// Install required
		return false;
	}

	void install()
	{
		// Quick install check
		static bool installed = false;
		if (installed)
			return;

		// Use mutex to single-thread install
		static mutex mtx;
		unique_lock<mutex> lock(mtx);

		// Double check another thread hasn't just performed install
		if (installed)
			return;

		// Extract current registry data
		Root registry = Root::extract();

		// Check registry to determine if jadegit already up-to-date
		if (check_registry(registry))
		{
			installed = true;
			return;
		}

		// Fetch repository & latest commit
		unique_ptr<git_repository> repo;
		unique_ptr<git_commit> commit;
		fetch(repo, commit);

		// Build deployment
		Deployment deployment;
		Deploy::Director director(*repo, deployment, nullptr);
		director.build(Manager(registry), move(commit));

		// Install
		deployment.execute();

		// Set installed flag
		installed = true;
	}
}
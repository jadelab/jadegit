#pragma once
#include "Repository.h"

namespace JadeGit::Schema
{
	class Blob : public GitObject<git_blob>
	{
	public:
		static Blob lookup(const Repository& repo, const git_oid& oid);

		using GitObject::GitObject;

	protected:
		Blob(const Repository& repo, std::unique_ptr<git_blob> ptr);
	};
}
#pragma once
#include "Object.h"
#include <git2/oid.h>

namespace JadeGit::Schema
{
	class ICommittish
	{
	public:
		virtual git_oid id() const = 0;
	};

	class Committish : public Object, public ICommittish
	{
	public:
		using Object::Object;

		git_oid id() const final;
	};
}
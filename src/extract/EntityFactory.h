#pragma once
#include "Entity.h"
#include "NamedObjectFactory.h"

namespace JadeGit::Extract
{
	class EntityFactory : public NamedObjectFactory
	{
	public:
		static EntityFactory& Get();

		class Registration : public NamedObjectFactory::Registration
		{
		public:
			virtual Entity* Create(const DskObjectId& oid) const override = 0;
			virtual Entity* Create() const = 0;
		};

		void Register(const ClassNumber& key, const Registration* registrar);

		std::unique_ptr<Entity> Create(const DskObjectId& oid) const;
		std::unique_ptr<Entity> Create(const ClassNumber& key) const;
		std::unique_ptr<Entity> Create(const std::string& key) const;
		std::unique_ptr<Entity> resolve(const ClassNumber& klass, const QualifiedName& path, bool required = true) const;
		std::unique_ptr<Entity> resolve(const ClassNumber& klass, const Entity* ancestor, const QualifiedName& path, bool required = true) const;
		std::unique_ptr<Entity> resolve(const std::string& klass, const QualifiedName& path, bool required = true) const;
		std::unique_ptr<Entity> resolve(const std::string& klass, const Entity* ancestor, const QualifiedName& path, bool required = true) const;

	protected:
		EntityFactory() {}
	};
}
#pragma once
#include "NamedObjectFactory.h"
#include "ObjectRegistration.h"

namespace JadeGit::Extract
{
	template <class TDerived, class TInterface = NamedObjectFactory::Registration> requires std::is_base_of_v<NamedObject, TDerived>
	class NamedObjectRegistration : protected ObjectRegistration<TDerived, TInterface>
	{
	public:
		NamedObjectRegistration(const ClassNumber& key) : ObjectRegistration<TDerived, TInterface>(key)
		{
			NamedObjectFactory::Get().Register(key, this);
		}
	};
}
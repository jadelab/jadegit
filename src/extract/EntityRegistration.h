#pragma once
#include "EntityFactory.h"
#include "NamedObjectRegistration.h"

namespace JadeGit::Extract
{
	template <class TDerived> requires std::is_base_of_v<Entity, TDerived>
	class EntityRegistration : protected NamedObjectRegistration<TDerived, EntityFactory::Registration>
	{
	public:
		EntityRegistration(const ClassNumber& key) : NamedObjectRegistration<TDerived, EntityFactory::Registration>(key)
		{
			EntityFactory::Get().Register(key, this);
		}

		TDerived* Create() const final
		{
			return new TDerived();
		}
	};
}
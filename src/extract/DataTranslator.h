#pragma once
#include <Singleton.h>
#include <jomobj.hpp>
#include <string>

namespace JadeGit::Data
{
	class Any;
	class Property;
	class Object;
}

namespace JadeGit::Extract
{
	class Assembly;
	class Object;

	class DataTranslator
	{
	public:
		const bool basic;

		virtual void Copy(Assembly& assembly, const Object& source, const DskParam& value, Data::Object* target, const Data::Property* property, const std::string& name, const std::string& trail, bool deep) const = 0;

	protected:
		DataTranslator(bool basic) : basic(basic) {}
	};

	class AttributeTranslator : public DataTranslator
	{
	public:
		AttributeTranslator() : DataTranslator(true) {}

		void Copy(Assembly& assembly, const Object& source, const DskParam& value, Data::Object* target, const Data::Property* property, const std::string& name, const std::string& trail, bool deep) const final;

	protected:
		virtual Data::Any translate(const DskParam& value) const;
	};

	class ChildTranslator : public DataTranslator
	{
	protected:
		ChildTranslator() : DataTranslator(false) {}

		bool Copy(Assembly& assembly, Data::Object* parent, const DskObjectId& oid, const std::string& name, int index, const std::string& trail, bool deep) const;
	};

	class ChildReferenceTranslator : public ChildTranslator, public Singleton<ChildReferenceTranslator>
	{
	public:
		void Copy(Assembly& assembly, const Object& source, const DskParam& value, Data::Object* target, const Data::Property* property, const std::string& name, const std::string& trail, bool deep) const final;
	};

	class ChildCollectionTranslator : public ChildTranslator, public Singleton<ChildCollectionTranslator>
	{
	public:
		void Copy(Assembly& assembly, const Object& source, const DskParam& value, Data::Object* target, const Data::Property* property, const std::string& name, const std::string& trail, bool deep) const final;
	};

	class PeerTranslator : public DataTranslator
	{
	protected:
		PeerTranslator(bool basic) : DataTranslator(basic) {}

		void Copy(Assembly& assembly, const DskObjectId& peer, Data::Object* target, const Data::Property* property, const std::string& name, const std::string& trail) const;
		void Copy(Assembly& assembly, const DskCollection& peers, Data::Object* target, const Data::Property* property, const std::string& name, const std::string& trail) const;
	};

	class PeerReferenceTranslator : public PeerTranslator, public Singleton<PeerReferenceTranslator>
	{
	public:
		PeerReferenceTranslator() : PeerTranslator(true) {}

		void Copy(Assembly& assembly, const Object& source, const DskParam& value, Data::Object* target, const Data::Property* property, const std::string& name, const std::string& trail, bool deep) const final;
	};

	class PeerCollectionTranslator : public PeerTranslator, public Singleton<PeerCollectionTranslator>
	{
	public:
		PeerCollectionTranslator() : PeerTranslator(false) {}

		void Copy(Assembly& assembly, const Object& source, const DskParam& value, Data::Object* target, const Data::Property* property, const std::string& name, const std::string& trail, bool deep) const final;
	};
}
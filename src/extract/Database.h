#pragma once
#include "SchemaEntity.h"

namespace JadeGit::Extract
{
	class Database : public SchemaEntity
	{
	public:
		using SchemaEntity::SchemaEntity;
	
	protected:
		bool lookup(const Object* ancestor, const QualifiedName& path) final;
	};

	class DbFile;

	class DbClassMap : public Object
	{
	public:
		using Object::Object;
	};

	class DbFile : public SchemaEntity
	{
	public:
		using SchemaEntity::SchemaEntity;

		bool isShallow() const final;

	protected:
		void dependents(std::set<DskObjectId>& dependents) const override;
		bool lookup(const Object* ancestor, const QualifiedName& path) final;
	};
}
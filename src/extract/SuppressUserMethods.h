#pragma once

namespace JadeGit::Extract
{
	class SuppressUserMethods
	{
	public:
		SuppressUserMethods();
		~SuppressUserMethods();

	private:
		bool restore = false;

		bool invokeUserMethods(bool invoke) const;
	};
}
#pragma once
#include <jomtypes.h>
#include <jomobj.hpp>
#include <map>
#include <memory>
#include <functional>
#include <string>

namespace std
{
	template<>
	struct less<JomClassFeatureLevel>
	{
		bool operator()(const JomClassFeatureLevel& lhs, const JomClassFeatureLevel& rhs) const
		{
			return (lhs.classNo < rhs.classNo) || (lhs.classNo == rhs.classNo && lhs.number < rhs.number);
		}
	};
}

namespace JadeGit::Data
{
	class Class;
	class Property;
	class Object;
}

namespace JadeGit::Extract
{
	class Assembly;
	class DataTranslator;
	class Object;

	class DataMapping
	{
	public:
		typedef std::function<bool(const Object&)> Predicate;

		DataMapping(const Data::Property* property, Predicate predicate, const DataTranslator* translator) : property(property), predicate(predicate), translator(translator) {}

		std::string name;
		const Data::Property* const property;
		Predicate const predicate;
		const DataTranslator* translator = nullptr;

		void Copy(Assembly& assembly, const Object& source, const JomClassFeatureLevel& feature, Data::Object* target, const std::string& trail, bool deep, bool original) const;
	};

	class DataMap
	{
	public:
		DataMap(DataMap* base, Data::Class& cls) : base(base), cls(cls), level(base ? base->level + 1 : 1) {}

		const Data::Class& cls;
		const Level level;

		void ExcludeProperty(const JomClassFeatureLevel& feature);
		DataMapping* IncludeProperty(const JomClassFeatureLevel& feature, const Data::Property* property, DataMapping::Predicate predicate, const DataTranslator* translator);
		bool GetMapping(const JomClassFeatureLevel& feature, DataMapping*& mapping);

		void Copy(Assembly& assembly, const Object& source, Data::Object* target, const std::string& trail, bool deep, bool original) const;

	protected:
		DataMap* const base;
		std::map<JomClassFeatureLevel, std::unique_ptr<DataMapping>> mappings;
	};
}
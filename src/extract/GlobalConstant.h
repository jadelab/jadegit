#pragma once
#include "Constant.h"

namespace JadeGit::Extract
{
	class GlobalConstant : public Constant
	{
	public:
		using Constant::Constant;

	protected:
		bool lookup(const Object* ancestor, const QualifiedName& path) final;
	};
}
#pragma once
#include "Entity.h"

namespace JadeGit::Extract
{
	class ExternalDatabase : public Entity
	{
	public:
		using Entity::Entity;

		std::string getName() const final;

	protected:
		bool lookup(const Object* ancestor, const QualifiedName& path) final;

		DskObjectId getParentId() const final;
	};
}
#pragma once
#include "Object.h"

namespace JadeGit::Extract
{
	// Objects which can be resolved by name
	class NamedObject : public Object
	{
	public:
		using Object::Object;

		virtual std::string getName() const = 0;

		// Find the source object
		bool find(const Object* ancestor, const QualifiedName* path, bool required);
		bool find(const Object* ancestor, const QualifiedName& path, bool required);

		// Retrieve parent object
		using Object::getParentId;
		DskObjectId getParentId() const override = 0;

	protected:
		// Lookup object for extract
		virtual bool lookup(const Object* ancestor, const QualifiedName& path) = 0;

		template<class TParent, typename... Rest>
		bool lookup(const Object* ancestor, const QualifiedName& path, const JomClassFeatureLevel& first, Rest... rest)
		{
			if (const TParent* parent = dynamic_cast<const TParent*>(ancestor))
			{
				assert(!path.parent);
				return lookup(*parent, path, first, rest...);
			}

			// Resolve parent
			TParent parent;
			if (!parent.find(ancestor, path.parent.get(), false))
				return false;

			// Resolve child
			return lookup(parent, path, first, rest...);
		}

		template<class TParent, typename... Rest>
		bool lookup(const TParent& parent, const QualifiedName& path, const JomClassFeatureLevel& first, Rest... rest)
		{
			// Lookup child via current parent version
			if (lookup(parent, path.name, first, rest...))
				return true;

			// Lookup child via latest parent version
			if (parent.getVersionState() == ObjectVersionState::OBJVERSTATE_VERSIONED_CURRENT)
			{
				TParent latest;
				jade_throw(parent.getNextVersionObject(&latest));
				if (!latest.isNull() && lookup(latest, path.name, first, rest...))
					return true;
			}

			return false;
		}

		// Resolves child object in the context of parent supplied
		Data::Object* resolve(Data::Assembly& assembly, Data::Component* parent, bool shallow) const override;

	private:
		template<class TParent>
		bool lookup(const TParent& parent, const std::string& name, const JomClassFeatureLevel& first)
		{
			// Get collection
			DskMemberKeyDictionary collection;
			jade_throw(parent.getProperty(first, collection));

			// Lookup child
			jade_throw(collection.getAtKey(Jade::widen(name).c_str(), *this));
			return !isNull();
		}

		template<class TParent, typename... Rest>
		bool lookup(const TParent& parent, const std::string& name, const JomClassFeatureLevel& first, Rest... rest)
		{
			// Lookup in first or subsequent collections
			return lookup(parent, name, first) || lookup(parent, name, rest...);
		}
	};
}
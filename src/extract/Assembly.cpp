#include "Assembly.h"
#include "DataMap.h"
#include "SchemaIterator.h"
#include <data/storage/ObjectFileStorage.h>
#include <jade/ObjectSet.h>
#include <jade/Transient.h>
#include <registry/Schema.h>
#include <Platform.h>
#include <regex>

using namespace std;
using namespace Jade;

namespace JadeGit::Extract
{
	Assembly::Assembly(const FileSystem& source) : Data::Assembly(source, jadeVersion)
	{
	}

	Assembly::~Assembly()
	{
		for (auto maps : dataMaps)
			delete maps.second;
	}

	void Assembly::backfill(Registry::RepositoryT& registry, function<void(const string&)> print)
	{
		// Load registered & missing schemas
		Transient<ObjectSet<Schema>> registered;
		vector<string> missing;
		for (auto it = begin(registry.schemas); it != end(registry.schemas);)
		{
			// Lookup schema in database
			Schema schema;
			if (schema.find(nullptr, it->name, false))
			{
				// Add schema to registered set and advance
				registered.tryAdd(schema);
				it++;
			}
			else
			{
				// Add schema to missing list for removal below
				missing.push_back(it->name);

				// Erase schema entry from registry and skip
				it = registry.schemas.erase(it);
			}
		}
		
		// Define filter (returns true for schemas to be extracted)
		auto filter = [&](const Schema& schema)
			{
				// Extract schemas already registered
				if (registered.includes(schema))
				{
					return true;
				}
				// Ignore supplied schemas
				else if (Schema::isSupplied(schema.getName()))
				{
					return false;
				}
				// Assume new schema should be extracted otherwise (further work needed to distinguish when new schemas should be included)
				else
				{
					registry.schemas.push_back(Registry::make_schema(schema.getName()));
					return true;
				}
			};

		// Handle extraction
		extract(filter, print);

		// Handle schema removal (after any dependencies have been removed during extract)
		if (!missing.empty())
		{
			// Load known schemas
			getStorage().loadChildren(filesystem::path(), false);

			// Remove missing schemas
			for (auto& name : missing)
			{
				if (auto schema = this->schemas.Get(name))
				{
					if (print) print("Removing " + name);
					schema->Delete();
				}
			}
		}
	}

	void Assembly::extract(const vector<string>& schemas, function<void(const string&)> print)
	{
		// Treat schemas specified as regular expressions
		vector<regex> patterns;
		for (auto& pattern : schemas)
			patterns.push_back(regex(pattern));

		// Define filter (returns true for schemas to be extracted)
		auto filter = [&](const Schema& schema)
			{
				auto name = schema.getName();

				// Supplied schemas which are generally excluded by source control must be explicitly specified for extract
				if (Schema::isSupplied(name))
				{
					if (std::find(schemas.begin(), schemas.end(), name) != schemas.end())
						return true;
				}
				// Include all schemas otherwise if none explicitly specified
				else if (schemas.empty())
				{
					return true;
				}
				else
				{
					for (const auto& pattern : patterns)
					{
						if (regex_match(name, pattern))
							return true;
					}
				}

				return false;
			};

		// Handle extraction
		extract(filter, print);

		// Handle schema removal when all schemas are being extracted
		if (schemas.empty())
		{
			vector<Data::Schema*> missing;
			getStorage().loadChildren(filesystem::path(), false);
			for (auto& schema : this->schemas)
			{
				// Ignore static schemas
				if (schema.second->isStatic())
					continue;

				// Ignore schemas that exist (in current database anyways)
				if (Schema().find(nullptr, schema.second->name, false))
					continue;

				// Add to missing list for removal
				missing.push_back(schema.second);
			}

			// Remove missing schemas
			for (auto& schema : missing)
			{
				if (print) print("Removing " + schema->name);
				schema->Delete();
			}
		}
	}

	void Assembly::extract(function<bool(const Schema&)> filter, function<void(const string&)> print)
	{
		// Iterate schemas in dependency order
		Schema schema;
		SchemaIterator iter;
		bool acyclic = false;
		bool extracted = false;
		while (iter.next(schema, acyclic))
		{
			// Perform deep extract for schema if included
			if (filter(schema))
			{
				if (print) print("Extracting " + schema.getName());
				schema.extract(*this, true);

				extracted = true;
			}

			// Save & unload schemas to recover memory, provided schema just extracted isn't part of a cyclic dependency
			if (extracted && acyclic)
			{
				save(true);
				extracted = false;
			}
		}
		assert(!extracted);
	}

	void Assembly::unload()
	{
		// Unload data maps for non-static schemas which are about to be unloaded
		auto it = dataMaps.begin();
		while (it != dataMaps.end())
		{
			if (!it->second->cls.isStatic())
			{
				delete it->second;
				it = dataMaps.erase(it);
			}
			else
				it++;
		}

		Data::Assembly::unload();
	}
}
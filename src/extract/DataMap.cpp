#include "Assembly.h"
#include "DataMap.h"
#include "DataTranslator.h"
#include "Entity.h"
#include "ObjectFactory.h"
#include <jadegit/data/Object.h>
#include <jadegit/data/Chrono.h>
#include <jadegit/data/Property.h>
#include <jade/Any.h>
#include <jade/Chrono.h>
#include <jade/Iterator.h>
#include <jade/StringUtf8.h>
#include <joscalls.h>
#include <Log.h>

using namespace Jade;
using namespace JadeGit::Data;

namespace JadeGit::Extract
{
	void DataMap::ExcludeProperty(const JomClassFeatureLevel& feature)
	{
		mappings.try_emplace(feature, nullptr);
	}

	DataMapping* DataMap::IncludeProperty(const JomClassFeatureLevel& feature, const Data::Property* property, DataMapping::Predicate predicate, const DataTranslator* translator)
	{
		auto result = mappings.try_emplace(feature, new DataMapping(property, predicate, translator));
		return (*result.first).second.get();
	}

	bool DataMap::GetMapping(const JomClassFeatureLevel& feature, DataMapping*& mapping)
	{
		auto iter = mappings.find(feature);
		if (iter == std::end(mappings))
			return false;

		mapping = (*iter).second.get();
		return true;
	}

	void DataMap::Copy(Assembly& assembly, const Object& source, Data::Object* target, const std::string& trail, bool deep, bool original) const
	{
		if (base) base->Copy(assembly, source, target, trail, deep, original);

		for (auto iter = mappings.begin(); iter != mappings.end(); iter++)
		{
			if (DataMapping* mapping = (*iter).second.get())
				mapping->Copy(assembly, source, (*iter).first, target, trail, deep, original);
		}
	}

	void DataMapping::Copy(Assembly& assembly, const Object& source, const JomClassFeatureLevel& feature, Data::Object* target, const std::string& trail, bool deep, bool original) const
	{
		assert(translator);
		if (!translator)
			return;

		// Suppress copying basic properties to copies/proxies of the original
		if (!original && translator->basic)
			return;

		// Suppress copying based on predicate
		if (predicate && !predicate(source))
		{
			target->resetValue(property);
			return;
		}

		/* Get the property value */
		/* NOTE: This could be optimized by shifting into the translators to avoid the use of 'Any' (which doesn't seem reusable) */
		DskParamAny value;
		jade_throw(source.getProperty(feature, &value, __LINE__));

		/* Copy using translator */
		translator->Copy(assembly, source, value, target, property, name, trail, deep);
	}

	/* Handles copying simple attributes */
	void AttributeTranslator::Copy(Assembly& assembly, const Object& source, const DskParam& value, Data::Object* target, const Data::Property* property, const std::string& name, const std::string& trail, bool deep) const
	{
		assert(property);
		LOG_TRACE("Extracting attribute: " << name);

		try
		{
			switch (value.header.format)
			{
			// Handle copying primitive arrays
			case PFormat::DSKOBJECTID:
			{
				DskObjectId oid;
				jade_throw(paramGetOid(value, oid));

				DskCollection values(oid);

				bool empty = false;
				jade_throw(values.isEmpty(&empty));

				// Get collection to copy to, instantiating if source collection isn't empty
				Data::Collection* collection = target->GetValue(*property, !empty).Get<Data::Collection*>();

				// Ignore scenarios where the source collection is empty and there's no collection to update
				if (!collection)
				{
					if (!empty) throw logic_error(format("Failed to instantiate primitive collection ({})", property->getQualifiedName()));
					break;
				}

				// Copy source collection
				collection->Clear();
				DskParamAny entry;
				Jade::Iterator<DskParamAny> iter(values);
				while (iter.next(entry))
					collection->Add(translate(entry));

				break;
			}
			// Handle copying simple primitive attribute
			default:
				target->SetValue(property, translate(value));
			}
		}
		catch (...)
		{
			throw_with_nested(runtime_error(format("Failed to extract attribute ({}.{})", trail, name)));
		}
	}

	// TODO: Need to standardize line ending conversion within jade submodule
	// Handling conversion here/below is needed to support extract unit testing, but currently at the cost of filtering again when written to git filesystem
	std::string replaceCrLf(std::string&& str)
	{
		std::string::size_type pos = 0;
		while ((pos = str.find("\r\n", pos)) != std::string::npos)
			str.erase(pos, 1);

		return std::move(str);
	}

	Any AttributeTranslator::translate(const DskParam& value) const
	{
		switch (value.header.format)
		{
		case PFormat::DSKJADEBINARY:
		{
			Byte* buffer = 0;
			Size length = 0;
			paramGetBinary(value, buffer, length);
			return Value(Binary(buffer, length));
		}
		case PFormat::DSKBOOLEAN:
			return Value((bool)value.body.boolean);

		case PFormat::DSKBYTE:
			return Value(value.body.byte);

		case PFormat::DSKCHARACTER:
			return Value(static_cast<char>(value.body.character));	// TODO: What happens if unicode character?

		case PFormat::DSKDATE:
		{
			Date date;
			jade_throw(paramGetDate(value, date));
			return Value(date);
		}
		case PFormat::DSKINTEGER:
			return Value(value.body.integer);

		case PFormat::DSKREAL:
			return Value(value.body.real);

		case PFormat::DSKDECIMAL:
		{
			Real real = 0;
			if (!jomDecimalToReal(&value.body.decimal, &real))
				throw overflow_error("Failed to convert decimal to real value");
			return Value(real);
		}
		case PFormat::DSKJADESTRING:
		{
			String string;
			paramGetString(value, string);
			return Value(replaceCrLf(narrow(std::move(string))));
		}
		case PFormat::DSKJADESTRINGUTF8:
		{
			std::u8string string;
			paramGetString(value, string);
			return Value(string);
		}
		case PFormat::DSKTIME:
		{
			Time time;
			jade_throw(paramGetTime(value, time));
			return Value(time);
		}
		case PFormat::DSKTIMESTAMP:
		{
			TimeStamp ts;
			jade_throw(paramGetTimeStamp(value, ts));
			return Value(ts);
		}
		default:
			throw logic_error(format("Unhandled extract value type ({})", static_cast<underlying_type_t<decltype(value.header.format)>>(value.header.format)));
		}
	}

	bool ChildTranslator::Copy(Assembly& assembly, Data::Object* parent, const DskObjectId& oid, const std::string& name, int index, const std::string& trail, bool deep) const
	{
		if (oid.isNull())
			return true;

		std::unique_ptr<Object> child(ObjectFactory::Get().Create(oid));

		/* Suppress copy if child is an entity and we're not performing a deep copy */
		auto entity = dynamic_cast<Entity*>(child.get());
		if (entity && !deep)
			return false;

		/* Suppress copy if child is shallow (empty or proxy to another), which shouldn't be extracted */
		if (child->isShallow())
			return true;

		/* Copy object */
		child->Copy(assembly, parent, (entity ? trail + "::" + entity->getName() : trail + "." + name + (index ? "[" + std::to_string(index) + "]" : "")), deep);
		return true;
	}

	void ChildReferenceTranslator::Copy(Assembly& assembly, const Object& source, const DskParam& value, Data::Object* target, const Data::Property* property, const std::string& name, const std::string& trail, bool deep) const
	{
		DskObjectId oid;
		jade_throw(paramGetOid(value, oid));

		ChildTranslator::Copy(assembly, target, oid, name, 0, trail, deep);
	}

	void ChildCollectionTranslator::Copy(Assembly& assembly, const Object& source, const DskParam& value, Data::Object* target, const Data::Property* property, const std::string& name, const std::string& trail, bool deep) const
	{
		DskObjectId oid;
		jade_throw(paramGetOid(value, oid));

		DskCollection children(oid);
		DskObject child;
		Jade::Iterator<DskObject> iter(children);
		int index = 0;
		while (iter.next(child))
		{
			if (!ChildTranslator::Copy(assembly, target, child.oid, name, ++index, trail, deep))
				break;
		}
	}

	void PeerTranslator::Copy(Assembly& assembly, const DskObjectId& peer, Data::Object* target, const Data::Property* property, const std::string& name, const std::string& trail) const
	{
		try
		{
			target->SetValue(property, Object::resolve(assembly, peer));
		}
		catch (...)
		{
			throw_with_nested(runtime_error(format("Failed to extract peer reference ({}.{})", trail, name)));
		}
	}

	void PeerTranslator::Copy(Assembly& assembly, const DskCollection& peers, Data::Object* target, const Data::Property* property, const std::string& name, const std::string& trail) const
	{
		bool empty = false;
		jade_throw(peers.isEmpty(&empty));

		// Get collection to copy to, instantiating if source collection isn't empty
		Data::Collection* collection = target->GetValue(*property, !empty).Get<Data::Collection*>();

		// Ignore scenarios where the source collection is empty and there's no collection to update
		if (!collection)
		{
			if (!empty) throw logic_error(format("Failed to instantiate peer collection ({})", property->getQualifiedName()));
			return;
		}

		int index = 0;
		try
		{
			// Copy source collection
			collection->Clear();
			DskObject peer;
			Jade::Iterator<DskObject> iter(peers);
			while (iter.next(peer))
			{
				index++;
				collection->Add(Data::Value(Object::resolve(target->getAssembly(), peer.oid)));
			}
		}
		catch (...)
		{
			throw_with_nested(runtime_error(format("Failed to extract peer collection ({}.{}[{}])", trail, name, index)));
		}


	}

	void PeerReferenceTranslator::Copy(Assembly& assembly, const Object& source, const DskParam& value, Data::Object* target, const Data::Property* property, const std::string& name, const std::string& trail, bool deep) const
	{
		assert(property);

		LOG_TRACE("Extracting peer: " << name);

		DskObjectId oid;
		jade_throw(paramGetOid(value, oid));

		PeerTranslator::Copy(assembly, oid, target, property, name, trail);
	}

	void PeerCollectionTranslator::Copy(Assembly& assembly, const Object& source, const DskParam& value, Data::Object* target, const Data::Property* property, const std::string& name, const std::string& trail, bool deep) const
	{
		assert(property);

		LOG_TRACE("Extracting peers: " << name);

		DskObjectId oid;
		jade_throw(paramGetOid(value, oid));

		DskCollection peers(oid);

		PeerTranslator::Copy(assembly, peers, target, property, name, trail);
	}
}
#pragma once
#include "DataMap.h"

namespace JadeGit::Extract
{
	class Assembly;

	class DataMapFactory
	{
	public:
		static DataMapFactory &Get() { static DataMapFactory f; return f; }

		class Mapper
		{
		public:
			virtual DataMap* Create(Assembly& assembly, DataMap* base) const = 0;
		};

		void Register(ClassNumber key, const Mapper* registrar);

		DataMap* Create(const DskClass& klass, Assembly& assembly) const;

	protected:
		DataMapFactory() {}

	private:
		typedef std::map<ClassNumber, const Mapper*> Registry;
		Registry registry;
	};
}
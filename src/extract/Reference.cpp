#include "Reference.h"
#include "DataMapper.h"
#include "EntityRegistration.h"
#include <jadegit/data/RootSchema/ExternalReferenceMeta.h>
#include <jadegit/data/RootSchema/JadeDynamicExplicitInverseRefMeta.h>
#include <jadegit/data/RootSchema/JadeDynamicImplicitInverseRefMeta.h>
#include <jadegit/data/RootSchema/InverseMeta.h>

using namespace std;
using namespace JadeGit::Data;

namespace JadeGit::Extract
{
	static DataMapper<ReferenceMeta> referenceMapper(DSKREFERENCE, &RootSchema::reference, {
		{PRP_Reference_constraint, new DataProperty(&ReferenceMeta::constraint)},
		{PRP_Reference_constraintChanged, nullptr}
		});

	static DataMapper<ExplicitInverseRefMeta> explicitInverseRefMapper(DSKEXPLICITINVERSEREF, &RootSchema::explicitInverseRef, {
		{PRP_ExplicitInverseRef__allowTransToPersistRef, new DataProperty(&ExplicitInverseRefMeta::transientToPersistentAllowed)},
		{PRP_ExplicitInverseRef_inverseNotRequired, new DataProperty(&ExplicitInverseRefMeta::inverseNotRequired)}
		});

	static DataMapper<ExternalReferenceMeta> externalReferenceMapper(DSKEXTERNALREFERENCE, &RootSchema::externalReference, {
		{PRP_ExternalReference_externalSchemaMap, nullptr},
		{PRP_ExternalReference_joinPredicate, new DataProperty(&ExternalReferenceMeta::joinPredicate)},
		{PRP_ExternalReference_joinPredicateInfo, new DataProperty(&ExternalReferenceMeta::joinPredicateInfo)}
		});

	static DataMapper<JadeDynamicExplicitInverseRefMeta> dynamicExplicitInverseRefMapper(DSKJADEDYNAMICEXPLICITINVERSEREF, &RootSchema::jadeDynamicExplicitInverseRef, {
		{PRP_JadeDynamicExplicitInverseRef_dynamicPropertyCluster, new DataProperty(&JadeDynamicExplicitInverseRefMeta::dynamicPropertyCluster)}
		});

	static DataMapper<JadeDynamicImplicitInverseRefMeta> dynamicImplicitInverseRefMapper(DSKJADEDYNAMICIMPLICITINVERSEREF, &RootSchema::jadeDynamicImplicitInverseRef, {
		{PRP_JadeDynamicImplicitInverseRef_dynamicPropertyCluster, new DataProperty(&JadeDynamicImplicitInverseRefMeta::dynamicPropertyCluster)}
		});

	static DataMapper<InverseMeta> inverseMapper(DSKINVERSE, &RootSchema::inverse, {
		{PRP_Inverse__systemBasic, nullptr}
		});

	static EntityRegistration<ExplicitInverseRef> explicitInverseRef(DSKEXPLICITINVERSEREF);
	static EntityRegistration<ImplicitInverseRef> implicitInverseRef(DSKIMPLICITINVERSEREF);

	string Reference::GetBasicTypeName() const
	{
		return "Reference";
	}

	void ExplicitInverseRef::dependents(set<DskObjectId>& dependents) const
	{
		Reference::dependents(dependents);

		// Handle indirect dependencies via inverses
		set<DskObjectId> inverses;
		getProperty(PRP_ExplicitInverseRef_inverses, inverses);
		for (auto& i : inverses)
		{
			DskObject inverse(i);

			DskObjectId ref = NullDskObjectId;
			jade_throw(inverse.getProperty(PRP_Inverse_leftReference, &ref));

			if (ref == oid)
				jade_throw(inverse.getProperty(PRP_Inverse_rightReference, &ref));

			dependents.insert(ref);
		}
	}

	class Inverse : public Object
	{
	public:
		Inverse(DskObjectId target) : Object(target) {}

	protected:
		Data::Inverse* resolve(Data::Assembly& assembly, Data::Component* parent, bool shallow) const override
		{
			// Resolve both references
			auto left = static_cast<Data::ExplicitInverseRef*>(&getProperty<ExplicitInverseRef>(PRP_Inverse_leftReference).resolve(assembly, true));
			auto right = static_cast<Data::ExplicitInverseRef*>(&getProperty<ExplicitInverseRef>(PRP_Inverse_rightReference).resolve(assembly, true));

			// Treat left as the parent reference
			if (right == parent)
				swap(left, right);

			// Parent should be either reference
			if (left != parent)
				throw runtime_error("Failed to resolve inverse reference matching parent");

			// Return existing
			if (Data::Inverse* result = left->getInverse(right))
				return result;

			// Return new
			return new Data::Inverse(left, right);
		}
	};
	static ObjectRegistration<Inverse> inverse(DSKINVERSE);

	class JadeDynamicExplicitInverseRef : public ExplicitInverseRef
	{
	public:
		using ExplicitInverseRef::ExplicitInverseRef;

	protected:
		bool isShallow() const final
		{
			return getProperty<bool>(PRP_JadeDynamicExplicitInverseRef_dynamicDefinition);
		}
	};
	static EntityRegistration<JadeDynamicExplicitInverseRef> dynamicExplicitInverseRef(DSKJADEDYNAMICEXPLICITINVERSEREF);

	class JadeDynamicImplicitInverseRef : public ImplicitInverseRef
	{
	public:
		using ImplicitInverseRef::ImplicitInverseRef;

	protected:
		bool isShallow() const final
		{
			return getProperty<bool>(PRP_JadeDynamicImplicitInverseRef_dynamicDefinition);
		}
	};
	static EntityRegistration<JadeDynamicImplicitInverseRef> dynamicImplicitInverseRef(DSKJADEDYNAMICIMPLICITINVERSEREF);
}
#pragma once
#include "SchemaEntity.h"
#include <jadegit/data/Class.h>

namespace JadeGit::Extract
{
	bool hasPredefinedLength(const DskType& type);

	class Type : public SchemaEntity
	{
	public:
		using SchemaEntity::SchemaEntity;
		using SchemaEntity::resolve;

		void children(std::set<DskObjectId>& children) const override;
		void dependents(std::set<DskObjectId>& dependents) const override;
		void GetNamesakes(std::set<DskObjectId>& namesakes) const override;

		bool isOriginal() const final;

	protected:
		bool lookup(const Object* ancestor, const QualifiedName& path) override;

		Data::Type* resolve(Data::Assembly& assembly, Data::Component* parent, bool shallow) const override;
	};

	class Class : public Type
	{
	public:
		using Type::Type;
		using Type::resolve;

		void children(std::set<DskObjectId>& children) const override;
		void dependents(std::set<DskObjectId>& dependents) const override;

	protected:
		bool lookup(const Object* ancestor, const QualifiedName& path) final;

		Data::Class* resolve(Data::Assembly& assembly, Data::Component* parent, bool shallow) const override;
	};

	class GUIClass : public Class
	{
	public:
		using Class::Class;

		void getAssociates(std::set<DskObjectId>& associates) const override;
	};

	class JadeInterface : public Type
	{
	public:
		using Type::Type;

		void dependents(std::set<DskObjectId>& dependents) const override;
		bool isShallow() const final;

	protected:
		bool lookup(const Object* ancestor, const QualifiedName& path) final;
	};
}
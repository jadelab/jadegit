#pragma once
#include "SchemaEntity.h"

namespace JadeGit::Extract
{
	class JadePackage : public SchemaEntity
	{
	public:
		using SchemaEntity::SchemaEntity;

	protected:
		bool lookup(const Object* ancestor, const QualifiedName& path) override;
	};

	class JadeExportedPackage : public JadePackage
	{
	public:
		using JadePackage::JadePackage;

	protected:
		void dependents(std::set<DskObjectId>& dependents) const final;
		bool lookup(const Object* ancestor, const QualifiedName& path) final;
	};

	class JadeImportedPackage : public JadePackage
	{
	public:
		using JadePackage::JadePackage;

	protected:
		void children(std::set<DskObjectId>& children) const final;
		bool lookup(const Object* ancestor, const QualifiedName& path) final;
		Data::Entity* resolve(Data::Assembly& assembly, Data::Component* parent, bool shallow) const final;
	};
}
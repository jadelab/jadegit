#include "NamedObject.h"
#include <jadegit/data/NamedObjectFactory.h>

using namespace std;

namespace JadeGit::Extract
{
	// Find the source object
	bool NamedObject::find(const Object* ancestor, const QualifiedName* path, bool required)
	{
		if (!path)
			throw invalid_argument("Fully qualified path is required");

		if (lookup(ancestor, *path))
			return true;

		if (required)
			throw runtime_error("Cannot find: " + static_cast<string>(*path));

		return false;
	}

	bool NamedObject::find(const Object* ancestor, const QualifiedName& path, bool required)
	{
		return find(ancestor, &path, required);
	}

	Data::Object* NamedObject::resolve(Data::Assembly& assembly, Data::Component* parent, bool shallow) const
	{
		// Resolve child object
		// NOTE: Unlike entities, this doesn't currently support mutating into a new type
		auto name = getName();
		if (auto object = Data::NamedObjectFactory::Get().Resolve(GetTypeName(), parent, QualifiedName(name), false, shallow, false))
			return object;

		// Instantiate new object, inferred when receiver may not be extracted because it's a shallow copy/proxy
		return Data::NamedObjectFactory::Get().Create(GetTypeName(), parent, name.c_str())->inferred(this->isShallow());
	}
}
#include "NamedObjectFactory.h"

using namespace std;

namespace JadeGit::Extract
{
	NamedObjectFactory& NamedObjectFactory::Get()
	{
		static NamedObjectFactory f; return f;
	}

	void NamedObjectFactory::Register(const ClassNumber& key, const Registration* registrar)
	{
		ObjectFactory::Register(key, registrar);
	}

	unique_ptr<NamedObject> NamedObjectFactory::Create(const DskObjectId& oid) const
	{
		return unique_ptr<NamedObject>(static_cast<const Registration*>(Lookup(oid.classNo))->Create(oid));
	}
}
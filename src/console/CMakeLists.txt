# Add console functionality to common object library
target_link_libraries(jadegit PRIVATE CLI11)
target_precompile_headers(jadegit PRIVATE <CLI/CLI.hpp>)

target_sources(jadegit PRIVATE
	${CMAKE_CURRENT_LIST_DIR}/Application.cpp
	${CMAKE_CURRENT_LIST_DIR}/Build.cpp
	${CMAKE_CURRENT_LIST_DIR}/Command.cpp
	${CMAKE_CURRENT_LIST_DIR}/CommandRegistry.cpp
	${CMAKE_CURRENT_LIST_DIR}/Commit.cpp
	${CMAKE_CURRENT_LIST_DIR}/Progress.cpp
	${CMAKE_CURRENT_LIST_DIR}/Registry.cpp
	${CMAKE_CURRENT_LIST_DIR}/Repository.cpp
	${CMAKE_CURRENT_LIST_DIR}/RepositoryCommand.cpp
	${CMAKE_CURRENT_LIST_DIR}/Session.cpp
	${CMAKE_CURRENT_LIST_DIR}/Switch.cpp
)

if (USE_JADE)
	target_sources(jadegit PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/Deploy.cpp
		${CMAKE_CURRENT_LIST_DIR}/Experimental.cpp
	)
endif()

if (USE_JADE AND USE_GIT2)
	target_sources(jadegit PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/Backfill.cpp
		${CMAKE_CURRENT_LIST_DIR}/Extract.cpp
	)
endif()

if (WITH_SCHEMA)
	target_sources(jadegit PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/Clone.cpp
		${CMAKE_CURRENT_LIST_DIR}/Fetch.cpp
		${CMAKE_CURRENT_LIST_DIR}/Install.cpp
	)
endif()

# Setup console executable
add_executable(jadegitcli ${jadegit_SOURCE_DIR}/src/jadegit.rc)
set_target_properties(jadegitcli PROPERTIES OUTPUT_NAME jadegit)
target_compile_definitions(jadegitcli PRIVATE JADEGIT_FILEDESC="jadegit console" JADEGIT_FILENAME="jadegit.exe" GIT_VERSION="${GIT_VERSION}")
target_include_directories(jadegitcli PRIVATE ${jadegit_SOURCE_DIR}/src)

install(TARGETS jadegitcli RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})
install(FILES $<TARGET_PDB_FILE:jadegitcli> DESTINATION ${CMAKE_INSTALL_BINDIR} OPTIONAL)

# Link console executable to schema library, otherwise common object library directly
if(WITH_SCHEMA)
	target_link_libraries(jadegitcli PRIVATE jadegitscm)
else()
	target_link_libraries(jadegitcli PRIVATE jadegit)
endif()

# Add console executable specific source files
target_sources(jadegitcli PRIVATE
	${CMAKE_CURRENT_LIST_DIR}/Main.cpp
)
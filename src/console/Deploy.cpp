#include "Command.h"
#include "CommandRegistration.h"
#include "Session.h"
#include <deploy/Commands.h>

using namespace std;
using namespace JadeGit;
using namespace JadeGit::Deploy;

namespace JadeGit::Console
{
	class DeployCommand : public Command
	{
	public:
		static constexpr const char* description = "Deployment commands";

		DeployCommand(CLI::App& cmd, Session& session) : Command(cmd, session)
		{
			// Need at least one subcommand
			cmd.require_subcommand(1);

			// Start
			auto start = cmd.add_subcommand("start", "Start deployment");
			start->add_option("--registry", this->registry, "Path to registry update file to be validated")->check(CLI::ExistingFile)->required();
			start->callback([this]() { Deploy::start(this->registry); });

			// Finish
			auto finish = cmd.add_subcommand("finish", "Finish deployment");
			finish->add_option("--registry", this->registry, "Path to registry update file to be loaded")->check(CLI::ExistingFile)->required();
			finish->callback([this]() { Deploy::finish(this->registry); });
		}

		void execute() final
		{
			// Not applicable
		}

	protected:
		string registry;
	};
	static CommandRegistration<DeployCommand, true> registration("deploy");
}
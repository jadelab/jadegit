#include <jadegit/vfs/FileSystem.h>
#include <jadegit/vfs/File.h>
#include <jadegit/vfs/AnyFileIterator.h>

namespace JadeGit
{
	FileSystem::FileSystem() {}
	FileSystem::~FileSystem() {}

	void FileSystem::flush() const {}

	FileSystem::operator bool() const
	{
		return this != &NullFileSystem::get();
	}

	AnyFileIterator::AnyFileIterator(std::unique_ptr<AnyFile>&& parent) : parent(std::move(parent)) {}
	AnyFileIterator::~AnyFileIterator() {}

	const NullFileSystem& NullFileSystem::get()
	{
		static NullFileSystem fs;
		return fs;
	}

	bool NullFileSystem::isReadOnly() const
	{
		return false;
	}

	void NullFileSystem::flush() const
	{
	}

	File NullFileSystem::open(const std::filesystem::path& path) const
	{
		return File();
	}
}
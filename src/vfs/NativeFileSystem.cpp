#include <jadegit/vfs/NativeFileSystem.h>
#include <jadegit/vfs/File.h>
#include "NativeFile.h"

using namespace std;

namespace fs = filesystem;

namespace JadeGit
{
	NativeFileSystem::NativeFileSystem(const fs::path& base, bool writable, const FileSignature* signature) : base(base), writable(writable), signature(signature)
	{
		// Ignore empty base (current working directory)
		if (base.empty())
			return;

		// Check if base path already exists
		if (fs::exists(base))
		{
			// Ensure base path is a directory
			if (!fs::is_directory(base))
				throw runtime_error(base.generic_string() + " is not a directory");
		}
		else
		{
			// Create directory if allowed
			if (writable)
				fs::create_directories(base);
			else
				throw runtime_error(base.generic_string() + " doesn't exist");
		}
	}

	NativeFileSystem::~NativeFileSystem() {}

	bool NativeFileSystem::isReadOnly() const
	{
		return !writable;
	}

	File NativeFileSystem::open(const fs::path& path) const
	{
		return File(make_unique<NativeFile>(*this, path));
	}
}
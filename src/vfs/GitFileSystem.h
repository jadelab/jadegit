#pragma once
#include <jadegit/git2.h>
#include <jadegit/vfs/FileSystem.h>
#include <map>

namespace JadeGit
{
	class GitFileSystemSource;

	class GitFileSystem : public FileSystem
	{
	public:
		GitFileSystem(git_repository* repo, const FileSignature* signature = nullptr);
		GitFileSystem(git_index* index, const FileSignature* signature = nullptr);
		GitFileSystem(const git_commit* commit, const FileSignature* signature = nullptr);
		GitFileSystem(std::unique_ptr<git_commit> commit, const FileSignature* signature = nullptr);
		~GitFileSystem();

		void add(std::unique_ptr<git_commit> commit);
		void add(std::unique_ptr<git_reference> reference);
		void add(std::string author, git_oid tree, std::unique_ptr<git_commit> ours, std::unique_ptr<git_commit> theirs = std::unique_ptr<git_commit>());

		void flush() const final;
		bool isReadOnly() const final;
		File open(const std::filesystem::path& path) const final;

		operator git_repository* () const;
		operator git_index* () const;
		operator git_tree* () const;

	private:
		friend class GitFileBlame;
		friend class GitIndexFile;
		friend class GitTreeFile;

		void add(std::unique_ptr<GitFileSystemSource> source);

		git_repository* const repo;
		git_index* const index;
		std::shared_ptr<git_tree> our_tree;

		std::unique_ptr<GitFileSystemSource> first;

		const FileSignature* signature = nullptr;
		mutable std::map<git_oid, FileSignature> signatures;
	};
}
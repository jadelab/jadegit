#include "NativeFileIterator.h"
#include <jadegit/vfs/NativeFileSystem.h>

namespace JadeGit
{
	NativeFileIterator::NativeFileIterator(std::unique_ptr<AnyFile>&& parent, const std::filesystem::directory_iterator& iter) : AnyFileIterator(std::move(parent)), iter(iter) {}

	NativeFileIterator::~NativeFileIterator() {}

	std::unique_ptr<AnyFileIterator> NativeFileIterator::clone() const
	{
		return std::make_unique<NativeFileIterator>(parent->clone(), iter);
	}

	bool NativeFileIterator::valid() const
	{
		return iter != std::filesystem::end(iter);
	}

	std::filesystem::path NativeFileIterator::filename() const
	{
		return (*iter).path().filename();
	}

	void NativeFileIterator::next()
	{
		iter++;
	}
}
#include "MemoryFileIterator.h"
#include "MemoryFile.h"

namespace JadeGit
{
	MemoryFileIterator::MemoryFileIterator(std::unique_ptr<AnyFile>&& parent, const MemoryFileDirectory& directory) : AnyFileIterator(std::move(parent)), directory(directory)
	{
		iter = directory.begin();
	}

	MemoryFileIterator::~MemoryFileIterator() {}

	std::unique_ptr<AnyFileIterator> MemoryFileIterator::clone() const
	{
		auto clone = std::make_unique<MemoryFileIterator>(parent->clone(), directory);
		clone->iter = iter;
		return clone;
	}

	bool MemoryFileIterator::valid() const
	{
		return iter != directory.end();
	}

	std::filesystem::path MemoryFileIterator::filename() const
	{
		return (*iter).first;
	}

	void MemoryFileIterator::next()
	{
		iter++;
	}
}
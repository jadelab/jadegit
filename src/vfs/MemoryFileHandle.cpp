#include "MemoryFileHandle.h"
#include "MemoryFile.h"
#include "MemoryFileIterator.h"
#include "MemoryFileStream.h"
#include <jadegit/vfs/MemoryFileSystem.h>

using namespace std;

namespace JadeGit
{
	MemoryFileHandle::MemoryFileHandle(const MemoryFileSystem& fs, const filesystem::path& path) : fs(fs), relative_path(path)
	{
		file = path.empty() ? fs.root : fs.root->find(path);
	}

	MemoryFileHandle::~MemoryFileHandle() {}

	unique_ptr<AnyFile> MemoryFileHandle::clone() const
	{
		return make_unique<MemoryFileHandle>(fs, relative_path);
	}

	const FileSystem* MemoryFileHandle::vfs() const
	{
		return &fs;
	}

	filesystem::path MemoryFileHandle::path() const
	{
		return relative_path;
	}

	bool MemoryFileHandle::exists() const
	{
		return !!file;
	}

	bool MemoryFileHandle::isDirectory() const
	{
		return file && file->isDirectory();
	}

	unique_ptr<AnyFileIterator> MemoryFileHandle::begin() const
	{
		return isDirectory() ? make_unique<MemoryFileIterator>(clone(), *static_cast<MemoryFileDirectory*>(file)) : nullptr;
	}

	void MemoryFileHandle::remove()
	{
		if (!file)
			throw runtime_error("File does not exist");

		file->remove();
		file = nullptr;
	}

	unique_ptr<istream> MemoryFileHandle::createInputStream(ios_base::openmode mode) const
	{
		/* Check file already exists */
		if (!file)
			throw runtime_error("File does not exist");

		/* Check file is not a directory */
		if (file->isDirectory())
			throw runtime_error("Cannot create input stream for directory");

		return make_unique<MemoryFileStream>(*static_cast<MemoryFile*>(file), mode);
	}

	unique_ptr<ostream> MemoryFileHandle::createOutputStream(ios_base::openmode mode)
	{
		/* Make the file if we have to */
		if (!file)
			file = fs.root->make(relative_path);

		return make_unique<MemoryFileStream>(*static_cast<MemoryFile*>(file), mode);
	}
}
target_sources(jadegit PRIVATE
	${CMAKE_CURRENT_LIST_DIR}/Director.cpp
	${CMAKE_CURRENT_LIST_DIR}/DirectoryBuilder.cpp
	${CMAKE_CURRENT_LIST_DIR}/JARIDeploymentBuilder.cpp
	${CMAKE_CURRENT_LIST_DIR}/MultiExtractBuilder.cpp
	${CMAKE_CURRENT_LIST_DIR}/PowerShellDeploymentBuilder.cpp
	${CMAKE_CURRENT_LIST_DIR}/XMLDeploymentBuilder.cpp
)

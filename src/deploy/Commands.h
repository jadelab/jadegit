#pragma once
#include <filesystem>

namespace JadeGit::Deploy
{
	void start(const std::filesystem::path& registry);
	void finish(const std::filesystem::path& registry);
}
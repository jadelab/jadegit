#pragma once
#include <jadegit/data/EntityFactory.h>
#include <jadegit/data/EntityDict.h>
#include "NamedObjectRegistration.h"

namespace JadeGit::Data
{
	template<class T>
	concept EntityParent = Derived<T, Entity> || std::is_same<T, Assembly>::value;

	template<Derived<Entity> TDerived, typename = typename TDerived::Parents>
	class EntityRegistration;

	template<Derived<Entity> TDerived, EntityParent TParent, EntityParent... TParents>
	class EntityRegistration<TDerived, ObjectParents<TParent, TParents...>> : public EntityRegistration<TDerived, ObjectParents<TParents...>>
	{
	public:
		using EntityRegistration<TDerived, ObjectParents<TParents...>>::EntityRegistration;

	protected:
		using Parent = TParent;

		using EntityRegistration<TDerived, ObjectParents<TParents...>>::Resolve;

	private:

	};

	template<Derived<Entity> TDerived, EntityParent TParent>
	class EntityRegistration<TDerived, ObjectParents<TParent>> : protected NamedObjectRegistration<TDerived, EntityFactory::Registration>
	{
		static_assert(std::is_abstract_v<TDerived> || std::is_constructible_v<TDerived, TParent*, const Class*, const char*> || std::is_constructible_v<TDerived, TParent&, const Class*, const char*>, "Derived entity cannot be constructed");

	public:
		template <typename... Rest>
		EntityRegistration(const char* key, Rest... rest) : NamedObjectRegistration<TDerived, EntityFactory::Registration>(key, rest...)
		{
			EntityFactory::Get().Register<TDerived>(key, this);
		}

		using NamedObjectRegistration<TDerived, EntityFactory::Registration>::Resolve;

	protected:
		using Parent = TParent;
		const std::string alias;

		TDerived* load(Assembly* assembly, const Class* dataClass, const std::filesystem::path& path) const override
		{
			// Resolve parent
			auto parent = ResolveParent(assembly, path);
			assert(parent);

			// Derive entity name from path stem
			auto name = path.stem().string();

			// Return existing
			if (TDerived* result = this->lookup(parent, name))
				return result;

			// Create new
			return this->CreateInstance<TParent, const Class*, const char*>(parent, dataClass, name.c_str());
		}

		TDerived* Resolve(const Assembly* assembly, const std::filesystem::path& path, bool expected, bool shallow) const override
		{
			// Resolve parent
			auto parent = ResolveParent(assembly, path, expected);
			if (!parent)
				return nullptr;

			// Derive entity name from path stem
			auto name = path.stem().string();

			// Attempt to resolve from collection
			if (TDerived* result = Resolve(parent, name.c_str(), shallow, false))
				return result;

			if (expected)
				throw std::runtime_error(std::format("Failed to resolve {} [{}]", alias, path.generic_string()));

			return nullptr;
		}

		Parent* ResolveParent(const Assembly* assembly, const std::filesystem::path& path, bool expected = true) const
		{
			// Get parent path
			std::filesystem::path parent_path = path.parent_path();

			if constexpr (!is_major_entity<TDerived>)
				throw std::runtime_error(std::format("{} entities must be embedded in parent file", this->key));
			else
			{
				// Verify & remove sub-folder used for this kind of entity
				std::filesystem::path subFolder = TDerived::subFolder;
				if (!subFolder.empty())
				{
					if (parent_path.stem() != subFolder)
						throw std::runtime_error(std::format("Unexpected path for {} [{}]", alias, path.generic_string()));

					parent_path = parent_path.parent_path();
				}
			}

			// Resolve parent
			return EntityFactory::Get().Resolve<Parent>(assembly, parent_path, expected, true);
		}
	};
}
#include <jadegit/data/RelationalAttribute.h>
#include <jadegit/data/EntityVisitor.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/RACollectionValueMeta.h>
#include <jadegit/data/RootSchema/RAMethodMeta.h>
#include <jadegit/data/RootSchema/RAOidMeta.h>
#include <jadegit/data/RootSchema/RAParameterMeta.h>
#include <jadegit/data/RootSchema/RAPropertyMeta.h>
#include <jadegit/data/RootSchema/RARelationIndexMeta.h>
#include <jadegit/data/RootSchema/RARelationKeyMeta.h>
#include <jadegit/data/RootSchema/RARelationOidMeta.h>
#include <jadegit/data/RootSchema/RARpsPropertyMeta.h>
#include <jadegit/data/CollClass.h>
#include "NamedObjectRegistration.h"

namespace JadeGit::Data
{
	static NamedObjectRegistration<RelationalAttribute> relationalAttribute("RelationalAttribute", &RelationalTable::attributes);
	static NamedObjectRegistration<RAMethod> raMethod("RAMethod", std::mem_fn(&ExternalRPSClassMap::getConstraint), &RelationalTable::attributes);

	template ObjectValue<RelationalTable* const, &RelationalAttributeMeta::table>;

	RelationalAttribute::RelationalAttribute(RelationalTable& parent, const Class* dataClass, const char* name) : NamedObject(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::relationalAttribute), name),
		table(parent)
	{
	}

	RelationalAttribute::RelationalAttribute(Object& parent, const Class* dataClass, const char* name) : NamedObject(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::relationalAttribute), name),
		table(nullptr)
	{
	}

	template ObjectValue<ExternalRPSClassMap* const, &RAMethodMeta::rpsClassMap>;

	RAMethod::RAMethod(RelationalTable& parent, const Class* dataClass, const char* name) : RelationalAttribute(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::raMethod), name),
		classMap(nullptr)
	{
	}

	RAMethod::RAMethod(ExternalRPSClassMap& parent, const Class* dataClass, const char* name) : RelationalAttribute(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::raMethod), name),
		classMap(parent)
	{
	}

	RelationalAttributeMeta::RelationalAttributeMeta(RootSchema& parent, const ObjectMeta& superclass) : RootClass(parent, "RelationalAttribute", superclass),
		attrName(NewString("attrName", 101)),
		decimals(NewInteger("decimals")),
		feature(NewReference<ExplicitInverseRef>("feature", NewType<Class>("Feature"))),
		length(NewInteger("length")),
		name(NewString("name", 101)),
		referencedClassName(NewString("referencedClassName", 101)),
		rpsColType(NewInteger("rpsColType")),
		rpsExcluded(NewBoolean("rpsExcluded")),
		rpsKeyKind(NewInteger("rpsKeyKind")),
		rpsMapKind(NewInteger("rpsMapKind")),
		sqlType(NewInteger("sqlType")),
		table(NewReference<ExplicitInverseRef>("table", NewType<Class>("RelationalTable")))
	{
		name->unwritten().bind(&RelationalAttribute::name);
		table->manual().child().bind(&RelationalAttribute::table);
	}

	RACollectionValueMeta::RACollectionValueMeta(RootSchema& parent, const RelationalAttributeMeta& superclass) : RootClass(parent, "RACollectionValue", superclass) {}

	RAMethodMeta::RAMethodMeta(RootSchema& parent, const RelationalAttributeMeta& superclass) : RootClass(parent, "RAMethod", superclass),
		rpsClassMap(NewReference<ExplicitInverseRef>("rpsClassMap", NewType<Class>("ExternalRPSClassMap")))
	{
		rpsClassMap->automatic().child().bind(&RAMethod::classMap);
	}

	RAOidMeta::RAOidMeta(RootSchema& parent, const RelationalAttributeMeta& superclass) : RootClass(parent, "RAOid", superclass) {}

	RAParameterMeta::RAParameterMeta(RootSchema& parent, const RelationalAttributeMeta& superclass) : RootClass(parent, "RAParameter", superclass) {}

	RAPropertyMeta::RAPropertyMeta(RootSchema& parent, const RelationalAttributeMeta& superclass) : RootClass(parent, "RAProperty", superclass) {}

	RARelationIndexMeta::RARelationIndexMeta(RootSchema& parent, const RelationalAttributeMeta& superclass) : RootClass(parent, "RARelationIndex", superclass),
		fromFirstClass(NewBoolean("fromFirstClass")),
		propertyName(NewString("propertyName", 101)) {}

	RARelationKeyMeta::RARelationKeyMeta(RootSchema& parent, const RelationalAttributeMeta& superclass) : RootClass(parent, "RARelationKey", superclass),
		keyIndex(NewInteger("keyIndex")) {}

	RARelationOidMeta::RARelationOidMeta(RootSchema& parent, const RelationalAttributeMeta& superclass) : RootClass(parent, "RARelationOid", superclass),
		fromFirstClass(NewBoolean("fromFirstClass")) {}

	RARpsPropertyMeta::RARpsPropertyMeta(RootSchema& parent, const RelationalAttributeMeta& superclass) : RootClass(parent, "RARpsProperty", superclass),
		excluded(NewBoolean("excluded")),
		guid(NewString("guid", 39)) {}
}
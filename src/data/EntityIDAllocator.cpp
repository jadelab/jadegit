#include "EntityIDAllocator.h"
#include <ranges>

using namespace uuids;

namespace JadeGit::Data
{
	thread_local EntityIDAllocator* allocator = nullptr;

	void EntityIDAllocator::set(EntityIDAllocator* replacement)
	{
		allocator = replacement;
	}

	template<class T = std::mt19937, std::size_t N = T::state_size * sizeof(typename T::result_type)>
	T makeRandomEngine()
	{
		std::random_device source;
		auto random_data = std::views::iota(std::size_t(), (N - 1) / sizeof(source()) + 1) | std::views::transform([&](auto) { return source(); });
		std::seed_seq seeds(std::begin(random_data), std::end(random_data));
		return T(seeds);
	}

	class RandomIDAllocator : public EntityIDAllocator
	{
	public:
		uuid allocate(const Entity& entity) final
		{
			thread_local auto engine = makeRandomEngine();
			thread_local uuids::uuid_random_generator generate(engine);
			return generate();
		}
	};

	EntityIDAllocator& EntityIDAllocator::get()
	{
		if (allocator)
			return *allocator;

		static RandomIDAllocator allocator;
		return allocator;
	}
}
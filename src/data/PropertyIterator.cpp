#include <jadegit/data/PropertyIterator.h>

namespace JadeGit::Data
{
	PropertyIterator::PropertyIterator() noexcept {}

	PropertyIterator::PropertyIterator(const Class* cls)
	{
		if (cls)
		{
			// Get root type and ensure properties have been laoded
			this->cls = &cls->getRootType();
			this->cls->Load();

			// Setup initial property iterator
			iter = this->cls->properties.begin();

			// Recurse up class hierarchy until next entry is found
			Recurse();
		}
	}

	void PropertyIterator::Recurse()
	{
		while (iter == cls->properties.end())
		{
			if (cls = cls->getSuperClass())
			{
				cls = &cls->getRootType();
				cls->Load();

				iter = cls->properties.begin();
			}
			else
			{
				iter = EntityDict<Property, &ClassMeta::properties>::const_iterator();
				break;
			}
		}
	}

	const Property& PropertyIterator::operator*() const
	{
		return *(iter->second);
	}

	const Property* PropertyIterator::operator->() const
	{
		return iter->second;
	}

	PropertyIterator& PropertyIterator::operator++()
	{
		if (cls)
		{
			// Move current iterator forward
			iter++;

			// Recurse through class hierarchy if current iterator has ended
			Recurse();
		}

		return *this;
	}

	bool PropertyIterator::operator==(const PropertyIterator& it) const
	{
		return cls == it.cls && iter == it.iter;
	}

	bool PropertyIterator::operator!=(const PropertyIterator& it) const
	{
		return !((*this) == it);
	}

	// Range based support
	PropertyIterator begin(PropertyIterator iter) noexcept
	{
		return iter;
	}

	PropertyIterator end(const PropertyIterator&) noexcept
	{
		return PropertyIterator();
	}
}
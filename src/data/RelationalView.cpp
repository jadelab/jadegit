#include <jadegit/data/RelationalView.h>
#include <jadegit/data/Schema.h>
#include <jadegit/data/EntityVisitor.h>
#include <jadegit/data/RootSchema.h>
#include <jadegit/data/RootSchema/ExternalRPSClassMapMeta.h>
#include <jadegit/data/RootSchema/JadeOdbcViewMeta.h>
#include <jadegit/data/RootSchema/JadeRpsMappingMeta.h>
#include <jadegit/data/CollClass.h>
#include "EntityRegistration.h"

namespace JadeGit::Data
{
	const std::filesystem::path JadeOdbcView::subFolder("relational-views");
	const std::filesystem::path JadeRpsMapping::subFolder("rps-mappings");

	static EntityRegistration<JadeOdbcView> relationalView("RelationalView", &Schema::relationalViews);		// Defined for backwards compatibility with extracts prior to defining subclasses
	static EntityRegistration<JadeOdbcView> jadeOdbcView("JadeOdbcView", &Schema::relationalViews);
	static EntityRegistration<JadeRpsMapping> jadeRpsMapping("JadeRpsMapping", &Schema::rpsDatabases);
	static ObjectRegistration<ExternalRPSClassMap> externalRPSClassMap("ExternalRPSClassMap");

	template ObjectValue<Array<ExternalRPSClassMap*>, &RelationalViewMeta::rpsClassMaps>;
	template ObjectValue<Schema* const, &RelationalViewMeta::schema>;
	template NamedObjectDict<RelationalTable, &RelationalViewMeta::tables>;

	JadeOdbcView::JadeOdbcView(Schema& parent, const Class* dataClass, const char* name) : MajorEntity(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::jadeOdbcView), name)
	{
		schema = parent;
	}

	void JadeOdbcView::Accept(EntityVisitor& v)
	{
		v.Visit(this);
	}

	JadeRpsMapping::JadeRpsMapping(Schema& parent, const Class* dataClass, const char* name) : MajorEntity(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::jadeRpsMapping), name)
	{
		schema = parent;
	}

	void JadeRpsMapping::Accept(EntityVisitor& v)
	{
		v.Visit(this);
	}

	template ObjectValue<RAMethod*, &ExternalRPSClassMapMeta::constraint>;
	template ObjectValue<Class*, &ExternalRPSClassMapMeta::rpsClass>;
	template ObjectValue<JadeRpsMapping* const, &ExternalRPSClassMapMeta::rpsDatabase>;
	template NamedObjectDict<RelationalTable, &ExternalRPSClassMapMeta::tables>;

	ExternalRPSClassMap::ExternalRPSClassMap(JadeRpsMapping& parent, const Class* dataClass) : Object(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::externalRPSClassMap)),
		rpsDatabase(parent)
	{
	}

	RAMethod* ExternalRPSClassMap::getConstraint(const std::string& name) const
	{
		auto constraint = static_cast<RAMethod*>(this->constraint);
		return constraint && constraint->name == name ? constraint : nullptr;
	}

	RelationalViewMeta::RelationalViewMeta(RootSchema& parent, const ObjectMeta& superclass) : RootClass(parent, "RelationalView", superclass),
		creator(NewString("creator", 31)),
		defaultVisibility(NewInteger("defaultVisibility")),
		includeSystemClasses(NewBoolean("includeSystemClasses")),
		includedObjectFeatures(NewReference<ImplicitInverseRef>("includedObjectFeatures", NewType<CollClass>("FeatureArray"))),
		name(NewString("name", 101)),
		resyncOnExecute(NewBoolean("resyncOnExecute")),
		rootClass(NewString("rootClass", 101)),
		rpsClassMaps(NewReference<ExplicitInverseRef>("rpsClassMaps", NewType<CollClass>("ExternalRPSClassMapSet"))),
		rpsDatabaseName(NewString("rpsDatabaseName", 129)),
		rpsDatabaseType(NewInteger("rpsDatabaseType")),
		rpsDefaultConnectionString(NewString("rpsDefaultConnectionString")),
		rpsDefaultPassword(NewString("rpsDefaultPassword", 129)),
		rpsDefaultUserName(NewString("rpsDefaultUserName", 31)),
		rpsExceptionCreate(NewInteger("rpsExceptionCreate")),
		rpsExceptionDelete(NewInteger("rpsExceptionDelete")),
		rpsExceptionUpdate(NewInteger("rpsExceptionUpdate")),
		rpsLoggingOptions(NewInteger("rpsLoggingOptions")),
		rpsShowMethods(NewBoolean("rpsShowMethods")),
		rpsShowObjectFeatures(NewBoolean("rpsShowObjectFeatures")),
		rpsShowVirtualProperties(NewInteger("rpsShowVirtualProperties")),
		rpsTopSchemaName(NewString("rpsTopSchemaName", 101)),
		rpsUseOidClassInstMap(NewBoolean("rpsUseOidClassInstMap")),
		schema(NewReference<ExplicitInverseRef>("schema", NewType<Class>("Schema"))),
		securityMethod(NewString("securityMethod", 101)),
		tables(NewReference<ExplicitInverseRef>("tables", NewType<CollClass>("RelationalTableDict"))),
		timeCreated(NewTimeStamp("timeCreated")),
		uuid(NewBinary("uuid", 16))
	{
		name->unwritten().bind(&RelationalView::name);
		rpsClassMaps->automatic().parent().bind(&RelationalView::rpsClassMaps);
		schema->manual().child().bind(&RelationalView::schema);
		tables->automatic().parent().bind(&RelationalView::tables);
	}

	JadeOdbcViewMeta::JadeOdbcViewMeta(RootSchema& parent, const RelationalViewMeta& superclass) : RootClass(parent, "JadeOdbcView", superclass) {}

	JadeRpsMappingMeta::JadeRpsMappingMeta(RootSchema& parent, const RelationalViewMeta& superclass) : RootClass(parent, "JadeRpsMapping", superclass) {}

	ExternalRPSClassMapMeta::ExternalRPSClassMapMeta(RootSchema& parent, const ObjectMeta& superclass) : RootClass(parent, "ExternalRPSClassMap", superclass),
		constraint(NewReference<ExplicitInverseRef>("constraint", NewType<Class>("RAMethod"))),
		excluded(NewBoolean("excluded")),
		guid(NewString("guid", 39)),
		includeInCallback(NewBoolean("includeInCallback")),
		noDeletes(NewBoolean("noDeletes")),
		rpsClass(NewReference<ExplicitInverseRef>("rpsClass", NewType<Class>("Class"))),
		rpsDatabase(NewReference<ExplicitInverseRef>("rpsDatabase", NewType<Class>("RelationalView"))),
		tableMode(NewInteger("tableMode")),
		tables(NewReference<ExplicitInverseRef>("tables", NewType<CollClass>("RelationalTableDict"))),
		typeMapValue(NewBinary("typeMapValue"))
	{
		constraint->manual().parent().bind(&ExternalRPSClassMap::constraint);
		rpsClass->manual().bind(&ExternalRPSClassMap::rpsClass);
		rpsDatabase->manual().child().bind(&ExternalRPSClassMap::rpsDatabase);
		tables->automatic().parent().bind(&ExternalRPSClassMap::tables);
	}
}
#include <jadegit/data/Application.h>
#include <jadegit/data/CollClass.h>
#include <jadegit/data/Schema.h>
#include <jadegit/data/EntityVisitor.h>
#include <jadegit/data/JadeWebServiceManager.h>
#include <jadegit/data/RootSchema.h>
#include "SchemaEntityRegistration.h"
#include <ranges>

using namespace std;

namespace JadeGit::Data
{
	const filesystem::path Application::subFolder("applications");

	DEFINE_OBJECT_CAST(Application);
	DEFINE_OBJECT_CAST(JadeWebServiceManager);

	static SchemaEntityRegistration<Application> application("Application", &Schema::applications);
	static ObjectRegistration<JadeWebServiceManager> jadeWebServiceManager("JadeWebServiceManager");

	// TODO: Further work/consideration needed to enable using the longer/friendly type names proposed below, may have to avoid using whitespace
	// When initially attempting this, the enumeration parsing/conversion failed because reading the input stream stops on first space in name
	map<Application::Type, const char*> EnumStrings<Application::Type>::data =
	{
		{ Application::Type::GUI, "G" },					// GUI
		{ Application::Type::GUI_No_Forms, "F" },			// GUI, No Forms
		{ Application::Type::Non_GUI, "S" },				// Non-GUI
		{ Application::Type::REST_Service, "R" },			// REST Service
		{ Application::Type::REST_Service_Non_GUI, "T" },	// REST Service, Non-GUI
		{ Application::Type::Web_Enabled, "W" },			// Web Enabled
		{ Application::Type::Web_Enabled_Non_GUI, "N" }		// Web Enabled, Non-GUI
	};

	template Value<Application::Type>;
	template ObjectValue<Set<JadeExportedPackage*>, &ApplicationMeta::exportedPackages>;
	template ObjectValue<JadeWebServiceManager*, &ApplicationMeta::jadeWebServiceManager>;
	template ObjectValue<Schema* const, &ApplicationMeta::schema>;

	Application::Application(Schema* parent, const Class* dataClass, const char* name) : MajorEntity(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::application), name),
		schema(parent)
	{
	}

	void Application::Accept(EntityVisitor &v)
	{
		v.Visit(this);
	}

	template ObjectValue<Application* const, &JadeWebServiceManagerMeta::application>;
	template ObjectValue<Array<JadeExposedList*>, &JadeWebServiceManagerMeta::_listOfExposures>;

	JadeWebServiceManager::JadeWebServiceManager(Application& parent, const Class* dataClass) : Object(parent, dataClass ? dataClass : GetDataClass(parent, &RootSchema::jadeWebServiceManager)),
		application(&parent)
	{
	}

	void JadeWebServiceManager::loaded(bool strict, queue<future<void>>& tasks)
	{
		Object::loaded(strict, tasks);

		// Handle web service classes to/from exposure list conversion
		tasks.push(async(launch::deferred, [this]()
			{
				// Check if exposure list needs to be populated from original web service classes property
				if (!exposures.size() && !application->webServiceClasses.empty())
				{
					auto names = ranges::views::split(application->webServiceClasses, ' ');

					for (const auto& token : names)
					{
						if (string name(token.begin(), token.end()); !name.empty())
						{
							// Resolve/add valid exposures to list
							if (auto exposure = Entity::resolve<JadeExposedList>(*this, name))
								exposures.Add(exposure);
						}
					}
				}

				// Rebuild original web service classes property from exposures list (still used by ddx)
				string webServiceClasses;
				for (auto& exposure : exposures)
					webServiceClasses += " " + exposure->name;
				application->webServiceClasses = webServiceClasses;
			}));
	}

	ApplicationMeta::ApplicationMeta(RootSchema& parent, const ObjectMeta& superclass) : RootClass(parent, "Application", superclass),
		aboutFormName(NewString("aboutFormName", 101)),
		appVersion(NewString("appVersion", 31)),
		applicationType(NewCharacter("applicationType")),
		controlSpacing(NewInteger("controlSpacing")),
		defaultApp(NewBoolean("defaultApp")),
		defaultLocaleId(NewInteger("defaultLocaleId")),
		defaultMdi(NewBoolean("defaultMdi")),
		exportedPackages(NewReference<ExplicitInverseRef>("exportedPackages", NewType<CollClass>("JadeExportedPackageNDict"))),
		finalizeMethod(NewReference<ImplicitInverseRef>("finalizeMethod", NewType<Class>("Method"))),
		fontBold(NewBoolean("fontBold")),
		fontName(NewString("fontName", 101)),
		fontSize(NewReal("fontSize")),
		formMargin(NewInteger("formMargin")),
		heightSingleLineControl(NewInteger("heightSingleLineControl")),
		helpFile(NewString("helpFile")),
		icon(NewBinary("icon")),
		initializeMethod(NewReference<ImplicitInverseRef>("initializeMethod", NewType<Class>("Method"))),
		internetPipeName(NewString("internetPipeName")),
		jadeWebServiceManager(NewReference<ExplicitInverseRef>("jadeWebServiceManager", NewType<Class>("JadeWebServiceManager"))),
		mdiStyle(NewInteger(Version(20, 0, 1), "mdiStyle")),
		mdiWindowListOrder(NewInteger(Version(20, 0, 1), "mdiWindowListOrder")),
		name(NewString("name", 101)),
		numberOfPipes(NewInteger("numberOfPipes")),
		schema(NewReference<ExplicitInverseRef>("schema", NewType<Class>("Schema"))),
		startupFormName(NewString("startupFormName", 101)),
		threeDControls(NewReference<ImplicitInverseRef>("threeDControls", NewType<CollClass>("ClassColl"))),
		useBorderStyleOnly(NewBoolean("useBorderStyleOnly")),
		webAppDirectory(NewString("webAppDirectory")),
		webBaseURL(NewString("webBaseURL")),
		webDefaultTimeZone(NewString(Version(22, 0, 1), "webDefaultTimeZone")),
		webDisplayMessages(NewBoolean("webDisplayMessages")),
		webDisplayPreference(NewCharacter("webDisplayPreference")),
		webEventClasses(NewReference<ImplicitInverseRef>("webEventClasses", NewType<CollClass>("ClassColl"))),
		webHomePage(NewString("webHomePage", 101)),
		webMachineName(NewString("webMachineName")),
		webMaxHTMLSize(NewInteger("webMaxHTMLSize")),
		webMinimumResponseTime(NewInteger("webMinimumResponseTime")),
		webServiceClasses(NewString("webServiceClasses")),
		webSessionTimeout(NewInteger("webSessionTimeout")),
		webShowModal(NewBoolean("webShowModal")),
		webStatusLineDisplay(NewString("webStatusLineDisplay")),
		webUseHTML32(NewBoolean("webUseHTML32")),
		webVirtualDirectory(NewString("webVirtualDirectory"))
	{
		aboutFormName->alias("_aboutFormName");
		applicationType->bind(&Application::applicationType);
		defaultApp->bind(&Application::defaultApp);
		defaultLocaleId->alias("_defaultLocaleId");
		exportedPackages->automatic().bind(&Application::exportedPackages);
		finalizeMethod->bind(&Application::finalizeMethod);
		initializeMethod->bind(&Application::initializeMethod);
		internetPipeName->alias("_internetPipeName");
		jadeWebServiceManager->automatic().parent().bind(&Application::webServiceManager);
		name->unwritten().bind(&Application::name);
		numberOfPipes->alias("_numberOfPipes");
		schema->manual().child().bind(&Application::schema);
		startupFormName->alias("_startupFormName");
		webAppDirectory->alias("_webAppDirectory");
		webBaseURL->alias("_webBaseURL");
		webDefaultTimeZone->alias("_webDefaultTimeZone");
		webDisplayMessages->alias("_webDisplayMessages");
		webDisplayPreference->alias("_webDisplayPreference");
		webEventClasses->alias("_webEventClasses");
		webHomePage->alias("_webHomePage");
		webMachineName->alias("_webMachineName");
		webMaxHTMLSize->alias("_webMaxHTMLSize");
		webServiceClasses->alias("_webServiceClasses").unwritten().bind(&Application::webServiceClasses);
		webSessionTimeout->alias("_webSessionTimeout");
		webShowModal->alias("_webShowModal");
		webStatusLineDisplay->alias("_webStatusLineDisplay");
		webUseHTML32->alias("_webUseHTML32");
		webVirtualDirectory->alias("_webVirtualDirectory");
	}

	JadeWebServiceManagerMeta::JadeWebServiceManagerMeta(RootSchema& parent, const ObjectMeta& superclass) : RootClass(parent, "JadeWebServiceManager", superclass),
		allowedSchemes(NewString("allowedSchemes", 11)),
		_listOfExposures(NewReference<ExplicitInverseRef>("exposures", NewType<CollClass>("JadeExposedListNDict"))),
		application(NewReference<ExplicitInverseRef>("application", NewType<Class>("Application"))),
		provider(NewBoolean("provider")),
		secureService(NewBoolean("secureService")),
		sessionHandling(NewBoolean("sessionHandling")),
		supportLibrary(NewString("supportLibrary", 61)),
		targetNamespace(NewString("targetNamespace")),
		useEncodedFormat(NewBoolean("useEncodedFormat")),
		useHttpGet(NewBoolean("useHttpGet")),
		useHttpPost(NewBoolean("useHttpPost")),
		useRPC(NewBoolean("useRPC")),
		useSOAP11(NewBoolean("useSOAP11")),
		useSOAP12(NewBoolean("useSOAP12")),
		versionControl(NewBoolean("versionControl"))
	{
		allowedSchemes->alias("_allowedSchemes");
		_listOfExposures->manual().alias("_listOfExposures").bind(&JadeWebServiceManager::exposures);
		application->manual().child().bind(&JadeWebServiceManager::application);
		useEncodedFormat->alias("handleCircularReferences");
		useSOAP11->alias("_useSOAP11");
		useSOAP12->alias("_useSOAP12");
	}
}
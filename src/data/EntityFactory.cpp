#include <jadegit/data/EntityFactory.h>
#include <jadegit/data/Assembly.h>

using namespace std;

namespace JadeGit::Data
{
	EntityFactory& EntityFactory::Get()
	{
		static EntityFactory f; return f;
	}

	Entity* EntityFactory::load(const string& key, Assembly* assembly, const filesystem::path& path) const
	{
		Class* dataClass = nullptr;
		return Lookup(key, assembly, dataClass)->load(assembly, dataClass, path);
	}

	const EntityFactory::Registration* EntityFactory::Lookup(const string& key, const Component* origin, Class*& dataClass, bool required) const
	{
		return static_cast<const Registration*>(ObjectFactory::Lookup(key, origin, dataClass, required));
	}

	const EntityFactory::Registration* EntityFactory::Lookup(const type_info& type) const
	{
		return static_cast<const Registration*>(ObjectFactory::Lookup(type));
	}

	Assembly* EntityFactory::Resolve(const Assembly* assembly, const filesystem::path& path) const
	{
		if (path == filesystem::path())
			return const_cast<Assembly*>(assembly);

		throw logic_error("Unexpected assembly path [" + path.generic_string() + "]");
	}
}
#include "ObjectFileStorage.h"
#include "ObjectFileFormat.h"
#include "ObjectLoader.h"
#include <jadegit/data/Assembly.h>
#include <jadegit/data/Entity.h>
#include <jadegit/vfs/FileIterator.h>
#include <assert.h>

using namespace std;

namespace JadeGit::Data
{
	ObjectFileStorage::ObjectFileStorage(Assembly& assembly, const FileSystem& fs) : assembly(assembly), fs(fs)
	{
		format = ObjectFileFormat::get();
		loader = make_unique<ObjectLoader>(*this);
	}

	ObjectFileStorage::~ObjectFileStorage() = default;

	void ObjectFileStorage::dele(const Entity& entity) const
	{
		assert(entity.isMajor());

		auto path = entity.path();

		// Delete associated sub-folder
		if (auto folder = fs.open(path); folder.exists())
			folder.remove();

		// Delete associated file
		fs.open(path.replace_extension(format->extension())).remove();
	}

	void ObjectFileStorage::flush() const
	{
		fs.flush();
	}

	ObjectLoader& ObjectFileStorage::getLoader() const
	{
		return *loader;
	}

	Entity* ObjectFileStorage::load(std::filesystem::path path, bool shallow, bool strict) const
	{
		return loader->load(path, shallow, strict);
	}

	void ObjectFileStorage::loadChildren(std::filesystem::path path, bool strict) const
	{
		File folder = fs.open(path);
		if (!folder.exists())
			return;

		if (!folder.isDirectory())
			throw logic_error("Directory path expected");

		// Queue files to be loaded
		auto extension = format->extension();
		for (auto& subfolder : folder)
		{
			if (!subfolder.isDirectory())
				continue;

			for (auto& file : subfolder)
			{
				if (file.isDirectory())
					continue;

				loader->queue(file, strict);
			}
		}

		// Load files
		loader->load();
	}

	void ObjectFileStorage::rename(const Entity& entity, const std::string& new_name) const
	{
		assert(entity.isMajor());

		auto path = entity.path();

		// Rename associated folder
		File folder = fs.open(path);
		if (folder.exists())
			folder.rename(new_name);

		// Rename associated file
		fs.open(path.replace_extension(format->extension())).rename(std::filesystem::path(new_name).replace_extension(format->extension()));
	}

	void ObjectFileStorage::write(const Entity& entity, bool adding) const
	{
		// TODO: Check file doesn't already exist unexpectedly when adding

		auto file = fs.open(entity.path().replace_extension(format->extension()));
		format->write(entity, file);
	}
}
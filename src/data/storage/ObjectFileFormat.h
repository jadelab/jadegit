#pragma once
#include <jadegit/vfs/File.h>
#include <future>
#include <queue>

namespace JadeGit::Data
{
	class Assembly;
	class Object;
	class Entity;

	class ObjectFileFormat
	{
	public:
		static const ObjectFileFormat* get();

		virtual const std::filesystem::path extension() const = 0;
		virtual Entity* load(Assembly& assembly, const File& file, bool shallow, bool strict, std::queue<std::future<void>>& tasks) const = 0;
		virtual void write(const Object& object, File& file) const = 0;
	};
}
#pragma once
#include "FileIterator.h"
#include <stack>

namespace JadeGit
{
	class RecursiveFileIterator
	{
	public:
		RecursiveFileIterator() noexcept;
		RecursiveFileIterator(const File& folder);

		const File& operator*() const;
		const File* operator->() const;

		RecursiveFileIterator& operator++();
		bool operator==(const RecursiveFileIterator& iter) const;
		bool operator!=(const RecursiveFileIterator& iter) const;

	private:
		std::stack<FileIterator> stack;
		bool recurse = false;
	};

	// Range based support
	RecursiveFileIterator begin(RecursiveFileIterator iter) noexcept;
	RecursiveFileIterator end(const RecursiveFileIterator&) noexcept;
}
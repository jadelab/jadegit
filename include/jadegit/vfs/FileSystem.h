#pragma once
#include <filesystem>
#include <jadegit/arch.h>

namespace JadeGit
{
	class File;

	class FileSystem
	{
	public:
		FileSystem();
		virtual ~FileSystem();

		// Indicates whether file system is read-only
		virtual bool isReadOnly() const = 0;

		// Create file handle
		virtual File open(const std::filesystem::path& path) const = 0;

		// Flush pending changes to underlying storage (if applicable)
		virtual void flush() const;

		// Returns true when file system isn't the null file system
		operator bool() const;
	};

	class NullFileSystem : public FileSystem
	{
	public:
		static const NullFileSystem& get();

	private:
		NullFileSystem() = default;
		~NullFileSystem() = default;

		bool isReadOnly() const final;
		File open(const std::filesystem::path& path) const final;
		void flush() const final;
	};
}
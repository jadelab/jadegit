#pragma once
#include "AnyFile.h"

namespace JadeGit
{
	class FileSystem;

	class AnyFileIterator
	{
	public:
		AnyFileIterator(std::unique_ptr<AnyFile>&& parent);
		virtual ~AnyFileIterator();

		/* Parent directory */
		const std::unique_ptr<AnyFile> parent;

		virtual std::unique_ptr<AnyFileIterator> clone() const = 0;

		/* Determines if iterator is still valid */
		virtual bool valid() const = 0;

		/* Returns filename of the current item, relative to the parent directory */
		virtual std::filesystem::path filename() const = 0;

		/* Moves iterator to point to next item */
		virtual void next() = 0;
	};
}
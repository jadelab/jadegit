#pragma once
#include <new>

namespace JadeGit
{
	class MemoryAllocated
	{
	public:
		void* operator new(std::size_t sz, std::align_val_t al);
		void* operator new[](std::size_t sz, std::align_val_t al);
		void operator delete(void* ptr, std::size_t sz, std::align_val_t al);
		void operator delete[](void* ptr, std::size_t sz, std::align_val_t al);
	};
}
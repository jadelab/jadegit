#pragma once
#include "NamedObject.h"
#include "Enum.h"
#include "Array.h"
#include "RootSchema/DevControlPropertiesMeta.h"
#include "RootSchema/DevControlTypesMeta.h"

namespace JadeGit::Data
{
	class Development : public Object
	{
	public:
		using Object::Object;
	};

	class GUIClass;

	class DevControlClass : public Development
	{
	public:
		using Parents = ObjectParents<GUIClass>;

		DevControlClass(GUIClass& parent, const Class* dataClass);

		ObjectValue<GUIClass* const, &DevControlClassMeta::guiClass> guiClass;
	};

	class DevControlTypes;

	class DevControlProperties : public Development, public INamedObject
	{
	public:
		using Parents = ObjectParents<DevControlTypes>;

		DevControlProperties(DevControlTypes& parent, const Class* dataClass, const char* name = nullptr);

		enum Type : int
		{
			Parent = -102,
			Name = 1,
			String = 2,
			Boolean = 3,
			Integer = 4,
			Character = 6,
			Color = 7,
			Font = 8,
			FontSize = 9,
			Real = 10,
			Picture = 11,
			IntegerSigned = 14,
			RealSigned = 15,
			StringMultiLined = 16,
			Xaml = 17,
			ViaPropertyPage = 99,
			List = 100
		};

		Value<Type> cntrlType = String;
		KeyValue<std::string> name;
		ObjectValue<Array<std::string>, &DevControlPropertiesMeta::optionsList> optionsList;
		ObjectValue<DevControlTypes* const, &DevControlPropertiesMeta::parent> parent;

	private:
		const char* getName() const final;
		void LoadHeader(const FileElement& source) final;
		void WriteHeader(tinyxml2::XMLElement* element, const Object* origin, bool reference) const final;
	};

	extern template std::map<DevControlProperties::Type, const char*> EnumStrings<DevControlProperties::Type>::data;

	class DevControlTypes : public DevControlClass, public INamedObject
	{
	public:
		DevControlTypes(GUIClass& parent, const Class* dataClass, const char* name = nullptr);

		ObjectValue<MemberKeyDictionary<DevControlProperties, &DevControlProperties::name>, &DevControlTypesMeta::controlPropsNameDict> controlProps;
		Value<std::string> name;

	private:
		const char* getName() const final;
		void LoadHeader(const FileElement& source) final;
		void WriteHeader(tinyxml2::XMLElement* element, const Object* origin, bool reference) const final;
	};
}
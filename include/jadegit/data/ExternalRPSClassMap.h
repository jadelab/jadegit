#pragma once
#include "RelationalTable.h"
#include "RootSchema/ExternalRPSClassMapMeta.h"

namespace JadeGit::Data
{
	class JadeRpsMapping;

	class ExternalRPSClassMap : public Object
	{
	public:
		using Parents = ObjectParents<JadeRpsMapping>;

		ExternalRPSClassMap(JadeRpsMapping& parent, const Class* dataClass);

		ObjectValue<RAMethod*, &ExternalRPSClassMapMeta::constraint> constraint;
		KeyValue<std::string> guid;
		ObjectValue<Class*, &ExternalRPSClassMapMeta::rpsClass> rpsClass;
		ObjectValue<JadeRpsMapping* const, &ExternalRPSClassMapMeta::rpsDatabase> rpsDatabase;
		NamedObjectDict<RelationalTable, &ExternalRPSClassMapMeta::tables> tables;

		RAMethod* getConstraint(const std::string& name) const;
	};

	extern template ObjectValue<RAMethod*, &ExternalRPSClassMapMeta::constraint>;
	extern template ObjectValue<Class*, &ExternalRPSClassMapMeta::rpsClass>;
	extern template ObjectValue<JadeRpsMapping* const, &ExternalRPSClassMapMeta::rpsDatabase>;
	extern template NamedObjectDict<RelationalTable, &ExternalRPSClassMapMeta::tables>;
}
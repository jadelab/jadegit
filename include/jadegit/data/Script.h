#pragma once
#include "Feature.h"

namespace JadeGit::Data
{
	class Script : public Feature
	{
	public:
		Script(Type* parent, const Class* dataClass, const char* name) : Feature(parent, dataClass, name) {};

		Value<bool> notImplemented;
		Value<std::string> source;

	protected:
		Script(Object* parent, const Class* dataClass, const char* name) : Feature(parent, dataClass, name) {};
	};
}
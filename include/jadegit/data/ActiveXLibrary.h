#pragma once
#include "ActiveXClass.h"
#include "EntityDict.h"
#include <jadegit/data/RootSchema/ActiveXLibraryMeta.h>

namespace JadeGit::Data
{
	class Schema;

	class ActiveXLibrary : public MajorEntity<ActiveXLibrary, Entity>
	{
	public:
		using Parents = ObjectParents<Schema>;
		static const std::filesystem::path subFolder;

		ActiveXLibrary(Schema& parent, const Class* dataClass, const char* name);

		NamedObjectDict<ActiveXClass, &ActiveXLibraryMeta::coClasses> classes;
		Value<Binary> guid;
		ObjectValue<Schema* const, &ActiveXLibraryMeta::_schema> schema;

		void Accept(EntityVisitor& v) final;

		Class* getBaseClass() const;
	};

	extern template NamedObjectDict<ActiveXClass, &ActiveXLibraryMeta::coClasses>;
	extern template ObjectValue<Schema* const, &ActiveXLibraryMeta::_schema>;
}
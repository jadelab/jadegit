#pragma once
#include "NamedObject.h"
#include "RootSchema/RAMethodMeta.h"

namespace JadeGit::Data
{
	class ExternalRPSClassMap;
	class RelationalTable;

	class RelationalAttribute : public NamedObject
	{
	public:
		using Parents = ObjectParents<RelationalTable>;

		RelationalAttribute(RelationalTable& parent, const Class* dataClass, const char* name);

		ObjectValue<RelationalTable* const, &RelationalAttributeMeta::table> table;

	protected:
		RelationalAttribute(Object& parent, const Class* dataClass, const char* name);
	};

	extern template ObjectValue<RelationalTable* const, &RelationalAttributeMeta::table>;

	class RAMethod : public RelationalAttribute
	{
	public:
		using Parents = ObjectParents<ExternalRPSClassMap, RelationalTable>;

		RAMethod(RelationalTable& parent, const Class* dataClass, const char* name);
		RAMethod(ExternalRPSClassMap& parent, const Class* dataClass, const char* name);

		ObjectValue<ExternalRPSClassMap* const, &RAMethodMeta::rpsClassMap> classMap;
	};

	extern template ObjectValue<ExternalRPSClassMap* const, &RAMethodMeta::rpsClassMap>;
}
#pragma once
#include "NamedObject.h"
#include "ObjectValue.h"
#include "RootSchema/JadeDynamicPropertyClusterMeta.h"

namespace JadeGit::Data
{
	class Class;
	class Type;

	DECLARE_OBJECT_CAST(Type)

	class JadeDynamicPropertyCluster : public NamedObject
	{
	public:
		using Parents = ObjectParents<Class>;

		JadeDynamicPropertyCluster(Class* parent, const Class* dataClass, const char* name);

		ObjectValue<Type*, &JadeDynamicPropertyClusterMeta::schemaType> schemaType;
	};
}
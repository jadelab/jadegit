#pragma once
#include "NamedObject.h"

namespace JadeGit::Data
{
	class ExternalSchemaEntity : public NamedObject
	{
	public:
		using Parents = ObjectParents<ExternalTable>;

	protected:
		using NamedObject::NamedObject;
	};
}
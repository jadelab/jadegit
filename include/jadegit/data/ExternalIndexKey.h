#pragma once
#include "ExternalSchemaEntity.h"
#include "RootSchema/ExternalIndexKeyMeta.h"

namespace JadeGit::Data
{
	class ExternalColumn;
	class ExternalIndex;

	class ExternalIndexKey : public ExternalSchemaEntity
	{
	public:
		using Parents = ObjectParents<ExternalIndex>;

		ExternalIndexKey(ExternalIndex& parent, const Class* dataClass);

		Value<ExternalColumn*> column;
		ObjectValue<ExternalIndex* const, &ExternalIndexKeyMeta::index> index;
	};
}
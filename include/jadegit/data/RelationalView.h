#pragma once
#include "ExternalRPSClassMap.h"
#include "RelationalTable.h"
#include "RootSchema/RelationalViewMeta.h"

namespace JadeGit::Data
{
	class Schema;

	class RelationalView : public Entity
	{
	public:
		using Parents = ObjectParents<Schema>;

		ObjectValue<Array<ExternalRPSClassMap*>, &RelationalViewMeta::rpsClassMaps> rpsClassMaps;
		ObjectValue<Schema*, &RelationalViewMeta::schema> schema;
		NamedObjectDict<RelationalTable, &RelationalViewMeta::tables> tables;

	protected:
		using Entity::Entity;
	};

	extern template ObjectValue<Array<ExternalRPSClassMap*>, &RelationalViewMeta::rpsClassMaps>;
	extern template ObjectValue<Schema* const, &RelationalViewMeta::schema>;
	extern template NamedObjectDict<RelationalTable, &RelationalViewMeta::tables>;

	class JadeOdbcView : public MajorEntity<JadeOdbcView, RelationalView>
	{
	public:
		static const std::filesystem::path subFolder;

		JadeOdbcView(Schema& parent, const Class* dataClass, const char* name);

	protected:
		void Accept(EntityVisitor& v) final;
	};

	class JadeRpsMapping : public MajorEntity<JadeRpsMapping, RelationalView>
	{
	public:
		static const std::filesystem::path subFolder;

		JadeRpsMapping(Schema& parent, const Class* dataClass, const char* name);

	protected:
		void Accept(EntityVisitor& v) final;
	};
}
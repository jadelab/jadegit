#pragma once
#include "EntityDict.h"
#include "JadeExportedConstant.h"
#include "JadeExportedMethod.h"
#include "JadeExportedProperty.h"
#include "RootSchema/JadeExportedTypeMeta.h"

namespace JadeGit::Data
{
	class JadeExportedPackage;

	DECLARE_OBJECT_CAST(JadeExportedPackage);

	class JadeExportedType : public JadeExportedEntity
	{
	public:
		using Parents = ObjectParents<JadeExportedPackage>;

		JadeExportedType(JadeExportedPackage* parent, const Class* dataClass, const char* name);

		EntityDict<JadeExportedConstant, &JadeExportedTypeMeta::exportedConstants> exportedConstants;
		EntityDict<JadeExportedMethod, &JadeExportedTypeMeta::exportedMethods> exportedMethods;
		EntityDict<JadeExportedProperty, &JadeExportedTypeMeta::exportedProperties> exportedProperties;
		ObjectValue<JadeExportedPackage*, &JadeExportedTypeMeta::package> package;

		std::unique_ptr<QualifiedName> GetOriginalEntityName() const override;
	};

	extern template EntityDict<JadeExportedConstant, &JadeExportedTypeMeta::exportedConstants>;
	extern template EntityDict<JadeExportedMethod, &JadeExportedTypeMeta::exportedMethods>;
	extern template EntityDict<JadeExportedProperty, &JadeExportedTypeMeta::exportedProperties>;
	extern template ObjectValue<JadeExportedPackage*, &JadeExportedTypeMeta::package>;
}
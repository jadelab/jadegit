#pragma once
#include "ActiveXFeature.h"

namespace JadeGit::Data
{
	class ActiveXMethod : public ActiveXFeature
	{
	public:
		ActiveXMethod(ActiveXClass& parent, const Class* dataClass, const char* name);

	protected:
		void loaded(bool strict, std::queue<std::future<void>>& tasks) final;
	};
}
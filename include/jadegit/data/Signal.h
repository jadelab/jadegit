#pragma once
#include <functional>
#include <map>

namespace JadeGit::Data
{
	template <typename... Args>
	class Signal
	{
	public:
		template <typename T>
		void Connect(T* inst, void (T::*func)(Args...))
		{
			Connect(inst, [=](Args... args) {
				(inst->*func)(args...);
			});
		}

		template <typename T>
		void Connect(T* inst, void (T::*func)(Args...) const) const
		{
			Connect(inst, [=](Args... args) {
				(inst->*func)(args...);
			});
		}

		void Connect(void* inst, std::function<void(Args...)> const& slot) const
		{
			slots.insert(std::make_pair(inst, slot));
		}

		void Disconnect(void* inst) const
		{
			slots.erase(inst);
		}

		void Emit(Args... p) {
			for (auto& it : slots) {
				it.second(p...);
			}
		}

	private:
		mutable std::pmr::map<void*, std::function<void(Args...)>> slots;
	};
}
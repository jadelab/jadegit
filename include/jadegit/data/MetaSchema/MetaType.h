#pragma once
#include "MetaObject.h"

namespace JadeGit
{
	class Version;
}

namespace JadeGit::Data
{
	class MetaSchema;

	template<class TType>
	class MetaType : public Meta<TType>
	{
	public:
		MetaType(MetaSchema& parent, const char* name);

	protected:
		MetaType(MetaSchema& parent, TType* existing, const char* name);

		template <class TType>
		TType* NewType(const char* name)
		{
			TType* type = dynamic_cast<TType*>(this->subject->GetExisting(name));
			return type ? type : new TType(this->subject->schema, nullptr, name);
		}

		template <class TMethod>
		TMethod* NewMethod(const char* name)
		{
			return new TMethod(this->subject, nullptr, name);
		}

		template <class TMethod>
		TMethod* NewMethod(const Version& minVersion, const char* name)
		{
			return versionCheck(minVersion) ? NewMethod<TMethod>(name) : nullptr;
		}

		bool versionCheck(const Version& minVersion) const;

	private:
		MetaSchema& parent;
	};
}
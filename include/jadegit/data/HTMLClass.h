#pragma once
#include "Class.h"
#include "JadeHTMLDocument.h"
#include "RootSchema/HTMLClassMeta.h"

namespace JadeGit::Data
{
	class HTMLClass : public Class
	{
	public:
		using Parents = ObjectParents<Schema>;

		HTMLClass(Schema* parent, const Class* dataClass, const char* name, Class* superclass = nullptr);

		ObjectValue<JadeHTMLDocument*, &HTMLClassMeta::_jadeHTMLDocument> htmlDocument;
	};
}
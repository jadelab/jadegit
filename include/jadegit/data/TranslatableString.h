#pragma once
#include "Constant.h"
#include "RootSchema/TranslatableStringMeta.h"

namespace JadeGit::Data
{
	class Locale;

	class TranslatableString : public Constant
	{
	public:
		using Parents = ObjectParents<Locale>;

		TranslatableString(Locale* parent, const Class* dataClass, const char* name);

		ObjectValue<Locale* const, &TranslatableStringMeta::locale> locale;

		void Accept(EntityVisitor &v) override;

	private:
		const Entity* getQualifiedParent() const final;
	};

	extern template ObjectValue<Locale* const, &TranslatableStringMeta::locale>;
}
#pragma once
#include "Entity.h"
#include "NamedObjectDict.h"

namespace JadeGit::Data
{
	template<class TChild, auto prop> requires std::is_base_of_v<Entity, TChild>
	using EntityDict = NamedObjectDict<TChild, prop>;
}
#pragma once
#include "ClassMeta.h"

namespace JadeGit::Data
{
	class HTMLClassMeta : public RootClass<>
	{
	public:
		static const HTMLClassMeta& get(const Object& object);
		
		HTMLClassMeta(RootSchema& parent, const ClassMeta& superclass);
	
		ExplicitInverseRef* const _jadeHTMLDocument;
	};
};

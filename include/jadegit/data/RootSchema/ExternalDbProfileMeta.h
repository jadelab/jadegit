#pragma once
#include "ObjectMeta.h"

namespace JadeGit::Data
{
	class ExternalDbProfileMeta : public RootClass<>
	{
	public:
		static const ExternalDbProfileMeta& get(const Object& object);
		
		ExternalDbProfileMeta(RootSchema& parent, const ObjectMeta& superclass);
	
		PrimAttribute* const clsQueryClass;
		PrimAttribute* const collQueryClass;
		ExplicitInverseRef* const database;
		PrimAttribute* const defineAttrClass;
		PrimAttribute* const defineRefFromClass;
		PrimAttribute* const defineRefToClass;
		PrimAttribute* const includeKeys;
		PrimAttribute* const offline;
		PrimAttribute* const prefixAttr;
		PrimAttribute* const prefixClass;
		PrimAttribute* const prefixKeys;
		PrimAttribute* const prefixRef;
		PrimAttribute* const refQueryClass;
		PrimAttribute* const replaceUScore;
		PrimAttribute* const savePswd;
		PrimAttribute* const sheet;
		PrimAttribute* const suffixArray;
		PrimAttribute* const suffixDict;
		PrimAttribute* const suffixSet;
		PrimAttribute* const toMixedCase;
		PrimAttribute* const toSingular;
		PrimAttribute* const useUnderscore;
	};
};

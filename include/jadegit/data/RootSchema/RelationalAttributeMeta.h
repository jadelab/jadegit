#pragma once
#include "ObjectMeta.h"

namespace JadeGit::Data
{
	class RelationalAttributeMeta : public RootClass<>
	{
	public:
		static const RelationalAttributeMeta& get(const Object& object);
		
		RelationalAttributeMeta(RootSchema& parent, const ObjectMeta& superclass);
	
		PrimAttribute* const attrName;
		PrimAttribute* const decimals;
		ExplicitInverseRef* const feature;
		PrimAttribute* const length;
		PrimAttribute* const name;
		PrimAttribute* const referencedClassName;
		PrimAttribute* const rpsColType;
		PrimAttribute* const rpsExcluded;
		PrimAttribute* const rpsKeyKind;
		PrimAttribute* const rpsMapKind;
		PrimAttribute* const sqlType;
		ExplicitInverseRef* const table;
	};
};

#pragma once
#include "JadeMethodMeta.h"

namespace JadeGit::Data
{
	class JadeWebServicesMethodMeta : public RootClass<>
	{
	public:
		static const JadeWebServicesMethodMeta& get(const Object& object);
		
		JadeWebServicesMethodMeta(RootSchema& parent, const JadeMethodMeta& superclass);
	
		PrimAttribute* const inputEncodingStyle;
		PrimAttribute* const inputNamespace;
		PrimAttribute* const inputUsesEncodedFormat;
		PrimAttribute* const outputEncodingStyle;
		PrimAttribute* const outputNamespace;
		PrimAttribute* const outputUsesEncodedFormat;
		PrimAttribute* const soapAction;
		ExplicitInverseRef* const soapHeaders;
		PrimAttribute* const useBareStyle;
		PrimAttribute* const useSoap12;
		PrimAttribute* const usesRPC;
	};
};

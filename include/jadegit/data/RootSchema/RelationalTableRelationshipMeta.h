#pragma once
#include "RelationalTableMeta.h"

namespace JadeGit::Data
{
	class RelationalTableRelationshipMeta : public RootClass<>
	{
	public:
		static const RelationalTableRelationshipMeta& get(const Object& object);
		
		RelationalTableRelationshipMeta(RootSchema& parent, const RelationalTableMeta& superclass);
	
		PrimAttribute* const firstPropertyName;
		PrimAttribute* const secondClassName;
	};
};

#pragma once
#include "../MetaSchema/MetaClass.h"

namespace JadeGit::Data
{
	template<class TType = Class>
	class RootClass : public MetaClass<TType>
	{
	public:
		RootClass(RootSchema& parent, const char* name, Class* superclass);
	};
}
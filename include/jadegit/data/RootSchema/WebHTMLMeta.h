#pragma once
#include "TextBoxMeta.h"

namespace JadeGit::Data
{
	class WebHTMLMeta : public RootClass<GUIClass>
	{
	public:
		static const WebHTMLMeta& get(const Object& object);
		
		WebHTMLMeta(RootSchema& parent, const TextBoxMeta& superclass);
	
		PrimAttribute* const ignoreHeight;
		PrimAttribute* const ignoreWidth;
		PrimAttribute* const transparent;
	};
};

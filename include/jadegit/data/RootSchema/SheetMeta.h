#pragma once
#include "GroupBoxMeta.h"

namespace JadeGit::Data
{
	class SheetMeta : public RootClass<GUIClass>
	{
	public:
		static const SheetMeta& get(const Object& object);
		
		SheetMeta(RootSchema& parent, const GroupBoxMeta& superclass);
	
		PrimAttribute* const icon;
	};
};

#pragma once
#include "ControlMeta.h"

namespace JadeGit::Data
{
	class TextBoxMeta : public RootClass<GUIClass>
	{
	public:
		static const TextBoxMeta& get(const Object& object);
		
		TextBoxMeta(RootSchema& parent, const ControlMeta& superclass);
	
		PrimAttribute* const alignment;
		PrimAttribute* const autoTab;
		PrimAttribute* const case_;
		PrimAttribute* const dataType;
		PrimAttribute* const decimals;
		PrimAttribute* const formatOut;
		PrimAttribute* const hintBackColor;
		PrimAttribute* const hintForeColor;
		PrimAttribute* const hintText;
		PrimAttribute* const integralHeight;
		PrimAttribute* const maxLength;
		PrimAttribute* const passwordField;
		PrimAttribute* const readOnly;
		PrimAttribute* const scrollBars;
		PrimAttribute* const scrollHorizontal;
		PrimAttribute* const scrollVertical;
		PrimAttribute* const selectionStyle;
		PrimAttribute* const text;
		PrimAttribute* const textOffset;
		PrimAttribute* const wantReturn;
		PrimAttribute* const webInputType;
	};
};

#pragma once
#include "JadeImportedFeatureMeta.h"

namespace JadeGit::Data
{
	class JadeImportedConstantMeta : public RootClass<>
	{
	public:
		static const JadeImportedConstantMeta& get(const Object& object);
		
		JadeImportedConstantMeta(RootSchema& parent, const JadeImportedFeatureMeta& superclass);
	
		ExplicitInverseRef* const exportedConstant;
	};
};

#pragma once
#include "ObjectMeta.h"

namespace JadeGit::Data
{
	class RelationalTableMeta : public RootClass<>
	{
	public:
		static const RelationalTableMeta& get(const Object& object);
		
		RelationalTableMeta(RootSchema& parent, const ObjectMeta& superclass);
	
		ExplicitInverseRef* const attributes;
		ExplicitInverseRef* const classes;
		PrimAttribute* const firstClassName;
		PrimAttribute* const name;
		ExplicitInverseRef* const rView;
		ExplicitInverseRef* const rpsClassMap;
		PrimAttribute* const rpsExcluded;
		PrimAttribute* const rpsExtra;
		PrimAttribute* const rpsIncludeInCallback;
		PrimAttribute* const rpsShowMethods;
		PrimAttribute* const rpsShowObjectFeatures;
		PrimAttribute* const rpsShowVirtualProperties;
		PrimAttribute* const rpsTableKind;
		PrimAttribute* const uuid;
	};
};

#pragma once
#include "JadeImportedFeatureMeta.h"

namespace JadeGit::Data
{
	class JadeImportedMethodMeta : public RootClass<>
	{
	public:
		static const JadeImportedMethodMeta& get(const Object& object);
		
		JadeImportedMethodMeta(RootSchema& parent, const JadeImportedFeatureMeta& superclass);
	
		ExplicitInverseRef* const exportedMethod;
	};
};

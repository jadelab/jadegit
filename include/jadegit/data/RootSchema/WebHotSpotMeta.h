#pragma once
#include "LabelMeta.h"

namespace JadeGit::Data
{
	class WebHotSpotMeta : public RootClass<GUIClass>
	{
	public:
		static const WebHotSpotMeta& get(const Object& object);
		
		WebHotSpotMeta(RootSchema& parent, const LabelMeta& superclass);
	};
};

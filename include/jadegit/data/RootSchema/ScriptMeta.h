#pragma once
#include "FeatureMeta.h"

namespace JadeGit::Data
{
	class ScriptMeta : public RootClass<>
	{
	public:
		static const ScriptMeta& get(const Object& object);
		
		ScriptMeta(RootSchema& parent, const FeatureMeta& superclass);
	
		PrimAttribute* const compiledOK;
		PrimAttribute* const compilerVersion;
		PrimAttribute* const errorCode;
		PrimAttribute* const errorLength;
		PrimAttribute* const errorPosition;
		PrimAttribute* const notImplemented;
		PrimAttribute* const source;
		PrimAttribute* const status;
		PrimAttribute* const warningCount;
	};
};

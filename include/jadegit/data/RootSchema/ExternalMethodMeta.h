#pragma once
#include "MethodMeta.h"

namespace JadeGit::Data
{
	class ExternalMethodMeta : public RootClass<>
	{
	public:
		static const ExternalMethodMeta& get(const Object& object);
		
		ExternalMethodMeta(RootSchema& parent, const MethodMeta& superclass);
	};
};

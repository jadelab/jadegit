#pragma once
#include "RelationalViewMeta.h"

namespace JadeGit::Data
{
	class JadeOdbcViewMeta : public RootClass<>
	{
	public:
		static const JadeOdbcViewMeta& get(const Object& object);
		
		JadeOdbcViewMeta(RootSchema& parent, const RelationalViewMeta& superclass);
	};
};

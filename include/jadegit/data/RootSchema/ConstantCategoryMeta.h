#pragma once
#include "SchemaEntityMeta.h"

namespace JadeGit::Data
{
	class ConstantCategoryMeta : public RootClass<>
	{
	public:
		static const ConstantCategoryMeta& get(const Object& object);
		
		ConstantCategoryMeta(RootSchema& parent, const SchemaEntityMeta& superclass);
	
		ExplicitInverseRef* const consts;
		ExplicitInverseRef* const schema;
	};
};

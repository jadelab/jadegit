#pragma once
#include "ExternalSchemaEntityMeta.h"

namespace JadeGit::Data
{
	class ExternalIndexMeta : public RootClass<>
	{
	public:
		static const ExternalIndexMeta& get(const Object& object);
		
		ExternalIndexMeta(RootSchema& parent, const ExternalSchemaEntityMeta& superclass);
	
		PrimAttribute* const cardinality;
		PrimAttribute* const duplicatesAllowed;
		PrimAttribute* const filterCondition;
		ExplicitInverseRef* const keys;
		ExplicitInverseRef* const table;
	};
};

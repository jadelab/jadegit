#pragma once
#include "ActiveXFeatureMeta.h"

namespace JadeGit::Data
{
	class ActiveXConstantMeta : public RootClass<>
	{
	public:
		static const ActiveXConstantMeta& get(const Object& object);
		
		ActiveXConstantMeta(RootSchema& parent, const ActiveXFeatureMeta& superclass);
	
		PrimAttribute* const activeXName;
	};
};

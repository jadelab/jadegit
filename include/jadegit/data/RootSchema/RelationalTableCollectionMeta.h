#pragma once
#include "RelationalTableMeta.h"

namespace JadeGit::Data
{
	class RelationalTableCollectionMeta : public RootClass<>
	{
	public:
		static const RelationalTableCollectionMeta& get(const Object& object);
		
		RelationalTableCollectionMeta(RootSchema& parent, const RelationalTableMeta& superclass);
	
		PrimAttribute* const collectionName;
		PrimAttribute* const collectionType;
	};
};

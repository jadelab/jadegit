#pragma once
#include "ClassMeta.h"

namespace JadeGit::Data
{
	class JadeWebServicesClassMeta : public RootClass<>
	{
	public:
		static const JadeWebServicesClassMeta& get(const Object& object);
		
		JadeWebServicesClassMeta(RootSchema& parent, const ClassMeta& superclass);
	
		PrimAttribute* const additionalInfo;
		PrimAttribute* const wsdl;
	};
};

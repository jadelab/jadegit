#pragma once
#include "ExternalScriptElementMapMeta.h"

namespace JadeGit::Data
{
	class ExternalParameterMapMeta : public RootClass<>
	{
	public:
		static const ExternalParameterMapMeta& get(const Object& object);
		
		ExternalParameterMapMeta(RootSchema& parent, const ExternalScriptElementMapMeta& superclass);
	};
};

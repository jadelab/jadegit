#pragma once
#include "RelationalTableMeta.h"

namespace JadeGit::Data
{
	class RelationalTableXRelationshipMeta : public RootClass<>
	{
	public:
		static const RelationalTableXRelationshipMeta& get(const Object& object);
		
		RelationalTableXRelationshipMeta(RootSchema& parent, const RelationalTableMeta& superclass);
	
		PrimAttribute* const firstPropertyName;
		PrimAttribute* const secondClassName;
	};
};

#pragma once
#include "TypeUsageMeta.h"

namespace JadeGit::Data
{
	class InlineTypeUsageMeta : public RootClass<>
	{
	public:
		static const InlineTypeUsageMeta& get(const Object& object);
		
		InlineTypeUsageMeta(RootSchema& parent, const TypeUsageMeta& superclass);
	};
};

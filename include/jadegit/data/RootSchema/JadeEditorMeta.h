#pragma once
#include "JadeTextEditMeta.h"

namespace JadeGit::Data
{
	class JadeEditorMeta : public RootClass<GUIClass>
	{
	public:
		static const JadeEditorMeta& get(const Object& object);
		
		JadeEditorMeta(RootSchema& parent, const JadeTextEditMeta& superclass);
	};
};

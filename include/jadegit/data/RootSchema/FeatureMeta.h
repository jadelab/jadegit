#pragma once
#include "SchemaEntityMeta.h"

namespace JadeGit::Data
{
	class FeatureMeta : public RootClass<>
	{
	public:
		static const FeatureMeta& get(const Object& object);
		
		FeatureMeta(RootSchema& parent, const SchemaEntityMeta& superclass);
	
		ExplicitInverseRef* const activeXFeature;
		ExplicitInverseRef* const exposedClassRefs;
		ExplicitInverseRef* const exposedFeatureRefs;
		ExplicitInverseRef* const schemaType;
		ExplicitInverseRef* const type;
	};
};

#pragma once
#include "ObjectMeta.h"

namespace JadeGit::Data
{
	class ExternalDatabaseMeta : public RootClass<>
	{
	public:
		static const ExternalDatabaseMeta& get(const Object& object);
		
		ExternalDatabaseMeta(RootSchema& parent, const ObjectMeta& superclass);
	
		ExplicitInverseRef* const _classMaps;
		ExplicitInverseRef* const _classes;
		ExplicitInverseRef* const _collClassMaps;
		ExplicitInverseRef* const _collClasses;
		PrimAttribute* const _dataSource;
		ExplicitInverseRef* const _driverInfo;
		ExplicitInverseRef* const _profile;
		ExplicitInverseRef* const _schema;
		PrimAttribute* const _state;
		ExplicitInverseRef* const _tables;
		PrimAttribute* const _version;
		PrimAttribute* const connectionString;
		PrimAttribute* const name;
		PrimAttribute* const password;
		PrimAttribute* const serverName;
		PrimAttribute* const userName;
		PrimAttribute* const uuid;
	};
};

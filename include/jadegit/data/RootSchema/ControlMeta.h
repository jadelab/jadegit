#pragma once
#include "WindowMeta.h"

namespace JadeGit::Data
{
	class ControlMeta : public RootClass<GUIClass>
	{
	public:
		static const ControlMeta& get(const Object& object);
		
		ControlMeta(RootSchema& parent, const WindowMeta& superclass);
	
		PrimAttribute* const backBrush;
		PrimAttribute* const borderColorSingle;
		PrimAttribute* const focusBackColor;
		PrimAttribute* const focusForeColor;
		PrimAttribute* const fontBold;
		PrimAttribute* const fontItalic;
		PrimAttribute* const fontName;
		PrimAttribute* const fontSize;
		PrimAttribute* const fontStrikethru;
		PrimAttribute* const fontUnderline;
		PrimAttribute* const fontWeight;
		PrimAttribute* const foreColor;
		ExplicitInverseRef* const form;
		ExplicitInverseRef* const parent;
		PrimAttribute* const parentAspect;
		PrimAttribute* const parentBottomOffset;
		PrimAttribute* const parentRightOffset;
		PrimAttribute* const relativeHeight;
		PrimAttribute* const relativeLeft;
		PrimAttribute* const relativeTop;
		PrimAttribute* const relativeWidth;
		PrimAttribute* const show3D;
		PrimAttribute* const tabIndex;
		PrimAttribute* const tabStop;
	};
};

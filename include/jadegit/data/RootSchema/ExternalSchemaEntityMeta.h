#pragma once
#include "ObjectMeta.h"

namespace JadeGit::Data
{
	class ExternalSchemaEntityMeta : public RootClass<>
	{
	public:
		static const ExternalSchemaEntityMeta& get(const Object& object);
		
		ExternalSchemaEntityMeta(RootSchema& parent, const ObjectMeta& superclass);
	
		PrimAttribute* const name;
		PrimAttribute* const remarks;
		PrimAttribute* const state;
	};
};

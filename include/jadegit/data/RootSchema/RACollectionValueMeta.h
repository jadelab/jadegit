#pragma once
#include "RelationalAttributeMeta.h"

namespace JadeGit::Data
{
	class RACollectionValueMeta : public RootClass<>
	{
	public:
		static const RACollectionValueMeta& get(const Object& object);
		
		RACollectionValueMeta(RootSchema& parent, const RelationalAttributeMeta& superclass);
	};
};

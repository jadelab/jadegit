#pragma once
#include "ExternalPropertyMapMeta.h"

namespace JadeGit::Data
{
	class ExternalAttributeMapMeta : public RootClass<>
	{
	public:
		static const ExternalAttributeMapMeta& get(const Object& object);
		
		ExternalAttributeMapMeta(RootSchema& parent, const ExternalPropertyMapMeta& superclass);
	
		ExplicitInverseRef* const classMap;
		ExplicitInverseRef* const column;
	};
};

#pragma once
#include "ClassMeta.h"

namespace JadeGit::Data
{
	class ExceptionClassMeta : public RootClass<>
	{
	public:
		static const ExceptionClassMeta& get(const Object& object);
		
		ExceptionClassMeta(RootSchema& parent, const ClassMeta& superclass);
	};
};

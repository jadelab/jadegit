#pragma once
#include "ObjectMeta.h"

namespace JadeGit::Data
{
	class ExternalObjectMeta : public RootClass<ExternalClass>
	{
	public:
		static const ExternalObjectMeta& get(const Object& object);
		
		ExternalObjectMeta(RootSchema& parent, const ObjectMeta& superclass);
	};
};

#pragma once
#include "RootClass.h"

namespace JadeGit::Data
{
	class ObjectMeta : public RootClass<>
	{
	public:
		static const ObjectMeta& get(const Object& object);
		
		ObjectMeta(RootSchema& parent);
	};
};

#pragma once
#include "ControlMeta.h"

namespace JadeGit::Data
{
	class JadeDotNetVisualComponentMeta : public RootClass<GUIClass>
	{
	public:
		static const JadeDotNetVisualComponentMeta& get(const Object& object);
		
		JadeDotNetVisualComponentMeta(RootSchema& parent, const ControlMeta& superclass);
	};
};

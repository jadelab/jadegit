#pragma once
#include "ControlMeta.h"

namespace JadeGit::Data
{
	class GroupBoxMeta : public RootClass<GUIClass>
	{
	public:
		static const GroupBoxMeta& get(const Object& object);
		
		GroupBoxMeta(RootSchema& parent, const ControlMeta& superclass);
	
		PrimAttribute* const caption;
		PrimAttribute* const clipControls;
		PrimAttribute* const transparent;
	};
};

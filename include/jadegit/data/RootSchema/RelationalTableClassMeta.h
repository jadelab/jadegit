#pragma once
#include "RelationalTableMeta.h"

namespace JadeGit::Data
{
	class RelationalTableClassMeta : public RootClass<>
	{
	public:
		static const RelationalTableClassMeta& get(const Object& object);
		
		RelationalTableClassMeta(RootSchema& parent, const RelationalTableMeta& superclass);
	
		PrimAttribute* const bCallIFAllInstances;
		PrimAttribute* const rootCollectionName;
	};
};

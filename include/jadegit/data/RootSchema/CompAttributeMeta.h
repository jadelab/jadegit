#pragma once
#include "AttributeMeta.h"

namespace JadeGit::Data
{
	class CompAttributeMeta : public RootClass<>
	{
	public:
		static const CompAttributeMeta& get(const Object& object);
		
		CompAttributeMeta(RootSchema& parent, const AttributeMeta& superclass);
	};
};

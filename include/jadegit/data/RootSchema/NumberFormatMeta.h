#pragma once
#include "LocaleFormatMeta.h"

namespace JadeGit::Data
{
	class NumberFormatMeta : public RootClass<>
	{
	public:
		static const NumberFormatMeta& get(const Object& object);
		
		NumberFormatMeta(RootSchema& parent, const LocaleFormatMeta& superclass);
	
		PrimAttribute* const decimalPlaces;
		PrimAttribute* const decimalSeparator;
		PrimAttribute* const groupings;
		PrimAttribute* const negativeFormat;
		PrimAttribute* const showLeadingZeros;
		PrimAttribute* const thousandSeparator;
	};
};

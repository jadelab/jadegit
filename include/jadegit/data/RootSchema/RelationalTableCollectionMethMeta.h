#pragma once
#include "RelationalTableMeta.h"

namespace JadeGit::Data
{
	class RelationalTableCollectionMethMeta : public RootClass<>
	{
	public:
		static const RelationalTableCollectionMethMeta& get(const Object& object);
		
		RelationalTableCollectionMethMeta(RootSchema& parent, const RelationalTableMeta& superclass);
	
		PrimAttribute* const collectionType;
		PrimAttribute* const methodName;
		PrimAttribute* const secondClassName;
	};
};

#pragma once
#include "ObjectMeta.h"

namespace JadeGit::Data
{
	class ExternalScriptElementMapMeta : public RootClass<>
	{
	public:
		static const ExternalScriptElementMapMeta& get(const Object& object);
		
		ExternalScriptElementMapMeta(RootSchema& parent, const ObjectMeta& superclass);
	};
};

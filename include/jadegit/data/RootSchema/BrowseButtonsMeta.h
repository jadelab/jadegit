#pragma once
#include "ControlMeta.h"

namespace JadeGit::Data
{
	class BrowseButtonsMeta : public RootClass<GUIClass>
	{
	public:
		static const BrowseButtonsMeta& get(const Object& object);
		
		BrowseButtonsMeta(RootSchema& parent, const ControlMeta& superclass);
	};
};

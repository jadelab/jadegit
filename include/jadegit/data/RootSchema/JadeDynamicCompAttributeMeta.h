#pragma once
#include "CompAttributeMeta.h"

namespace JadeGit::Data
{
	class JadeDynamicCompAttributeMeta : public RootClass<>
	{
	public:
		static const JadeDynamicCompAttributeMeta& get(const Object& object);
		
		JadeDynamicCompAttributeMeta(RootSchema& parent, const CompAttributeMeta& superclass);
	
		ExplicitInverseRef* const dynamicPropertyCluster;
	};
};

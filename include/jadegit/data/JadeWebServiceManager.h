#pragma once
#include "Object.h"
#include "RootSchema\JadeWebServiceManagerMeta.h"

namespace JadeGit::Data
{
	class Application;

	class JadeWebServiceManager : public Object
	{
	public:
		using Parents = ObjectParents<Application>;

		JadeWebServiceManager(Application& parent, const Class* dataClass);

		ObjectValue<Application* const, &JadeWebServiceManagerMeta::application> application;
		ObjectValue<Array<JadeExposedList*>, &JadeWebServiceManagerMeta::_listOfExposures> exposures;

	private:
		void loaded(bool strict, std::queue<std::future<void>>& tasks);
	};

	extern template ObjectValue<Application* const, &JadeWebServiceManagerMeta::application>;
	extern template ObjectValue<Array<JadeExposedList*>, &JadeWebServiceManagerMeta::_listOfExposures>;
}